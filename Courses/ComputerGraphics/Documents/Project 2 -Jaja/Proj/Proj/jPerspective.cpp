#include "jPerspective.h"
#include "jMatrix.h"
#include <stdlib.h>

jPerspective::jPerspective(void){
	ref.prp = new jMatrix(HOMO_DIM,HOMO_DIM);
	ref.vpn = new jMatrix(HOMO_DIM,HOMO_DIM);
	ref.vup = new jMatrix(HOMO_DIM,HOMO_DIM);
	ref.vrp = new jMatrix(HOMO_DIM,HOMO_DIM);
	mCan = new jMatrix(HOMO_DIM,HOMO_DIM);
	Mvv3dv = new jMatrix(HOMO_DIM,HOMO_DIM);
}


jPerspective::~jPerspective(void){
	ref.prp->~jMatrix();
	ref.vpn->~jMatrix();
	ref.vup->~jMatrix();
	ref.vrp->~jMatrix();
}

void jPerspective::computeMCan(){
	//2.1) Translate VRP to the origin
	//2.2) Rotate VRC so n-axis (VPN) is z-axis, u-axis is x-axis, and v-axis is y-axis
	//2.3) Translate so that the center of projection(PRP) is at the origin
	//2.4) Shear so the center line of the view volume is the z-axis
	//2.5) Scale into canonical view volume

	/*STEP 2.1 T(-VRP)*/
	jMatrix* tVrp = new jMatrix(HOMO_DIM,HOMO_DIM);
	tVrp->setTranslate(-ref.vrp->at(MX),-ref.vrp->at(MY),-ref.vrp->at(MZ));
	//printf("tVrp");
	//tVrp->printMatrix();

	/*STEP 2.2 R*/
	jMatrix* rZ = new jMatrix(ref.vpn);
	jMatrix* rX = new jMatrix(CART_DIM,1);
	jMatrix* rY = new jMatrix(CART_DIM,1);
	rZ->smult(1.0/(ref.vpn->norm()));	//Rz
	rX->crossProd(ref.vup,rZ);			//Rx
	rX->smult(1.0/rX->norm());
	rY->crossProd(rZ,rX);				//Ry
	jMatrix* R = new jMatrix(HOMO_DIM,HOMO_DIM,
		rX->at(MX), rX->at(MY), rX->at(MZ), 0.0,
		rY->at(MX), rY->at(MY), rY->at(MZ), 0.0,
		rZ->at(MX), rZ->at(MY), rZ->at(MZ), 0.0,
			   0.0,		   0.0,		   0.0,   W);
	rX->~jMatrix(); 
	rY->~jMatrix(); 
	rZ->~jMatrix();
	//printf("R");
	//R->printMatrix();

	/*STEP 2.3 T(-PRP)*/
	jMatrix* tPrp = new jMatrix(HOMO_DIM,HOMO_DIM);
	tPrp->setTranslate(-ref.prp->at(MX),-ref.prp->at(MY),-ref.prp->at(MZ));
	//printf("tPrp");
	//tPrp->printMatrix();

	/*STEP 2.4 SHPER*/
	double dopX =  ((double)(ref.uMax+ref.uMin)/2.0) - ref.prp->at(MX);
	double dopY =  ((double)(ref.vMax+ref.vMin)/2.0) - ref.prp->at(MY);
	double dopZ =  - ref.prp->at(MZ);
	double shX = - (dopX / dopZ);
	double shY = - (dopY / dopZ);
	jMatrix *shPer = new jMatrix(HOMO_DIM,HOMO_DIM,
		1.0, 0.0, shX, 0.0,
		0.0, 1.0, shY, 0.0,
		0.0, 0.0, 1.0, 0.0,
		0.0, 0.0, 0.0,   W);
	//printf("shPer");
	//shPer->printMatrix();

	/*STEP 2.5 SPER*/
	jMatrix *vrpNew = new jMatrix(4,1);	//first the VRP' vector is computed
	jMatrix *hVec = new jMatrix(4,1,
		0.0,0.0,0.0,W);
	vrpNew->mult(tPrp,hVec);
	vrpNew->mult(shPer,vrpNew);
	//vrpNew->printMatrix();
	//printf("vrpNew");
	//vrpNew->printMatrix();
	jMatrix *sPer = new jMatrix(HOMO_DIM,HOMO_DIM);
	// Sper = ( 2VRP'z / [(umax-umin)(VRP'z+B)] , 2VRP'z / [(vmax-vmin)(VRP'z+B)], -1 / (VRP'z+B))
	sPer->setScale( (double)(2.0*vrpNew->at(MZ)) / (double)( (ref.uMax-ref.uMin) * (vrpNew->at(MZ) + ref.backPlane)),
		(double)(2.0*vrpNew->at(MZ)) / (double)( (ref.vMax-ref.vMin) * (vrpNew->at(MZ) + ref.backPlane)),
		-((double)1.0 / (double)(vrpNew->at(MZ) + ref.backPlane)));
	vrpNew->~jMatrix();
	hVec->~jMatrix();
	//printf("sPer");
	//sPer->printMatrix();

	mCan->~jMatrix();
	mCan = new jMatrix(HOMO_DIM,HOMO_DIM);
	//MCan = Sper*ShPer*T(-PRP)*R*T(-VRP)
	mCan->mult(R,tVrp);
	mCan->mult(tPrp,mCan);
	mCan->mult(shPer,mCan);
	mCan->mult(sPer,mCan);

	tVrp->~jMatrix();
	R->~jMatrix();
	tPrp->~jMatrix();
	shPer->~jMatrix();
	sPer->~jMatrix();
}

void jPerspective::computeMvv3dv(){
	jMatrix *t1 = new jMatrix(HOMO_DIM,HOMO_DIM);
	t1->setTranslate(1,1,1);
	jMatrix *svv3dv = new jMatrix(HOMO_DIM,HOMO_DIM);
	// Svv3dv = S( (Xviewmax-Xviewmin)/2, (Yviewmax-Yviewmin)/2, (Zviewmax-Zviewmin)/1 )
	svv3dv->setScale((double)(view.xMax-view.xMin)/2.0,
		(double)(view.yMax-view.yMin)/2.0,
		(double)(view.zMax-view.zMin)/1.0);
	jMatrix *tV = new jMatrix(HOMO_DIM,HOMO_DIM);
	tV->setTranslate(view.xMin,view.yMin,view.zMin);


	Mvv3dv->~jMatrix();
	Mvv3dv = new jMatrix(HOMO_DIM,HOMO_DIM);
	Mvv3dv->mult(svv3dv,t1);
	Mvv3dv->mult(tV,Mvv3dv);
	//printf("Mvv3dv");
	//Mvv3dv->printMatrix();

	tV->~jMatrix();
	svv3dv->~jMatrix();
	t1->~jMatrix();
}

jMatrix *jPerspective::perspectiveTransform(jMatrix *p){
	//1) Cartesian -> Homogeneous
	//2) Transform to Canonical View Volume
	//3) Clip
	//4) Project
	//5) Scale to the window size and position
	//6) Homogeneous -> Cartesian

	jMatrix *temp, *res;

	/*STEP 1*/
	temp = p->homo();

	/*STEP 2*/
	// printf("before");
	// temp->printMatrix();
	temp->mult(mCan,temp);
	// printf("after");
	// temp->printMatrix();

	/*STEP 3*/
	// Perform clip
	//TODO

	/*STEP 4*/
	jMatrix *mPer = new jMatrix(HOMO_DIM, HOMO_DIM,
		1.0, 0.0, 0.0, 0.0,
		0.0, 1.0, 0.0, 0.0,
		0.0, 0.0, 1.0, 0.0,
		0.0, 0.0,-1.0, 0.0);
	temp->mult(mPer,temp);
	// printf("after mPer");
	// temp->printMatrix();

	/*STEP 5*/
	temp->mult(Mvv3dv,temp);
	// printf("after mvv3dv");
	// temp->printMatrix();

	/*STEP 6*/
	res = temp->cartesian();
	return res;
}

void jPerspective::project(){
	vrcLines.clear();
	computeMCan();
	computeMvv3dv();
	for (std::vector<Line3D>::iterator i = wcLines.begin(); i != wcLines.end(); ++i){
		jMatrix *r0, *r1;
		jMatrix *p0 = new jMatrix(CART_DIM, 1,
			i->x0,i->y0,i->z0);
		// printf("before");
		// p0->printMatrix();
		r0 = perspectiveTransform(p0);
		// printf("after");
		// r0->printMatrix();
		jMatrix *p1 = new jMatrix(CART_DIM, 1,
			i->x1,i->y1,i->z1);
		// printf("before");
		// p1->printMatrix();
		r1 = perspectiveTransform(p1);
		// printf("after");
		// r1->printMatrix();
		Line2D l;
		l.x0 = r0->at(MX);
		l.y0 = r0->at(MY);
		l.x1 = r1->at(MX);
		l.y1 = r1->at(MY);
		l.r = i->r;
		l.g = i->g;
		l.b = i->b;
		vrcLines.push_back(l);
		p0->~jMatrix();
		p1->~jMatrix();
	}
}

void jPerspective::addLine(Line3D line){
	wcLines.push_back(line);
}

void jPerspective::draw(){
	for (std::vector<Line2D>::iterator i = vrcLines.begin(); i != vrcLines.end(); ++i){
		glColor3d(i->r,i->g,i->b);
		glBegin(GL_LINES);
		glVertex2d(i->x0,i->y0);
		glVertex2d(i->x1,i->y1);
		glEnd();
	}
}

void jPerspective::setU(int min, int max){
	ref.uMin = min;
	ref.uMax = max;
}

void jPerspective::setV(int min, int max){
	ref.vMin = min;
	ref.vMax = max;
}

void jPerspective::setPlanes(int front, int back){
	ref.frontPlane = front;
	ref.backPlane = back;
}

void jPerspective::setVpn(jMatrix* m){
	ref.vpn->~jMatrix();
	ref.vpn = new jMatrix(m);
}

void jPerspective::setVup(jMatrix* m){
	ref.vup->~jMatrix();
	ref.vup = new jMatrix(m);
}

void jPerspective::setVrp(jMatrix* m){
	ref.vrp->~jMatrix();
	ref.vrp = new jMatrix(m);
}

void jPerspective::setPrp(jMatrix* m){
	ref.prp->~jMatrix();
	ref.prp = new jMatrix(m);
}

void jPerspective::setViewPort(int xMax, int xMin, int yMax, int yMin, int zMax, int zMin){
	view.xMax = xMax;
	view.xMin = xMin;
	view.yMax = yMax;
	view.yMin = yMin;
	view.zMax = zMax;
	view.zMin = zMin;
}

void jPerspective::rotateWorld(double cx, double cz, double cy, double theta, int axis){
	/*Compute the rotation matrix*/
	jMatrix *m = new jMatrix(HOMO_DIM,HOMO_DIM);
	jMatrix *m2 = new jMatrix(HOMO_DIM,HOMO_DIM);
	jMatrix *m3 = new jMatrix(HOMO_DIM,HOMO_DIM);
	m->setTranslate(-cx,-cy,-cz);
	m2->setRotate(theta,axis);
	m3->setTranslate(cx,cy,cz);
	m->mult(m2,m);
	m->mult(m3,m);
	std::vector<Line3D> temp;
	for (std::vector<Line3D>::iterator i = wcLines.begin(); i != wcLines.end(); ++i){
		jMatrix *p0 = new jMatrix(HOMO_DIM, 1,
			i->x0,i->y0,i->z0,1.0);
		p0->mult(m,p0);
		jMatrix *p1 = new jMatrix(HOMO_DIM, 1,
			i->x1,i->y1,i->z1,1.0);
		p1->mult(m,p1);
		Line3D l;
		l.x0 = p0->at(MX);
		l.y0 = p0->at(MY);
		l.z0 = p0->at(MZ);
		l.x1 = p1->at(MX);
		l.y1 = p1->at(MY);
		l.z1 = p1->at(MZ);
		l.r = i->r;
		l.g = i->g;
		l.b = i->b;
		temp.push_back(l);
		p0->~jMatrix();
		p1->~jMatrix();
	}
	wcLines = temp;
	m->~jMatrix();
	m2->~jMatrix();
	m3->~jMatrix();
}