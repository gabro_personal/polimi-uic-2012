/*
** CS 488 � Fall 2012 
** Project 2 - Design a Play Room
**
** Giacomo Tagliabue
**
** Compiled on windows 7 - Visual Studio 2010
*/

#include <stdlib.h>
#include <stdio.h>

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include <iostream>
#include <math.h>
#include "reader.h"
#include "jMatrix.h"
#include "jPerspective.h"

/*Macros*/
#define WINDOW_SIZE_X 800
#define WINDOW_SIZE_Y 800
#define BACKGROUND_COLOR 0,0,0,0
#define J_WHITE 1,1,1
#define J_BLACK 0,0,0
#define J_RED 1,0,0
#define J_GREEN 0,1,0
#define J_BLUE 0,0,1
#define J_PI 3.1415926535897932384626433832795

/*GLOBALS*/
jPerspective *per;

jMatrix *vpn;
jMatrix *vrp;
jMatrix *vup;
jMatrix *prp;

int view = 0;

Line3D generateLine(double x0, double y0, double z0, double x1, double y1, double z1, double r, double g, double b){
	Line3D l;
	l.x0=x0;
	l.y0=y0;
	l.z0=z0;
	l.x1=x1;
	l.y1=y1;
	l.z1=z1;
	l.r=r;
	l.g=g;
	l.b=b;
	return l;
}

std::vector<Line3D> generateCilinder(double cx, double cy, double cz,
	double radius, double height, int sides, double r, double g, double b)
{
	std::vector<Line3D> v;
	double inc = (double)(J_PI*2)/(double)sides;
	for (int i = 0; i < sides; i++){
		Line3D l1,l2,l3;
		l1 = generateLine(cos(i*inc)*radius+cx,cy,sin(i*inc)*radius+cz,
			cos(i*inc)*radius+cx,cy+height,sin(i*inc)*radius+cz,r,g,b);
		v.push_back(l1);
		l2 = generateLine(cos(i*inc)*radius+cx,cy,sin(i*inc)*radius+cz,
			cos((i+1)*inc)*radius+cx,cy,sin((i+1)*inc)*radius+cz,r,g,b);
		v.push_back(l2);
		l3 = generateLine(cos(i*inc)*radius+cx,cy+height,sin(i*inc)*radius+cz,
			cos((i+1)*inc)*radius+cx,cy+height,sin((i+1)*inc)*radius+cz,r,g,b);
		v.push_back(l3);
	}
	return v;
}

std::vector<Line3D> generateSphere(double cx, double cy, double cz, double radius,
	int sides, double r, double g, double b){
	std::vector<Line3D> vector;
	double inc = (double)(J_PI*2)/(double)sides;
	for (int i = 0; i < sides; i++){

	}
	return v;
}

/*Function for handling the keyboard interrupts*/
void Keyboard(unsigned char key, int x, int y)
{
	switch (key){
	case 'r': /*ROTATE*/
			per->rotateWorld(20,20,20,0.09,2);
			per->project();
		break;
	case 't': /*TOGGLE VIEW*/
		switch(view){
		case 0 : /*FRONT*/
			printf("FRONT VIEW SELECTED\n");
			vrp->~jMatrix();
			vup->~jMatrix();
			vpn->~jMatrix();
			vrp = new jMatrix(4,1,
				20.0,20.0,40.0,1.0);
			vpn = new jMatrix(3,1,
				0.0,sin(0.3),cos(0.3));
			vup = new jMatrix(3,1,
				0.0,1.0,0.0);
			per->setVup(vup);
			per->setVrp(vrp);
			per->setVpn(vpn);
			per->project();
			view++;
			break;
		case 1 : /*SIDE*/
			printf("SIDE VIEW SELECTED\n");
			vrp->~jMatrix();
			vpn->~jMatrix();
			vup->~jMatrix();
			vrp = new jMatrix(4,1,
				40.0,20.0,20.0,1.0);
			vpn = new jMatrix(3,1,
				cos(0.3),sin(0.3),0);
			vup = new jMatrix(3,1,
				0.0,1.0,0.0);
			per->setVup(vup);
			per->setVrp(vrp);
			per->setVpn(vpn);
			per->project();
			view++;
			break;
		case 2 : /*TOP*/
			printf("TOP VIEW SELECTED\n");
			vrp->~jMatrix();
			vpn->~jMatrix();
			vrp = new jMatrix(4,1,
				20.0,50.0,33.0,1.0);
			vpn = new jMatrix(3,1,
				0.0,1.0,0.0);
			vup = new jMatrix(3,1,
				0.0,0.0,1.0);
			per->setVup(vup);
			per->setVrp(vrp);
			per->setVpn(vpn);
			per->project();
			view++;
			break;
		case 3 : /*USER DEFINED*/
			printf("USER VIEW SELECTED\n");
			view = 0;
			break;
		}
		break;
	case 'i': /*ZOOM IN*/
		vrp->set(0,0,vrp->at(0,0)-(0.5*vpn->at(0,0)));
		vrp->set(1,0,vrp->at(1,0)-(0.5*vpn->at(1,0)));
		vrp->set(2,0,vrp->at(2,0)-(0.5*vpn->at(2,0)));
		per->setVrp(vrp);
		per->project();
		break;
	case 'o': /*ZOOM OUT*/
		vrp->set(0,0,vrp->at(0,0)+(0.5*vpn->at(0,0)));
		vrp->set(1,0,vrp->at(1,0)+(0.5*vpn->at(1,0)));
		vrp->set(2,0,vrp->at(2,0)+(0.5*vpn->at(2,0)));
		per->setVrp(vrp);
		per->project();
		break;
	}
}

void display(void) {
	/* clear the screen to the clear color */
	glClear(GL_COLOR_BUFFER_BIT);

	/*Draw! */
	per->draw();

	/* swap buffers */
	glutSwapBuffers();
}

void reshape (int w, int h) {
	/* set the viewport */
	glViewport (0, 0, (GLsizei) w, (GLsizei) h);

	/* Matrix for projection transformation */
	glMatrixMode (GL_PROJECTION); 

	/* replaces the current matrix with the identity matrix */
	glLoadIdentity ();

	/* Define a 2d orthographic projection matrix */
	gluOrtho2D (0.0, (GLdouble) w, 0.0, (GLdouble) h);
}

const char * filename = "project2.xml";  // Opens the XML file based on the entire pathname "filename"


void readDocument(){
	printf("file name %s \n", filename);
	openFile(filename);
	parseDatabase();
	printf("Printing out database \n");
	printDatabase();
}

void setPerspRef(){
	/*Set the references for the perspective projection*/
	per = new jPerspective();
	per->setU(-12,12);
	per->setV(-10,10);
	per->setPlanes(10,-50);
	vpn = new jMatrix(3,1,
		0.0,sin(0.3),cos(0.3));
	per->setVpn(vpn);
	
	vup = new jMatrix(3,1,
		0.0,1.0,0.0);
	per->setVup(vup);
	
	vrp = new jMatrix(4,1,
		20.0,20.0,40.0,1.0);
	per->setVrp(vrp);
	
	prp = new jMatrix(3,1,
		0.0,0.0,20.0);
	per->setPrp(prp);
	
	per->setViewPort(WINDOW_SIZE_X,0,WINDOW_SIZE_Y,0,0,100);
}

void loadObjects(){
	/*WALLS*/
	Line3D w[12];
	w[0] = generateLine(0,0,0,40,0,0,J_WHITE);
	w[1] = generateLine(40,0,0,40,0,40,J_WHITE);
	w[2] = generateLine(40,0,40,0,0,40,J_WHITE);
	w[3] = generateLine(0,0,40,0,0,0,J_WHITE);
	w[4] = generateLine(0,15,0,40,15,0,J_WHITE);
	w[5] = generateLine(40,15,0,40,15,40,J_WHITE);
	w[6] = generateLine(40,15,40,0,15,40,J_WHITE);
	w[7] = generateLine(0,15,40,0,15,0,J_WHITE);
	w[8] = generateLine(0,0,0,0,15,0,J_WHITE);
	w[9] = generateLine(0,0,40,0,15,40,J_WHITE);
	w[10] = generateLine(40,0,40,40,15,40,J_WHITE);
	w[11] = generateLine(40,0,0,40,15,0,J_WHITE);
	for (int i = 0; i < 12; i++){
		per->addLine(w[i]);
	}
	/*MIRROR*/
	Line3D m[4];
	m[0] = generateLine(31,0,0,36,0,0,J_WHITE);
	m[1] = generateLine(36,0,0,36,12,0,J_WHITE);
	m[2] = generateLine(36,12,0,31,12,0,J_WHITE);
	m[3] = generateLine(31,12,0,31,0,0,J_WHITE);
	for (int i = 0; i < 4; i++){
		per->addLine(m[i]);
	}
	/*TABLE*/
	std::vector<Line3D> v;
	v = generateCilinder(30,0,16,1.5,3,8,J_WHITE);
	for (std::vector<Line3D>::iterator i = v.begin(); i != v.end(); ++i){
		per->addLine(*i);
	}
	Line3D t[12];
	t[0] = generateLine(34,3,16,30,3,20,J_WHITE);
	t[1] = generateLine(30,3,20,26,3,16,J_WHITE);
	t[2] = generateLine(26,3,16,30,3,12,J_WHITE);
	t[3] = generateLine(30,3,12,34,3,16,J_WHITE);
	t[4] = generateLine(34,4,16,30,4,20,J_WHITE);
	t[5] = generateLine(30,4,20,26,4,16,J_WHITE);
	t[6] = generateLine(26,4,16,30,4,12,J_WHITE);
	t[7] = generateLine(30,4,12,34,4,16,J_WHITE);
	t[8] = generateLine(34,3,16,34,4,16,J_WHITE);
	t[9] = generateLine(30,3,20,30,4,20,J_WHITE);
	t[10] = generateLine(26,3,16,26,4,16,J_WHITE);
	t[11] = generateLine(30,3,12,30,4,12,J_WHITE);
	for (int i = 0; i < 12; i++){
		per->addLine(t[i]);
	}
	/*TOY*/
	Line3D g[8];
	g[0] = generateLine(8,0,27,12,0,28,J_WHITE);
	g[1] = generateLine(12,0,28,11,0,32,J_WHITE);
	g[2] = generateLine(11,0,32,7,0,31,J_WHITE);
	g[3] = generateLine(7,0,31,8,0,27,J_WHITE);
	g[4] = generateLine(7,0,31,9.5,3,29.5,J_WHITE);
	g[5] = generateLine(11,0,32,9.5,3,29.5,J_WHITE);
	g[6] = generateLine(12,0,28,9.5,3,29.5,J_WHITE);
	g[7] = generateLine(8,0,27,9.5,3,29.5,J_WHITE);
	for (int i = 0; i < 8; i++){
		per->addLine(g[i]);
	}
	/*LAMP*/
	Line3D l[9];
	l[0] = generateLine(18.5,8,17.5,22.5,8,18.5,J_WHITE);
	l[1] = generateLine(22.5,8,18.5,21.5,8,22.5,J_WHITE);
	l[2] = generateLine(21.5,8,22.5,17.5,8,21.5,J_WHITE);
	l[3] = generateLine(17.5,8,21.5,18.5,8,17.5,J_WHITE);
	l[4] = generateLine(17.5,8,21.5,20,11,20,J_WHITE);
	l[5] = generateLine(21.5,8,22.5,20,11,20,J_WHITE);
	l[6] = generateLine(22.5,8,18.5,20,11,20,J_WHITE);
	l[7] = generateLine(18.5,8,17.5,20,11,20,J_WHITE);
	l[8] = generateLine(20,11,20,20,15,20,J_WHITE);
	for (int i = 0; i < 9; i++){
		per->addLine(l[i]);
	}
}

int main(int argc, char** argv) {

	/* deal with any GLUT command Line options */
	glutInit(&argc, argv);

	/* create an output window */
	glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);
	glutInitWindowSize(WINDOW_SIZE_X,WINDOW_SIZE_Y);

	/***
	** MY CODE
	***/
	readDocument();
	setPerspRef();
	loadObjects();
	per->project();


	/* set the name of the window and try to create it */
	glutCreateWindow("Project 2");

	/* specify clear values for the color buffers */
	glClearColor (BACKGROUND_COLOR);

	/* Receive keyboard inputs */
	glutKeyboardFunc (Keyboard);

	/* assign the display function */
	glutDisplayFunc(display);

	/* assign the idle function */
	glutIdleFunc(display);

	/* sets the reshape callback for the current window */
	glutReshapeFunc(reshape);

	/* enters the GLUT event processing loop */
	glutMainLoop();

	return (EXIT_SUCCESS);
}