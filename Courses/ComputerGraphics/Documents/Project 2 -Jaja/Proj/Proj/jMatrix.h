#pragma once

#define W 1.0
#define MX 0,0
#define MY 1,0
#define MZ 2,0
#define CART_DIM 3
#define HOMO_DIM 4

/***
 * Matrix of doubles.
 * The dimension of the matrix is constant and cannot be changed
***/
class jMatrix
{
public:
	/***
	** Creates a new matrix of dimension r x c.
	** Additional parameters will be used to initialize the matrix
	** (WARNING: the additional parameters must be explicitly casted as double!)
	***/
	jMatrix::jMatrix(int r, int c, ...);

	/***
	** Creates a new matrix copying the values of the given matrix
	***/
	jMatrix::jMatrix(jMatrix* src);

	~jMatrix(void);
	/***
	** Set the matrix equals to the identity matrix
	** Throws an exception if the matrix is not squared
	***/
	void identity();

	/***
	** Returns the value at the specified row and column
	***/
	double at(int r, int c);

	/***
	** Set the value at the specified row and column
	***/
	void set(int r, int c, double val);

	/***
	** Prints the matrix on the stdout, used for debugging
	***/
	void printMatrix();

	/***
	** Set the matrix as a 3D translation matrix
	***/
	void setTranslate(double dx, double dy, double dz);

	/***
	** Set the matrix as a 3D rotation matrix
	** Axis can be: 1=x, 2=y, 3=z
	***/
	void setRotate(double theta, int axis);

	/***
	** Set the matrix as a 3D scaling matrix
	***/
	void setScale(double sx, double sy, double sz);

	/***
	** Stores in the matrix the result of the multiplication
	** The three matrices must have the compatible dimensions or an exception is thrown
	***/
	void mult(jMatrix* A, jMatrix *B);

	/***
	** Return the norm of the vector
	***/
	double norm();

	/***
	** Multiply every element of the matrix with the given scalar
	***/
	void smult(double val);

	/***
	** Stores in the vector the result of the crossProduct
	** The three matrices must have dimension 3 x 1 or an exception is thrown
	***/
	void crossProd(jMatrix* A, jMatrix* B);

	/***
	** Returns the Matrix in homogeneous coordinates
	** The matrix must be 3 x 3 or 3 x 1
	***/
	jMatrix *homo();

	/***
	** Returns the Matrix in cartesian coordinates
	** The matrix must be 4 x 4 or 4 x 1
	***/
	jMatrix *cartesian();

	/***
	** Get the number of rows
	***/
	int rows(){
		return _rows;
	}

	/***
	** Get the number of columns
	***/
	int columns(){
		return _columns;
	}

private:
	int _rows, _columns;
	double **M;
};

