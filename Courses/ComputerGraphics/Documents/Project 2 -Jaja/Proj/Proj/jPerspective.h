#pragma once
#include <vector>
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include "jMatrix.h"

#define J_LINE 1

/***
** This struct represent a 3D line with color information
***/
typedef struct Line3D{
	/*start point*/
	double x0;
	double y0;
	double z0;

	/*end point*/
	double x1;
	double y1;
	double z1;

	/*color*/
	double r;
	double g;
	double b;
}Line3D;

/***
** This struct represent a 2D line with color information
***/
typedef struct Line2D{
	/*start point*/
	double x0;
	double y0;

	/*end point*/
	double x1;
	double y1;

	/*color*/
	double r;
	double g;
	double b;
}Line2D;

/***
** This structure contains all the information needed to build the VRC and the perspective transformations
***/
typedef struct ref_t{
	int uMin, uMax, vMin, vMax; //bounds of viewing window in [VRC]
	int frontPlane, backPlane;	//clipping planes of perspective canonical viewing volume
	jMatrix *vpn, *vup;			//define VRC axes in [WC]
	jMatrix *vrp;				//define VRC origin in [WC]
	jMatrix *prp;				//Center of projection in [VRC]
}ref_t;

typedef struct viewport_t
{
	int xMax, xMin, yMax, yMin, zMax, zMin;
}viewport_t;

class jPerspective
{
public:
	jPerspective(void);
	~jPerspective(void);

	/***
	** Take all the input objects and perform all the transformations needed to draw them
	***/
	void project();

	/***
	** add a line to the list of lines to be displayed
	***/
	void addLine(Line3D line);

	/***
	** Draws all the objects
	***/
	void draw();

	/***
	** Set uMin and uMax
	***/
	void setU(int min, int max);

	/***
	** Set vMin and vMax
	***/
	void setV(int min, int max);

	/***
	** Set the VPN
	***/
	void setVpn(jMatrix* m);

	/***
	** Set the VUP
	***/
	void setVup(jMatrix* m);

	/***
	** Set the VRP
	***/
	void setVrp(jMatrix* m);

	/***
	** Set the PRP
	***/
	void setPrp(jMatrix* m);

	/***
	** Set the fornt and back clipping planes
	***/
	void setPlanes(int front, int back);

	/***
	** set the viewport coordinates
	***/
	void setViewPort(int xMax, int xMin, int yMax, int yMin, int zMax, int zMin);

	/***
	** rotate all the lines in the world along the specified center for the specified angle
	***/
	void rotateWorld(double cx, double cz, double cy, double theta, int axis);

private:
	/***
	** struct containing all the references for the perspective transformation
	***/
	ref_t ref;

	/***
	** struct containing the viewport size for the perspective transformation
	***/
	viewport_t view;

	/***
	** Canonicalize matrix
	***/
	jMatrix *mCan;

	/***
	** translation and scaling matrix
	***/
	jMatrix *Mvv3dv;

	/***
	** List of all the lines before being transformed to VRC space
	***/
	std::vector<Line3D> wcLines;

	/***
	** List of all the lines after being transformed to VRC space
	***/
	std::vector<Line2D> vrcLines;

	/***
	** compute the MCan matrix used to transform into the canonical view
	***/
	void computeMCan();

	/***
	** compute the Mvv3dv matrix used to transform into the viewport volume
	***/
	void computeMvv3dv();

	/***
	** Transform from the World coordinates to the View reference coordinates in the canoncial view
	***/
	jMatrix *perspectiveTransform(jMatrix *p);
};