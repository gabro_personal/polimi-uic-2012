#include "jMatrix.h"
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <math.h>

jMatrix::jMatrix(int r, int c, ...){
	_rows = r;
	_columns = c;
	M = new double*[_rows];
	for(int i=0; i<_rows; i++){
		M[i] = new double [_columns];
	}
	/*checks if there are additional parameters and fills the matrix with them*/
	va_list ap;
	va_start(ap,c);
	for(int i=0; i < _rows; i++){
		for (int j = 0; j < _columns; ++j){
			M[i][j] = va_arg(ap,double);
		}
	}
	va_end(ap);
}

jMatrix::jMatrix(jMatrix* src){
	_rows = src->rows();
	_columns = src->columns();
	M = new double*[_rows];
	for(int i=0; i<_rows; i++){
		M[i] = new double [_columns];
	}
	for (int i = 0; i < _rows; i++){
		for (int j = 0; j < _columns; j++){
			M[i][j] = src->at(i,j);
		}
	}
}

jMatrix::~jMatrix(void)
{
	for (int i=0; i<_rows; ++i) {
		delete[] M[i];
	}
	delete[] M;
}

void jMatrix::identity()
{
	if (_rows != _columns){
		throw "The matrix must be squared to transform it to the identity matrix";
	}
	else{
		for (int i=0; i<_rows; i++){
			for (int j=0; j<_columns; j++){
				if (i==j)
					M[i][j] = 1;
				else
					M[i][j] = 0;
			}
		}
	}
}

double jMatrix::at(int r, int c){
	if (r < _rows && c < _columns){
		return M[r][c];
	}
	else{
		throw "indexes out of bound";
	}
}

void jMatrix::setTranslate(double dx, double dy, double dz){
	if (_rows == HOMO_DIM && _columns == HOMO_DIM){
		this->identity();
		M[0][3]=dx;
		M[1][3]=dy;
		M[2][3]=dz;
	}
	else{
		throw "Matrix must be 4 x 4!";
	}													
}

void jMatrix::setRotate(double theta, int axis){
	if (_rows == HOMO_DIM && _columns == HOMO_DIM){
		this->identity();
		switch(axis){
		case 1 : /*x*/
			M[1][1] = cos(theta);
			M[1][2] = -sin(theta);
			M[2][1] = sin(theta);
			M[2][2] = cos(theta);
			break;
		case 2 : /*y*/
			M[0][0] = cos(theta);
			M[0][2] = sin(theta);
			M[2][0] = -sin(theta);
			M[2][2] = cos(theta);
			break;
		case 3 : /*z*/
			M[0][0] = cos(theta);
			M[0][1] = -sin(theta);
			M[1][0] = sin(theta);
			M[1][1] = cos(theta);
			break;
		default : throw "invalid axis input!"; break;
		}
	}
	else{
		throw "Matrix must be 4 x 4!";
	}													
}

void jMatrix::setScale(double sx, double sy, double sz){
	if (_rows == HOMO_DIM && _columns == HOMO_DIM){
		this->identity();
		M[0][0] = sx;
		M[1][1] = sy;
		M[2][2] = sz;
	}
	else{
		throw "Matrix must be 4 x 4!";
	}													
}

void jMatrix::set(int r, int c, double val){
	if (r < _rows && c < _columns){
		M[r][c]=val;
	}
	else{
		throw "indexes out of bound";
	}
}

void jMatrix::printMatrix(){
	printf("------ Matrix ------\n");
		for (int i = 0; i < _rows; i++){
			for (int j = 0; j < _columns; j++){
				printf("%f ", M[i][j]);
			}
			printf("\n");
		}
	}

void jMatrix::mult(jMatrix* A, jMatrix *B){
	//Check if the dimensions are compatible
	if( A->rows() != this->rows() ||
		A->columns() != B->rows() ||
		B->columns() != this->columns()){
			throw "Mismathced dimensions!";
	}
	else{
		jMatrix *res = new jMatrix(_rows,_columns);
		for (int i = 0; i < this->rows(); i++){
			for (int j = 0; j < this->columns(); j++){
				double sum = 0;
				for (int k = 0; k < A->columns(); k++){
					sum += A->at(i,k) * B->at(k,j);
				}
				res->set(i,j,sum);
			}
		}
		for (int i = 0; i < this->rows(); i++){
			for (int j = 0; j < this->columns(); j++){
				this->set(i,j,res->at(i,j));
			}
		}
		res->~jMatrix();
	}
}

double jMatrix::norm(){
	if (_columns == 1){
		double tot = 0;
		for (int i = 0; i < _rows; i++){
			tot += M[i][0] * M[i][0];
		}
		tot = sqrt(tot);
		return tot;
	}
	else{
		throw "The matrix must be a vector to get the norm!";
	}
}

void jMatrix::smult(double val){
	for (int i = 0; i < _rows; i++){
		for (int j = 0; j < _columns; j++){
			M[i][j] = M[i][j] * val;
		}
	}
}

void jMatrix::crossProd(jMatrix* A, jMatrix *B){
	if (_columns == 1 && A->columns() == 1 && B->columns() == 1 &&
		_rows == 3 && A->rows() == 3 && B->rows() == 3){
		M[0][0] = A->at(MY) * B->at(MZ) - A->at(MZ) * B->at(MY);
		M[1][0] = A->at(MZ) * B->at(MX) - A->at(MX) * B->at(MZ);
		M[2][0] = A->at(MX) * B->at(MY) - A->at(MY) * B->at(MX);
	}
	else{
		throw "Wrong dimensions for the cross product!";
	}
}

jMatrix *jMatrix::homo(){
	if (_rows == CART_DIM && _columns == CART_DIM){	//if it's a cartesian matrix
		return new jMatrix(HOMO_DIM,HOMO_DIM,
			this->at(0,0), this->at(0,1), this->at(0,2), .0,
			this->at(1,0), this->at(1,1), this->at(1,2), .0,
			this->at(2,0), this->at(2,1), this->at(2,2), .0,
			          .0,             .0,            .0,  W);
	}
	else if (_rows == CART_DIM && _columns == 1){	//if it's a cartesian vector
		return new jMatrix(HOMO_DIM,1,
			this->at(MX),
			this->at(MY),
			this->at(MZ),
			           W);
	}
	else{											//if it's neither a cartesian vector or matrix
		throw "Matrix must be 3 x 3 or 3 x 1!";
	}
}

jMatrix *jMatrix::cartesian(){
	if (_rows == HOMO_DIM && _columns == HOMO_DIM){	//if it's a homogeneous matrix
		double w = this->at(HOMO_DIM-1, HOMO_DIM-1);
		return new jMatrix(CART_DIM,CART_DIM,
			this->at(0,0)/w, this->at(0,1)/w, this->at(0,2)/w,
			this->at(1,0)/w, this->at(1,1)/w, this->at(1,2)/w,
			this->at(2,0)/w, this->at(2,1)/w, this->at(2,2)/w);
	}
	else if (_rows == HOMO_DIM && _columns == 1){	//if it's a homogeneous vector
		double w = this->at(HOMO_DIM-1, 0);
		return new jMatrix(CART_DIM,1,
			this->at(MX)/w,
			this->at(MY)/w,
			this->at(MZ)/w);
	}
	else{											//if it's neither a homogeneous vector or matrix
		throw "Matrix must be 3 x 3 or 3 x 1!";
	}
}