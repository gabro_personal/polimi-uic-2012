#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <list>

#include "reader.h"

#include "CG_Common/CG_Debug/cg_debug.hh"
#include "CG_Engine/CG_2D/cg_drawing_primitives.hh"
#include "CG_Elements/CG_Routines/cg_draw_routines.hh"
#include "CG_Common/CG_Matrices/CG_Vector.hpp"
#include "CG_Common/CG_Matrices/CG_Matrix.hpp"
#include "CG_Engine/CG_3D/CG_Perspective_Projector.hpp"
#include "CG_Engine/CG_3D/CG_Projector.hpp"
#include "CG_Engine/CG_3D/CG_Parallel_Projector.hpp"
#include "CG_Common/CG_Matrices/cg_transformations.hh"
using namespace CGLib;

void init_data() {

	const char * filename = "/home/alberto/Repos/Git/cg_lab/CG_HW2/src/project2.xml";
	printf("file name %s \n", filename);
	openFile(filename);
	parseDatabase();
	printf("Printing out database \n");
	printDatabase();

}

static CG_Cartesian_f* vrp;
static CG_Cartesian_f* vpn;
static CG_Cartesian_f* vup;
static CG_Cartesian_f* prp;

static CG_Projector* par_pro;
static CG_Projector* per_pro;

static CG_Projector* current_projector = NULL;

static std::list<CG_HomPoint_f*> *shared_list;
static float shared_coord;

static CG_TransMatrix* r_mat;

void read_db_vectors(void) {
	vrp = new CG_Cartesian_f(vrc.vrp.x, vrc.vrp.y, vrc.vrp.z);
	vpn = new CG_Cartesian_f(vrc.vpn.x, vrc.vpn.y, vrc.vpn.z);
	vup = new CG_Cartesian_f(vrc.vup.x, vrc.vup.y, vrc.vup.z);
	prp = new CG_Cartesian_f(vrc.prp.x, vrc.prp.y, vrc.prp.z);
}

void init_projections(void) {

	read_db_vectors();
	par_pro = new CG_Parallel_Projector(vrp, vpn, vup, volume.uMax, volume.uMin, volume.vMax,
			volume.vMin, prp, volume.zMin + 10, volume.zMax - 10);
	per_pro = new CG_Perspective_Projector(vrp, vpn, vup, volume.uMax, volume.uMin, volume.vMax,
			volume.vMin, prp, volume.zMin + 10, volume.zMax - 10);

	shared_list = new std::list<CG_HomPoint_f*>();

	r_mat = get_rot_matrix_from_angles(0, 45, 0);
}

void clear_list(std::list<CG_HomPoint_f*>* list) {
	std::list<CG_HomPoint_f*>::iterator it;
	if (list == NULL) {
		return;
	}
	for (it = list->begin(); it != list->end(); it++) {
		delete *it;
	}
	list->clear();
}

void delete_projections(void) {
	delete vrp;
	delete vpn;
	delete vup;
	delete prp;
	delete par_pro;
	delete per_pro;
	clear_list(shared_list);
	delete shared_list;
	delete r_mat;
}

void draw_list(std::list<CG_HomPoint_f*>& result) {
	CG_HomPoint_f *beg, *end;
	std::list<CG_HomPoint_f*>::iterator it;
	if (result.size() == 0) {
		return;
	}
	it = result.begin();
	beg = *it;
	it++;
	for (; it != result.end(); it++) {
		end = *it;
		int xb = cg_roundf((*beg)[X_coord]);
		int yb = cg_roundf((*beg)[Y_coord]);
		int xe = cg_roundf((*end)[X_coord]);
		int ye = cg_roundf((*end)[Y_coord]);
		if (xb < 801 && xb >= 0 && yb < 801 && yb >= 0 && xe < 801 && xe >= 0 && ye < 801
				&& ye >= 0) {
			cg_draw_segment(xb, yb, xe, ye);
		}
		beg = end;
	}
	if (result.size() > 2) {
		end = *(result.begin());
		int xb = cg_roundf((*beg)[X_coord]);
		int yb = cg_roundf((*beg)[Y_coord]);
		int xe = cg_roundf((*end)[X_coord]);
		int ye = cg_roundf((*end)[Y_coord]);
		if (xb < 801 && xb >= 0 && yb < 801 && yb >= 0 && xe < 801 && xe >= 0 && ye < 801
				&& ye >= 0) {
			cg_draw_segment(xb, yb, xe, ye);
		}
	}
}

void draw_points(std::list<CG_HomPoint_f*>& result) {
	std::list<CG_HomPoint_f*>::iterator it;

	for (it = result.begin(); it != result.end(); it++) {
		glVertex2f((*(*it))[X_coord], (*(*it))[Y_coord]);
	}
}

void draw_picture_frame(void) {
	std::list<CG_HomPoint_f*> *list, *result;
	std::list<CG_HomPoint_f*>::iterator it;

	list = new std::list<CG_HomPoint_f*>();
	for (int i = 0; i < faceCount; i++) {
		CG_HomPoint_f* p = new CG_HomPoint_f(faceList[i].point.x, faceList[i].point.y,
				faceList[i].point.z);
		list->push_back(p);
	}

	result = current_projector->transform_polygon(*list);

	clear_list(list);
	delete list;

	draw_list(*result);

	clear_list(result);
	delete result;
}

void draw_room_borders(void) {
	std::list<CG_HomPoint_f*> *list, *result;
	std::list<CG_HomPoint_f*>::iterator it;

	list = new std::list<CG_HomPoint_f*>();

	/*
	 * list room vertices
	 */
	// back wall
	list->push_back(new CG_HomPoint_f(-5, 0, -40));
	list->push_back(new CG_HomPoint_f(60, 0, -40));
	list->push_back(new CG_HomPoint_f(60, 40, -40));
	list->push_back(new CG_HomPoint_f(-5, 40, -40));

	result = current_projector->transform_polygon(*list);
	draw_list(*result);
	clear_list(list);
	clear_list(result);
	delete result;

	// left wall
	list->push_back(new CG_HomPoint_f(-5, 0, -40));
	list->push_back(new CG_HomPoint_f(-5, 40, -40));
	list->push_back(new CG_HomPoint_f(-5, 40, -10));
	list->push_back(new CG_HomPoint_f(-5, 0, -10));

	result = current_projector->transform_polygon(*list);
	draw_list(*result);
	clear_list(list);
	clear_list(result);
	delete result;

	// floor
	list->push_back(new CG_HomPoint_f(-5, 0, -40));
	list->push_back(new CG_HomPoint_f(60, 0, -40));
	list->push_back(new CG_HomPoint_f(60, 0, -10));
	list->push_back(new CG_HomPoint_f(-5, 0, -10));

	result = current_projector->transform_polygon(*list);
	draw_list(*result);
	clear_list(list);
	clear_list(result);
	delete result;

	// ceiling
	list->push_back(new CG_HomPoint_f(-5, 40, -40));
	list->push_back(new CG_HomPoint_f(60, 40, -40));
	list->push_back(new CG_HomPoint_f(60, 40, -10));
	list->push_back(new CG_HomPoint_f(-5, 40, -10));

	result = current_projector->transform_polygon(*list);
	draw_list(*result);
	clear_list(list);
	clear_list(result);
	delete result;
	delete list;
}

void draw_mirror(void) {
	std::list<CG_HomPoint_f*> *list, *result;
	std::list<CG_HomPoint_f*>::iterator it;
	float ylow = 0;
	float yhigh = 30;
	float xleft = 40, xright = 55;
	float zfront = -39, zback = -40;

	// front surface
	list = new std::list<CG_HomPoint_f*>();
	list->push_back(new CG_HomPoint_f(xleft, ylow, zfront));
	list->push_back(new CG_HomPoint_f(xright, ylow, zfront));
	list->push_back(new CG_HomPoint_f(xright, yhigh, zfront));
	list->push_back(new CG_HomPoint_f(xleft, yhigh, zfront));

	result = current_projector->transform_polygon(*list);
	draw_list(*result);
	clear_list(list);
	clear_list(result);
	delete result;

	// left surface
	//list = new std::list<CG_HomPoint_f*>();
	list->push_back(new CG_HomPoint_f(xleft, ylow, zfront));
	list->push_back(new CG_HomPoint_f(xleft, ylow, zback));
	list->push_back(new CG_HomPoint_f(xleft, yhigh, zback));
	list->push_back(new CG_HomPoint_f(xleft, yhigh, zfront));

	result = current_projector->transform_polygon(*list);
	draw_list(*result);
	clear_list(list);
	clear_list(result);
	delete result;

	// right surface
	//list = new std::list<CG_HomPoint_f*>();
	list->push_back(new CG_HomPoint_f(xright, ylow, zfront));
	list->push_back(new CG_HomPoint_f(xright, ylow, zback));
	list->push_back(new CG_HomPoint_f(xright, yhigh, zback));
	list->push_back(new CG_HomPoint_f(xright, yhigh, zfront));

	result = current_projector->transform_polygon(*list);
	draw_list(*result);
	clear_list(list);
	clear_list(result);
	delete result;

	// back surface
	//list = new std::list<CG_HomPoint_f*>();
	list->push_back(new CG_HomPoint_f(xleft, ylow, zback));
	list->push_back(new CG_HomPoint_f(xright, ylow, zback));
	list->push_back(new CG_HomPoint_f(xright, yhigh, zback));
	list->push_back(new CG_HomPoint_f(xleft, yhigh, zback));

	result = current_projector->transform_polygon(*list);
	draw_list(*result);
	clear_list(list);
	clear_list(result);
	delete result;
	delete list;
}

void draw_table(void) {

	std::list<CG_HomPoint_f*> *list, *result;
	std::list<CG_HomPoint_f*>::iterator it;
	CG_HomPoint_f* p;
	int i;

	list = new std::list<CG_HomPoint_f*>();
	for (i = 0; i < 4; i++) {
		p = new CG_HomPoint_f(nodeList[i].point.x, nodeList[i].point.y, nodeList[i].point.z);
		list->push_back(p);
	}
	result = current_projector->transform_polygon(*list);
	draw_list(*result);
	clear_list(list);
	clear_list(result);
	delete result;

	//list = new std::list<CG_HomPoint_f*>();
	for (; i < 8; i++) {
		p = new CG_HomPoint_f(nodeList[i].point.x, nodeList[i].point.y, nodeList[i].point.z);
		list->push_back(p);
	}
	result = current_projector->transform_polygon(*list);
	draw_list(*result);
	clear_list(list);
	clear_list(result);
	delete result;

	// draw vertical edges
	for (i = 0; i < 4; i++) {
		p = new CG_HomPoint_f(nodeList[i].point.x, nodeList[i].point.y, nodeList[i].point.z);
		list->push_back(p);
		p = new CG_HomPoint_f(nodeList[i + 4].point.x, nodeList[i + 4].point.y,
				nodeList[i + 4].point.z);
		list->push_back(p);
		result = current_projector->transform_polygon(*list);
		draw_list(*result);
		clear_list(list);
		clear_list(result);
		delete result;
	}
	delete list;
}

void draw_chandalier(void) {

	std::list<CG_HomPoint_f*> *list, *result;
	std::list<CG_HomPoint_f*>::iterator it;
	CG_HomPoint_f* p;
	int i;
	list = new std::list<CG_HomPoint_f*>();

	// draw cord
	for (i = 8; i < 10; i++) {
		p = new CG_HomPoint_f(nodeList[i].point.x, nodeList[i].point.y, nodeList[i].point.z);
		list->push_back(p);
	}
	result = current_projector->transform_polygon(*list);
	draw_list(*result);
	clear_list(list);
	clear_list(result);
	delete result;

	// draw pyramid base
	for (i = 10; i < 14; i++) {
		p = new CG_HomPoint_f(nodeList[i].point.x, nodeList[i].point.y, nodeList[i].point.z);
		list->push_back(p);
	}
	result = current_projector->transform_polygon(*list);
	draw_list(*result);
	clear_list(list);
	clear_list(result);
	delete result;

	// draw vertical edges
	p = new CG_HomPoint_f(nodeList[9].point.x, nodeList[9].point.y, nodeList[9].point.z);
	list->push_back(p);
	for (i = 10; i < 14; i++) {
		p = new CG_HomPoint_f(nodeList[i].point.x, nodeList[i].point.y, nodeList[i].point.z);
		list->push_back(p);
		result = current_projector->transform_polygon(*list);
		draw_list(*result);
		list->pop_back();
		delete p;
		clear_list(result);
	}
	clear_list(list);
	delete list;
}

void draw_game(void) {

	std::list<CG_HomPoint_f*> *list, *result;
	std::list<CG_HomPoint_f*>::iterator it;
	CG_HomPoint_f* p;
	int i;
	list = new std::list<CG_HomPoint_f*>();
	float expand = 1.5;
	float zmove = 10;
	float xmove = 20;

	// draw pyramid base
	for (i = 10; i < 14; i++) {
		p = new CG_HomPoint_f(nodeList[i].point.x * expand + xmove, nodeList[i].point.y - 30,
				nodeList[i].point.z * expand + zmove);
		list->push_back(p);
	}
	result = current_projector->transform_polygon(*list);
	draw_list(*result);
	clear_list(list);
	clear_list(result);
	delete result;

	// draw vertical edges
	p = new CG_HomPoint_f(nodeList[9].point.x * expand + xmove, nodeList[9].point.y - 30,
			nodeList[9].point.z * expand + zmove);
	list->push_back(p);
	for (i = 10; i < 14; i++) {
		p = new CG_HomPoint_f(nodeList[i].point.x * expand + xmove, nodeList[i].point.y - 30,
				nodeList[i].point.z * expand + zmove);
		list->push_back(p);
		result = current_projector->transform_polygon(*list);
		draw_list(*result);
		list->pop_back();
		delete p;
		clear_list(result);
	}
	clear_list(list);
	delete list;
}

void store_circumference(int x, int y) {
	CG_HomPoint_f* p;
	p = new CG_HomPoint_f(float(x), shared_coord, float(y));
	shared_list->push_back(p);
}

void get_xz_circumference(int xc, int zc, unsigned int rad, float beg_angle, float end_angle,
		unsigned int scale) {
	std::list<CG_HomPoint_f*>::iterator it;
	cg_draw_circumference_call(0, 0, rad, beg_angle, end_angle, store_circumference);

	for (it = shared_list->begin(); it != shared_list->end(); it++) {
		float x = float(xc) + (*it)->get_el(X_coord) / (float(scale));
		(*it)->set_el(X_coord, x);
		float z = float(zc) + (*it)->get_el(Z_coord) / (float(scale));
		(*it)->set_el(Z_coord, z);
	}
}

void get_extrema(std::list<CG_HomPoint_f*>& result, CG_HomPoint_f** p1, CG_HomPoint_f** p2) {
	std::list<CG_HomPoint_f*>::iterator it;
	it = result.begin();
	*p1 = *it;
	*p2 = *p1;
	it++;
	for (; it != result.end(); it++) {
		if ((*(*it))[X_coord] < (*(*p1))[X_coord]) {
			*p1 = *it;
		} else if ((*(*it))[X_coord] > (*(*p2))[X_coord]) {
			*p2 = *it;
		}
	}
}

void draw_cylinder(void) {
	std::list<CG_HomPoint_f*> *base_down, *base_up /*, *edge1, *edge2*/;
	std::list<CG_HomPoint_f*>::iterator it;
//	CG_HomPoint_f *p1, *p2, *r1, *r2;
	CG_HomPoint_f *p1, *p2;
	std::list<CG_HomPoint_f*> *ledge = new std::list<CG_HomPoint_f*>();
	std::list<CG_HomPoint_f*> *redge = new std::list<CG_HomPoint_f*>();
	std::list<CG_HomPoint_f*> *vedges = new std::list<CG_HomPoint_f*>();
	std::list<CG_HomPoint_f*> *res;

	const unsigned int scale = 16;

	shared_coord = cylinder.center.y;
	get_xz_circumference(int(cylinder.center.x), int(cylinder.center.z),
			((unsigned int) (cylinder.radius)) * scale, 0, 360, scale);

	base_down = current_projector->transform_vertices(*shared_list);
	draw_points(*base_down);

	for (it = shared_list->begin(); it != shared_list->end(); it++) {
		(*it)->set_el(Y_coord, (*it)->get_el(Y_coord) + cylinder.height);
	}
	base_up = current_projector->transform_vertices(*shared_list);
	draw_points(*base_up);

	// draw lateral edges
	if (base_down->size() > 0 || base_up->size() > 0) {

		float max = -1000, min = 1000;
		for (it = shared_list->begin(); it != shared_list->end(); it++) {
			p1 = new CG_HomPoint_f((*(*it))[X_coord], (*(*it))[Y_coord], (*(*it))[Z_coord]);
			p2 = new CG_HomPoint_f((*(*it))[X_coord], (*(*it))[Y_coord] - cylinder.height,
					(*(*it))[Z_coord]);
			vedges->push_back(p1);
			vedges->push_back(p2);
			res = current_projector->transform_polygon(*vedges);
			clear_list(vedges);
			if (res->size() > 0) {
				if ((*(res->front()))[X_coord] < min) {
					min = (*(res->front()))[X_coord];
					clear_list(ledge);
					ledge = res;
				}
				if ((*(res->front()))[X_coord] > max) {
					max = (*(res->front()))[X_coord];
					clear_list(redge);
					redge = res;
				}
			}

		}

	}
	draw_list(*ledge);
	draw_list(*redge);

	clear_list(shared_list);
	clear_list(base_down);
	clear_list(base_up);
	clear_list(ledge);
	clear_list(redge);
	clear_list(vedges);
//	edge1->clear();
//	edge2->clear();

	delete base_down;
	delete base_up;
	delete vedges;
	delete ledge;
	delete redge;
//	delete edge1;
//	delete edge2;

}

static unsigned int sphere_scale;
static std::list<CG_HomPoint_f*> *extrema;

void draw_sphere_line(int x, int y) {

	/*
	 std::cout << "x: " << ((float(x) - sphere.center.x))
	 << ", y: " << (sphere.center.y + (float(y) - sphere.center.y))
	 << std::endl;
	 */

	std::list<CG_HomPoint_f*> *result;
	std::list<CG_HomPoint_f*>::iterator it;
	CG_HomPoint_f *p1, *p2;
	unsigned int rad;

	shared_coord = sphere.center.y + float(y) / float(sphere_scale);
	if (x > 0) {
		rad = (unsigned int) x;
	} else {
		rad = 0;
	}
	get_xz_circumference(int(sphere.center.x), int(sphere.center.z), rad, 0, 360, sphere_scale);

	result = current_projector->transform_vertices(*shared_list);
	clear_list(shared_list);

	if (result->size() > 0) {
		if (y != 0) {
			get_extrema(*result, &p1, &p2);
			extrema->push_back(p1);
			extrema->push_back(p2);
			draw_points(*extrema);
			extrema->clear();
		} else {
			draw_points(*result);
		}
	}

	clear_list(result);
	delete result;

}

void draw_sphere(void) {
	sphere_scale = 17;
	extrema = new std::list<CG_HomPoint_f*>();
	cg_draw_circumference_call(0, 0, ((unsigned int) (sphere.radius)) * sphere_scale, 0, 180,
			draw_sphere_line);
	delete extrema;
}

static bool pending_r = false;
static bool pending_t = false;
static bool pending_i = false;
static bool pending_o = false;
static bool pending_a = false;
static int r_ops = 0;

void rotate_vector(CG_Cartesian_f** v, CG_TransMatrix& rot) {
	CG_Vector<float> *v1, *v2;
	v1 = ((*v)->to_hom());
	v1->set_el(3, 0);
	v2 = v1;
	delete *v;
	v1 = rot * (*v1);
	delete v2;
	*v = new CG_Cartesian_f((*v1)[X_coord], (*v1)[Y_coord], (*v1)[Z_coord]);
	delete v1;
}

void ops_r(void) {
	r_ops++;
	if (r_ops == 8) {
		r_ops = 0;
		delete_projections();
		init_projections();
		return;
	}

	rotate_vector(&vrp, *r_mat);

	rotate_vector(&vpn, *r_mat);

	delete par_pro;
	par_pro = new CG_Parallel_Projector(vrp, vpn, vup, volume.uMax, volume.uMin, volume.vMax,
			volume.vMin, prp, volume.zMin, volume.zMax);
	delete per_pro;
	per_pro = new CG_Perspective_Projector(vrp, vpn, vup, volume.uMax, volume.uMin, volume.vMax,
			volume.vMin, prp, volume.zMin, volume.zMax);
}

static int toggle_pos = 0;

void ops_t(void) {
	toggle_pos++;
	if (toggle_pos == 4) {
		toggle_pos = 0;
	}
	switch (toggle_pos) {
		case 0:
			delete_projections();
			init_projections();
			break;
		case 2:
			// up view
			delete vrp;
			vrp = new CG_Cartesian_f(20, 45, -30);
			delete vpn;
			vpn = new CG_Cartesian_f(0, -1, 0);
			delete vup;
			vup = new CG_Cartesian_f(0, 0, 1);
			delete par_pro;
			par_pro = new CG_Parallel_Projector(vrp, vpn, vup, volume.uMax, volume.uMin,
					volume.vMax, volume.vMin, prp, volume.zMin, volume.zMax);
			delete per_pro;
			per_pro = new CG_Perspective_Projector(vrp, vpn, vup, volume.uMax, volume.uMin,
					volume.vMax, volume.vMin, prp, volume.zMin, volume.zMax);
			break;
		case 1:
			// left view
			delete vrp;
			vrp = new CG_Cartesian_f(-5, 20, -20);
			delete vpn;
			vpn = new CG_Cartesian_f(1, 0, 0);
			delete vup;
			vup = new CG_Cartesian_f(0, 1, 0);
			delete par_pro;
			par_pro = new CG_Parallel_Projector(vrp, vpn, vup, volume.uMax, volume.uMin,
					volume.vMax, volume.vMin, prp, volume.zMin, volume.zMax);
			delete per_pro;
			per_pro = new CG_Perspective_Projector(vrp, vpn, vup, volume.uMax, volume.uMin,
					volume.vMax, volume.vMin, prp, volume.zMin, volume.zMax);
			break;
		case 3:
			float vrpx, vrpy, vrpz, vpnx, vpny, vpnz, vupx, vupy, vupz;
			std::cout << "type coordinates of vrp,vpn and vup separated by space" << std::endl;
			scanf("%f %f %f %f %f %f %f %f %f", &vrpx, &vrpy, &vrpz, &vpnx, &vpny, &vpnz, &vupx,
					&vupy, &vupz);
			delete vrp;
			vrp = new CG_Cartesian_f(vrpx, vrpy, vrpz);
			delete vpn;
			vpn = new CG_Cartesian_f(vpnx, vpny, vpnz);
			delete vup;
			vup = new CG_Cartesian_f(vupx, vupy, vupz);
			delete par_pro;
			par_pro = new CG_Parallel_Projector(vrp, vpn, vup, volume.uMax, volume.uMin,
					volume.vMax, volume.vMin, prp, volume.zMin, volume.zMax);
			delete per_pro;
			per_pro = new CG_Perspective_Projector(vrp, vpn, vup, volume.uMax, volume.uMin,
					volume.vMax, volume.vMin, prp, volume.zMin + 10, volume.zMax - 5);
			break;
	}
}

static int zoom_in = 0;

void ops_i(void) {
	if (zoom_in < 6) {
		prp->set_el(Z_coord, prp->get_el(Z_coord) * float(1.1));
		zoom_in++;
		delete par_pro;
		par_pro = new CG_Parallel_Projector(vrp, vpn, vup, volume.uMax, volume.uMin, volume.vMax,
				volume.vMin, prp, volume.zMin + 10, volume.zMax - 10);
		delete per_pro;
		per_pro = new CG_Perspective_Projector(vrp, vpn, vup, volume.uMax, volume.uMin, volume.vMax,
				volume.vMin, prp, volume.zMin + 10, volume.zMax - 10);
	}
}

void ops_o(void) {
	if (zoom_in > -6) {
		prp->set_el(Z_coord, prp->get_el(Z_coord) / float(1.1));
		zoom_in--;
		delete par_pro;
		par_pro = new CG_Parallel_Projector(vrp, vpn, vup, volume.uMax, volume.uMin, volume.vMax,
				volume.vMin, prp, volume.zMin + 10, volume.zMax - 10);
		delete per_pro;
		per_pro = new CG_Perspective_Projector(vrp, vpn, vup, volume.uMax, volume.uMin, volume.vMax,
				volume.vMin, prp, volume.zMin + 10, volume.zMax - 10);
	}
}

void ops_a(void) {
	CG_TransMatrix* rot;

	int axis;
	std::cout << "type x, y or z for the axis you want to rotate around" << std::endl;
	/*
	 *  you have to type 'a'twice, because every two typing the inserted value is wrong:
	 *  maybe due to some behaviour of the GNU functions...
	 */

	axis = getchar();
	switch (axis) {

		case 'x': {
			rot = get_rot_matrix_from_angles(45, 0, 0);
			break;
		}

		case 'y': {
			rot = get_rot_matrix_from_angles(0, 45, 0);
			break;
		}

		case 'z': {
			rot = get_rot_matrix_from_angles(0, 0, 45);
			break;
		}

		default: {
			std::cout << "wrong axis" << std::endl;
			return;
		}

	}

	rotate_vector(&vrp, *rot);

	rotate_vector(&vpn, *rot);

	rotate_vector(&vup, *rot);

	delete par_pro;
	par_pro = new CG_Parallel_Projector(vrp, vpn, vup, volume.uMax, volume.uMin, volume.vMax,
			volume.vMin, prp, volume.zMin + 10, volume.zMax - 10);
	delete per_pro;
	per_pro = new CG_Perspective_Projector(vrp, vpn, vup, volume.uMax, volume.uMin, volume.vMax,
			volume.vMin, prp, volume.zMin + 10, volume.zMax - 10);
}

void check_pending_ops(void) {

	if (pending_r) {
		ops_r();
		pending_r = false;
	}
	if (pending_t) {
		ops_t();
		pending_t = false;
	}
	if (pending_i) {
		ops_i();
		pending_i = false;
	}
	if (pending_o) {
		ops_o();
		pending_o = false;
	}
	if (pending_a) {
		ops_a();
		pending_a = false;
	}
}

void Keyboard(unsigned char key, int x, int y) {
//cout<<key<<endl;
	switch (key) {
		case 27:       //ESCAPE key
			exit(0);
			break;

		case 'r':
			pending_r = true;
			break;

		case 't':
			pending_t = true;
			break;

		case 'i':
			pending_i = true;
			break;

		case 'o':
			pending_o = true;
			break;
		case 'a':
			pending_a = true;
			break;
	}
}

/******************************************************************/

void display(void) {
	/* clear the screen to the clear colour */
	glClear(GL_COLOR_BUFFER_BIT);

// draw a red square
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_POINTS);

// set the projector
	current_projector = per_pro;

	/*
	 * draw the back wall
	 */
	glColor3f(0.0, 0.0, 0.0);
	draw_room_borders();
	glColor3f(1.0, 0.5, 0.2);
	draw_picture_frame();
	glColor3f(0.0, 0.0, 1);
	draw_mirror();
	glColor3f(1.0, 1.0, 0.0);
	draw_table();
	glColor3f(1.0, 1.0, 0.0);
	draw_chandalier();
	glColor3f(1.0, 1.0, 0.0);
	draw_game();
	draw_cylinder();
	glColor3f(1.0, 0.0, 0.0);
	draw_sphere();

	glEnd();

	glColor3f(1.0, 1.0, 1.0);

	/* swap buffers */
	glutSwapBuffers();

	check_pending_ops();
}

void reshape(int w, int h) {
	/* set the viewport */
	glViewport(0, 0, (GLsizei) w, (GLsizei) h);

	/* Matrix for projection transformation */
	glMatrixMode(GL_PROJECTION);

	/* replaces the current matrix with the identity matrix */
	glLoadIdentity();

	/* Define a 2d orthographic projection matrix */
	gluOrtho2D(0.0, (GLdouble) w, 0.0, (GLdouble) h);
}

/*******************************************************************/

int main(int argc, char** argv) {

	/* deal with any GLUT command Line options */
	glutInit(&argc, argv);

	/* create an output window */
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(800, 800);

	/* set the name of the window and try to create it */
	glutCreateWindow("CS 488 - Assignment 2");

	/* specify clear values for the color buffers */
	glClearColor(1.0, 1.0, 1.0, 0.0);

	/* Receive keyboard inputs */
	glutKeyboardFunc(Keyboard);

	/* assign the display function */
	glutDisplayFunc(display);

	/* assign the idle function */
	glutIdleFunc(display);

	/* sets the reshape callback for the current window */
	glutReshapeFunc(reshape);

//parse database
	init_data();

	init_projections();

	/* enters the GLUT event processing loop */
	glutMainLoop();

	delete_projections();

	return (EXIT_SUCCESS);
}
