/*!
 * \file
 *
 * \author Alberto Scolari
 *
 * \brief basic transformations using 4x4 matrices like rotation, translation
 * 			 and shear
 */

#include <stddef.h>
#include <math.h>
#include <cstdarg>

#include "cg_transformations.hh"
#include "CG_Common/CG_Matrices/CG_Vector.hpp"
#include "CG_Common/CG_Debug/cg_debug.hh"
#include "CG_Common/basic_functions.hh"
#include "CG_Vector.hpp"
#include "CG_Matrix.hpp"
using namespace CGLib;

namespace CGLib {

CG_TransMatrix* get_unit_matrix() {
	CG_TransMatrix *m = new CG_TransMatrix();
	for (unsigned int i = 0; i < 4; i++) {
		(*m)[i][i] = 1;
	}
	return m;
}

// defined in cg_debug.hh
#ifdef APPROX

#define MIN_REP 1.0e-7

static float zero_approx(const float num) {
	if ((num > -MIN_REP) && (num < MIN_REP)) {
		return 0;
	} else {
		return num;
	}
}

#define approx(num) zero_approx(num)

#else

#define approx(num) num

#endif

CG_TransMatrix* get_rot_matrix_from_angles(float x_angle, float y_angle,
		float z_angle) {

	CG_TransMatrix* beg = get_unit_matrix(), *res, *m;
	res = beg;
	if (x_angle != 0) {
		m = get_unit_matrix();
		(*m)[1][1] = approx(cosf(cg_deg_to_radf(x_angle)));
		(*m)[1][2] = approx(-sinf(cg_deg_to_radf(x_angle)));
		(*m)[2][1] = approx(sinf(cg_deg_to_radf(x_angle)));
		(*m)[2][2] = approx(cosf(cg_deg_to_radf(x_angle)));
		res = (*m) * (*beg);
		delete beg;
		delete m;
		beg = res;
	}
	if (y_angle != 0) {
		m = get_unit_matrix();
		(*m)[0][0] = approx(cosf(cg_deg_to_radf(y_angle)));
		(*m)[0][2] = approx(sinf(cg_deg_to_radf(y_angle)));
		(*m)[2][0] = approx(-sinf(cg_deg_to_radf(y_angle)));
		(*m)[2][2] = approx(cosf(cg_deg_to_radf(y_angle)));
		res = (*m) * (*beg);
		delete beg;
		delete m;
		beg = res;
	}
	if (z_angle != 0) {
		m = get_unit_matrix();
		(*m)[0][0] = approx(cosf(cg_deg_to_radf(z_angle)));
		(*m)[0][1] = approx(-sinf(cg_deg_to_radf(z_angle)));
		(*m)[1][0] = approx(sinf(cg_deg_to_radf(z_angle)));
		(*m)[1][1] = approx(cosf(cg_deg_to_radf(z_angle)));
		res = (*m) * (*beg);
		delete beg;
		delete m;
	}

	return res;
}

CG_TransMatrix* get_rot_matrix_from_vectors(CG_Cartesian_f& x_vec,
		CG_Cartesian_f& y_vec, CG_Cartesian_f& z_vec) {

	CG_Cartesian_f *tmp;
	float mod;

	CG_TransMatrix* res = get_unit_matrix();

	mod = x_vec.module();
	if (mod == 1) {
		res->import_submatrix(x_vec, 0, 0);
	} else if (mod != 0) {
		tmp = new CG_Cartesian_f();
		tmp->import_submatrix(x_vec, 0, 0);
		tmp->mult_els(1 / mod);
		res->import_submatrix(*tmp, 0, 0);
		delete tmp;
	}

	mod = y_vec.module();
	if (mod == 1) {
		res->import_submatrix(y_vec, 0, 1);
	} else if (mod != 0) {
		tmp = new CG_Cartesian_f();
		tmp->import_submatrix(y_vec, 0, 0);
		tmp->mult_els(1 / mod);
		res->import_submatrix(*tmp, 0, 1);
		delete tmp;
	}

	mod = z_vec.module();
	if (mod == 1) {
		res->import_submatrix(z_vec, 0, 2);
	} else if (mod != 0) {
		tmp = new CG_Cartesian_f();
		tmp->import_submatrix(z_vec, 0, 0);
		tmp->mult_els(1 / mod);
		res->import_submatrix(*tmp, 0, 2);
		delete tmp;
	}
	return res;
}

/*
CG_TransMatrix* get_rot_matrix_from_vectors(CG_Cartesian_f& x_vec,
		CG_Cartesian_f& y_vec, CG_Cartesian_f& z_vec) {

	CG_Vector<float> *xf, *yf, *zf;
	xf = x_vec.to_float();
	xf->normalize();

	yf = y_vec.to_float();
	yf->normalize();

	zf = z_vec.to_float();
	zf->normalize();

	CG_TransMatrix *res = get_rot_matrix_from_vectors(*xf, *yf, *zf);
	delete xf;
	delete yf;
	delete zf;
	return res;

}
*/

CG_TransMatrix* get_scale_matrix(float x_scale, float y_scale, float z_scale) {

	CG_TransMatrix *res = new CG_TransMatrix();
	(*res)[0][0] = x_scale;
	(*res)[1][1] = y_scale;
	(*res)[2][2] = z_scale;
	(*res)[3][3] = 1;
	return res;
}

CG_TransMatrix* get_translation_matrix(float x_trans, float y_trans,
		float z_trans) {

	CG_TransMatrix *res = get_unit_matrix();
	(*res)[0][3] = x_trans;
	(*res)[1][3] = y_trans;
	(*res)[2][3] = z_trans;
	return res;
}

CG_TransMatrix* get_translation_matrix_opposite(CG_Cartesian_f& v) {

	return get_translation_matrix(-float(v[X_coord]),
			-float(v[Y_coord]), -float(v[Z_coord]));
}

CG_TransMatrix* get_shear_matrix(float sh1, float sh2, cg_coordinate c) {

	CG_TransMatrix *res = get_unit_matrix();
	unsigned int rs1 = 0, rs2 = 0, cc = 0;
	switch (c) {
	case X_coord:
		rs1 = 1;
		rs2 = 2;
		cc = 0;
		break;
	case Y_coord:
		rs1 = 0;
		rs2 = 2;
		cc = 1;
		break;
	case Z_coord:
		rs1 = 0;
		rs2 = 1;
		cc = 2;
		break;
	case W_coord:
		break;
	}

	(*res)[rs1][cc] = sh1;
	(*res)[rs2][cc] = sh2;
	return res;
}

/*!
 * \fn CG_TransMatrix* compose_transformations(int count, CG_TransMatrix* t1, ...)
 *
 * \brief computes the composition of a variable number of transformations,
 * where the first one (t1) is the first transformation to be applied, and so on
 *
 * \param count number of transformations to be composed
 * \param t1 the first transformation (i.e. tn x t(n-1) x ... x t3 x t2 xt1) of the sequence
 * \return the composition of all the transformations
 */
CG_TransMatrix* compose_transformations(int count, CG_TransMatrix* t1, ...) {
	CG_TransMatrix *res, *cur;
	va_list ap;
	va_start(ap, t1);

	res = t1;
	if (count >= 2) {
		cur = va_arg(ap, CG_TransMatrix*);
		res = (*res) * (*cur);
		for (int i = 3; i <= count; i++) {
			cur = va_arg(ap, CG_TransMatrix*);

			DEBUG_STM(
				if (cur == NULL || res == NULL) {			\
				DEBUG1("NULL transformation pointer n. ", i)	\
				continue;										\
				}
			)

			*res *= (*cur);
		}
	}

	va_end(ap);
	return res;
}

}

