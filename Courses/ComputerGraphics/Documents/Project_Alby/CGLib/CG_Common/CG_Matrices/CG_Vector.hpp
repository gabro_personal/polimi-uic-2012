/*!
 * \file
 *
 * \author Alberto Scolari
 *
 * \brief File containing the definition of the CG_Vector.
 *
 *  Here too as for CG_Matrix, the methods returning a pointer of type CG_Vector
 *  allocate a new object, which must be properly pointed and deleted.
 *  Don't forget also to delete the previous objects, whose pointers must be kept
 *  until the objects are deleted!
 */

#ifndef CGVECTOR_H_
#define CGVECTOR_H_
#include <math.h>
#include "CG_Matrix.hpp"
using namespace CGLib;



namespace CGLib {

typedef enum {
	X_coord = 0, Y_coord = 1, Z_coord = 2, W_coord = 3
} cg_coordinate;

#define x(p) ((p).vals[X_coord][0])
#define y(p) ((p).vals[Y_coord][0])
#define z(p) ((p).vals[Z_coord][0])
#define w(p) ((p).vals[W_coord][0])

/*!
 * \class CG_Vector
 *
 * \brief the class which stores a COLUMN vector, with the operations defined it
 *
 * \tparam T the type of stored elements (usually int or float)
 */
template<typename T> class CG_Vector: public CG_Matrix<T> {
public:

	/*!
	 * Constructor of vector
	 *
	 * \param rows the number of rows of the vector
	 */
	CG_Vector(const unsigned int rows) :
			CG_Matrix<T>(rows, 1) {
	}

	/*!
	 * Default constructor
	 */
	virtual ~CG_Vector() {
	}

	/*!
	 * \brief returns the module of the vector
	 *
	 * \return the module of the vector
	 */
	float module() {
		float mod = 0;
		for (unsigned int i = 0; i < this->rows; i++) {
			mod += float(this->vals[i][0] * this->vals[i][0]);
		}
		return sqrtf(mod);
	}

	/*!
	 * \brief transforms the current vector into a unit vector
	 *
	 * The transformation happens INSIDE the vector: the elements are modified,
	 * no new vector is created.
	 */
	void normalize() {
		float mod = this->module();
		if (mod == 0 || mod == 1) {
			/*
			 * if the module is 0, no division is possible; if it is 1
			 * it is already a unit vector.
			 */
			return;
		}
		for (unsigned int i = 0; i < this->rows; i++) {
			this->vals[i][0] = T((float(this->vals[i][0])) / mod);
		}
	}

	/*!
	 * \brief returns the "cartesian" vector of the current vecto, i.e. a vector
	 * having one row less (the bottom rowis "remved") and all the original values
	 * in ALL the remaining columns.
	 *
	 * Note that no transfomation happens for the original vector: e.g., if the last
	 * element (the one which won't be present in the new vector) is not 1, this is ignored
	 * and the "cartesian" vector will contain wrong values (see method
	 * \ref <homogenize_method>["(homogenize())"] to scale a vector to homogeneous coordinates
	 * having last element of value 1).
	 *
	 * \return
	 */
	CG_Vector<T>* to_cart() {
		CG_Vector<T>* res = new CG_Vector<T>(this->rows - 1);
		for (unsigned int i = 0; i < this->rows - 1; i++) {
			res->vals[i][0] = this->vals[i][0];
		}
		return res;
	}

	/*!
	 * \brief returns a new vector which is the "homogeneous" expansion
	 * of the current vector, i.e. it has a further last row whose element
	 * has value 1
	 *
	 * The new vector has the same elements of the current one, and a 1 as last bottom element.
	 * No scaling is applied!
	 *
	 * \return the pointer to the new homogeneous  vector
	 */
	CG_Vector<T>* to_hom() {
		CG_Vector<T>* res = new CG_Vector<T>(this->rows + 1);
		for (unsigned int i = 0; i < this->rows; i++) {
			res->set_el(i, this->vals[i][0]);
		}
		res->vals[this->rows][0] = 1;
		return res;
	}

	/*!
	 * \anchor <homogenize_method>
	 *
	 * \brief "homogenizes" the vector by scaling all the elements s.t. the last one
	 * has value 1
	 *
	 * If the last element is 0, no scaling is performed.
	 *
	 * \return the initial value of the last element.
	 */
	int homogenize() {
		if (this->vals[this->rows - 1][0] == 0) {
			return 0;
		} else {
			T last = this->vals[this->rows - 1][0];
			for (unsigned int i = 0; i < this->rows - 1; i++) {
				this->vals[i][0] /= last;
			}
			this->vals[this->rows - 1][0] = 1;
			return last;
		}
	}

	/*!
	 * \brief redefinition of the operator [], to access the elements of the vector
	 * in read-only mode
	 *
	 * \param row the row to access
	 * \return if the "row" is in the bounds, the element at row "row"; 0 instead
	 */
	T operator[](int row) {
		return this->get_el(row);
	}

	/*!
	 * \brief vector product in 3 dimensions
	 *
	 * This method returns a new vector storing the vector product (denoted as X) of the
	 * previous vectors in the appearing order (i.e. "this" X "v2", NOT THE CONTRARY).
	 * The vector is intended ONLY in 3 dimensions, i.e. only the first 3 dimensions
	 * are used to compute the product; the remaining rows (if any) are copied from the
	 * current vector.
	 * This in order to compute the vector product also with 4-dimensions vectors (greater
	 * dimensions are uncommon) intended as homogeneous vectors, whose vector product
	 * must still be a homogeneous vector indicating a 3-d vector with the first
	 * dimensions being those of the 3-d vector product and the last dimensions being 0.
	 *
	 * \param v2 the right operand
	 * \return the pointer to the new vector
	 */
	virtual CG_Vector<T>* vector_prod_3d(CG_Vector<T>& v2) {
		if ((this->rows < 3) || (v2.getRows() != this->rows)) {
			return NULL;
		}
		CG_Vector<T> *prod = new CG_Vector<T>(this->rows);
		prod->set_el(0,
				this->vals[Y_coord][0] * v2[Z_coord] - this->vals[Z_coord][0] * v2[Y_coord]);
		prod->set_el(1,
				-this->vals[X_coord][0] * v2[Z_coord] + this->vals[Z_coord][0] * v2[X_coord]);
		prod->set_el(2,
				this->vals[X_coord][0] * v2[Y_coord] - this->vals[Y_coord][0] * v2[X_coord]);
		for (unsigned int i = 3; i < this->rows; i++) {
			prod->set_el(i, this->vals[i][0]);
		}
		return prod;
	}

	/*!
	 * \brief redefinition of get_el(), since a CG_Vector is a column vector
	 * and thus has always column equal to 0. Returns the element at row "row".
	 *
	 * \param row the row to access
	 * \return if the given row is in the bounds, the corresponding element; 0 otherwise
	 */
	T get_el(unsigned int row) {
		if (row < this->rows) {
			return this->vals[row][0];
		} else {
			return 0;
		}
	}

	/*!
	 * \brief redefinition of set_el(), since a CG_Vector is a column vector
	 * and thus has always column equal to 0. Stores "value" into the element
	 * at row "row"
	 *
	 * If the "row" is out of bound, no operation is performed.
	 *
	 * \param row the row to access
	 * \param value the value to store
	 */
	void set_el(unsigned int row, T value) {
		if (row < this->rows) {
			this->vals[row][0] = value;
		}
	}

	using CG_Matrix<T>::to_float;
	/*!
	 * \brief redefinition of to_float, since the returned pointer points still to a vector.
	 * Returns the new vector pointer, containing float elements of the same value of the original
	 * corresponding elements
	 *
	 * \return the pointer to the new vector of float elements
	 */
	CG_Vector<float>* to_float() {
		return static_cast<CG_Vector<float>*>(this->CG_Matrix<T>::to_float());
	}

	using CG_Matrix<T>::to_int;
	/*!
	 * \brief redefinition of to_int, since the returned  pointer points still to a vector.
	 * Returns the new vector pointer, whose elements are the integer rounding of those of
	 * the original vector.
	 *
	 * \return  the pointer to the new vector of int elements
	 */
	CG_Vector<int>* to_int() {
		return static_cast<CG_Vector<int>*>(this->CG_Matrix<T>::to_int());
	}

};

// for both points and vectors (which are undistinguishable in 3D)
class CG_Cartesian_f: public CG_Vector<float> {
public:

	CG_Cartesian_f() :
			CG_Vector<float>(3) {
	}

	CG_Cartesian_f(float x, float y, float z) :
			CG_Vector<float>(3) {
		this->CG_Matrix<float>::vals[0][0] = x;
		this->CG_Matrix<float>::vals[1][0] = y;
		this->CG_Matrix<float>::vals[2][0] = z;
	}

	virtual ~CG_Cartesian_f() {
	}

	//using CG_Vector<int>::vector_prod_3d;
	virtual CG_Cartesian_f* vector_prod_3d(CG_Cartesian_f& v2) {
		return static_cast<CG_Cartesian_f *>(this->CG_Vector<float>::vector_prod_3d(v2));
	}

};

class CG_HomVector_f: public CG_Vector<float> {
public:

	CG_HomVector_f(float x, float y, float z) :
	CG_Vector<float>(4) {
		this->CG_Matrix<float>::vals[0][0] = x;
		this->CG_Matrix<float>::vals[1][0] = y;
		this->CG_Matrix<float>::vals[2][0] = z;
		this->CG_Matrix<float>::vals[3][0] = 0;
	}

	virtual ~CG_HomVector_f() {
	}

	//using CG_Vector<int>::vector_prod_3d
	virtual CG_HomVector_f* vector_prod_3d(CG_HomVector_f& v2) {
		return static_cast<CG_HomVector_f*>(this->CG_Vector<float>::vector_prod_3d(v2));
	}

};

class CG_HomPoint_f: public CG_Vector<float> {
public:

	CG_HomPoint_f(float x, float y, float z) :
	CG_Vector<float>(4) {
		this->CG_Matrix<float>::vals[0][0] = x;
		this->CG_Matrix<float>::vals[1][0] = y;
		this->CG_Matrix<float>::vals[2][0] = z;
		this->CG_Matrix<float>::vals[3][0] = 1;
	}

	virtual ~CG_HomPoint_f() {
	}

};


class CG_TransMatrix: public CG_Matrix<float> {
public:
	CG_TransMatrix() :
			CG_Matrix<float>(4, 4) {
	}

	CG_TransMatrix* to_float() {
		return this;
	}

	CG_TransMatrix* operator*=(CG_TransMatrix& m2) {
		return static_cast<CG_TransMatrix*>(this->CG_Matrix<float>::operator *=(m2));
	}

	CG_TransMatrix* operator*(CG_TransMatrix& m2) {
		return static_cast<CG_TransMatrix*>(this->CG_Matrix<float>::operator *(m2));
	}

	CG_Matrix<float>* operator*(CG_Matrix<float>& m2) {
		return static_cast<CG_Matrix<float>*>(this->CG_Matrix<float>::operator *(m2));
	}

	CG_Vector<float>* operator*(CG_Vector<float>& m2) {
			return static_cast<CG_Vector<float>*>(this->CG_Matrix<float>::operator *(m2));
		}

};

} /* namespace CGLib */
#endif /* CGVECTOR_H_ */
