/*!
 * \file
 *
 * \author Alberto Scolari
 *
 * \brief basic transformations using 4x4 matrices like rotation, translation
 * 			 and shear.
 */

#include "CG_Common/CG_Matrices/CG_Matrix.hpp"
#include "CG_Common/CG_Matrices/CG_Vector.hpp"
using namespace CGLib;

#ifndef CG_TRANSFORMATIONS_HH_
#define CG_TRANSFORMATIONS_HH_

namespace CGLib {





CG_TransMatrix* get_unit_matrix();

CG_TransMatrix* get_rot_matrix_from_angles(float x_angle, float y_angle,
		float z_angle);

CG_TransMatrix* get_rot_matrix_from_vectors(CG_Cartesian_f& x_vec,
		CG_Cartesian_f& y_vec, CG_Cartesian_f& z_vec);

CG_TransMatrix* get_scale_matrix(float x_scale, float y_scale, float z_scale);

CG_TransMatrix* get_translation_matrix_opposite(CG_Cartesian_f& v);

CG_TransMatrix* get_translation_matrix(float x_trans, float y_trans,
		float z_trans);

CG_TransMatrix* get_shear_matrix(float sh1, float sh2, cg_coordinate c);

/*!
 * \fn CG_TransMatrix* compose_transformations(int count, CG_TransMatrix* t1, ...)
 *
 * \brief computes the composition of a variable number of transformations,
 * where the first one (t1) is the first transformation to be applied, and so on
 *
 * \param count number of transformations to be composed
 * \param t1 the first transformation (i.e. tn x t(n-1) x ... x t3 x t2 xt1) of the sequence
 * \return the composition of all the transformations
 */

CG_TransMatrix* compose_transformations(int count, CG_TransMatrix* t1, ...);

}

#endif /* CG_TRANSFORMATIONS_HH_ */
