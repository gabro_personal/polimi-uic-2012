/*!
 * \file
 *
 * \author Alberto Scolari
 *
 * \brief File containing the definition of the CG_Matrix.
 *
 *  The methods returning a pointer of type CG_Matrix allocate a new object,
 *  which must be properly pointed and deleted. Don't forget also to delete
 *  the previous objects, whose pointers must be kept until the objects
 *  are deleted!
 */

#ifndef CGMATRIX_H_
#define CGMATRIX_H_
#include <cstddef>

#include "CG_Common/basic_functions.hh"
using namespace CGLib;

namespace CGLib {

/*!
 * \fn void set_to_zero(T** ptr, const int rows, const int cols)
 *
 * \brief initializes a two dimensional array to 0
 *
 * \tparam T type of values if the array (usually int or float)
 * \param ptr the pointer to the array
 * \param rows the number of rows
 * \param cols the number of columns
 */
template<typename T> static inline void set_to_zero(T** ptr, const int rows, const int cols) {
	for (int j = 0; j < rows; j++) {
		for (int i = 0; i < cols; i++) {
			ptr[j][i] = T(0);
		}
	}
}

template<typename T> static inline void delete_array(T** array, const unsigned int rows) {
	for (unsigned int i = 0; i < rows; i++) {
		delete[] array[i];
	}
	delete[] array;
}

/*!
 * \class CG_Matrix
 *
 * \brief class to simulate a matrix, with the basic operations.
 *
 * \tparam T type of stored values (usually int or float).
 *
 */
template<typename T> class CG_Matrix {
public:

	/*!
	 * \brief CG_Matrix costructor, which initializes to 0 ALL the values.
	 *
	 * \param mrows number of rows
	 * \param mcols number of columns
	 */
	CG_Matrix(unsigned int mrows, unsigned int mcols) :
			rows(mrows), cols(mcols) {
		this->vals = new T*[mrows];
		for (unsigned int i = 0; i < mrows; i++) {
			this->vals[i] = new T[mcols];
		}
		set_to_zero(vals, mrows, mcols);
	}

	/*!
	 * \brief initialize all the elements on the diagonal to "value".
	 *
	 * If the matrix is not sqare, the upper diagonal is initialized.
	 *
	 * \param value the value to be stored into the diagonal matrix
	 */
	void init_diag(T value) {
		unsigned int min = this->rows;
		if (this->cols < this->rows) {
			min = this->cols;
		}
		for (unsigned int i = 0; i < min; i++) {
			vals[i][i] = value;
		}
	}

	/*!
	 * \brief default destructor
	 */
	virtual ~CG_Matrix() {
		delete_array(this->vals, this->rows);
	}

	/*!
	 * \brief operator  [] for a fast access to each row
	 *
	 * If row is outside the matrix bounds, NULL is returned
	 *
	 * \param row the row to access
	 * \return the pointer to the row row-1
	 */
	virtual T* operator[](unsigned int row) const {
		if (((unsigned int) (row)) >= this->rows) {
			return NULL;
		}
		return this->vals[row];
	}

	/*!
	 * \brief matrix product operator, as the usual mathematical definition.
	 *
	 * If the matrices are not compatible for multiplication, NULL is returned.
	 * If they are, a new matrix is created of the proper dimensions.
	 *
	 * \param m2 the right operand
	 * \return the pointer to the new matrix, or NULL if the operands matrices are not compatible
	 */
	CG_Matrix<T>* operator*(CG_Matrix<T>& m2) {
		if (this->cols != m2.getRows()) {
			return NULL;
		}
		CG_Matrix<T>* prod = new CG_Matrix<T>(this->rows, m2.getCols());
		for (unsigned int i = 0; i < this->rows; i++) {
			for (unsigned int j = 0; j < m2.getCols(); j++) {
				for (unsigned int k = 0; k < this->cols; k++) {
					// BASED ON HE FACT THAT THERE IS 0 INSIDE!!
					prod->set_el(i, j, prod->get_el(i, j) + (this->vals[i][k] * m2[k][j]));
				}
			}
		}
		return prod;
	}

	CG_Matrix<T>* operator*=(CG_Matrix<T>& m2) {
		T** ptr;
		if (this->cols != m2.getRows()) {
			return NULL;
		}

		// create new elements array
		ptr = new T*[this->rows];
		for (unsigned int i = 0; i < this->rows; i++) {
			ptr[i] = new T[m2.cols];
		}
		set_to_zero<T>(ptr, this->rows, m2.cols);
		this->cols = m2.getCols();

		// compute the values
		for (unsigned int i = 0; i < this->rows; i++) {
			for (unsigned int j = 0; j < m2.getCols(); j++) {
				for (unsigned int k = 0; k < this->cols; k++) {
					ptr[i][j] += (this->vals[i][k] * m2[k][j]);
				}
			}
		}

		// store values into current matrix and delete old elements
		delete_array(this->vals, this->rows);

		this->vals = ptr;

		return this;

	}

	/*!
	 * \brief sum operator for matrices, according to the mathematical definition
	 *
	 * If the matrices are not compatible for sum (i.e. of the same dimensions), NULL is returned.
	 * If they are, a new matrix is created of the same dimensions.
	 *
	 * \param m2 he right operand
	 * \return the pointer to the new matrix, or NULL if the operands matrices are not compatible
	 */
	CG_Matrix<T>* operator+(CG_Matrix<T>& m2) {
		if (this->rows != m2.getRows() || this->cols != m2.getCols()) {
			return NULL;
		}
		CG_Matrix<T>* sum = new CG_Matrix<T>(this->rows, this->cols);
		for (unsigned int i = 0; i < this->rows; i++) {
			for (unsigned int j = 0; j < this->cols; j++) {
				sum->set_el(i, j, this->vals[i][j] + m2[i][j]);
			}
		}
		return sum;
	}

	CG_Matrix<T>* operator-(CG_Matrix<T>& m2) {
		if (this->rows != m2.getRows() || this->cols != m2.getCols()) {
			return NULL;
		}
		CG_Matrix<T>* sum = new CG_Matrix<T>(this->rows, this->cols);
		for (unsigned int i = 0; i < this->rows; i++) {
			for (unsigned int j = 0; j < this->cols; j++) {
				sum->set_el(i, j, this->vals[i][j] - m2.vals[i][j]);
			}
		}
		return sum;
	}

	bool operator!=(CG_Matrix<T>& m2) {
		if (this->rows != m2.getRows() || this->cols != m2.getCols()) {
			return true;
		}
		for (unsigned int i = 0; i < this->rows; i++) {
			for (unsigned int j = 0; j < this->cols; j++) {
				if (this->vals[i][j] != m2.vals[i][j]) {
					return true;
				}
			}
		}
		return false;
	}

	/*!
	 * \brief transposes the matrix, returning a new matrix of the proper dimensions
	 *
	 * \return the pointer to the new matrix
	 */
	CG_Matrix<T>* transpose() {
		CG_Matrix<T>* tr = new CG_Matrix<T>();
		for (unsigned int i = 0; i < this->rows; i++) {
			for (unsigned int j = 0; j < this->cols; j++) {
				tr->set_el(j, i, this->vals[i][j]);
			}
		}
		return tr;
	}

	/*!
	 * \brief creates a matrix of the same dimensions, whose elements have the same value
	 * but are of type float
	 *
	 * \return the pointer to the new matrix
	 */
	CG_Matrix<float>* to_float() {
		CG_Matrix<float>* ret = new CG_Matrix<float>(this->rows, this->cols);
		for (unsigned int i = 0; i < this->rows; i++) {
			for (unsigned int j = 0; j < this->cols; j++) {
				ret->set_el(i, j, float(this->vals[i][j]));
			}
		}
		return ret;
	}

	/*!
	 * \brief creates a matrix of the same dimensions, whose integer elements are the approximations
	 * of those of the current matrix
	 *
	 * \return the pointer to the new matrix
	 */
	virtual CG_Matrix<int>* to_int() {
		CG_Matrix<int>* ret = new CG_Matrix<int>(this->rows, this->cols);
		for (unsigned int i = 0; i < this->rows; i++) {
			for (unsigned int j = 0; j < this->cols; j++) {
				ret->set_el(i, j, cg_roundf(float(this->vals[i][j])));
			}
		}
		return ret;
	}

	/*!
	 * \brief returns the element specified by the given (0 - based) coordinates
	 *
	 * If either one of the coordinates is out of bounds, 0 is returned
	 *
	 * \param x the row
	 * \param y the column
	 * \return value at coordinates (x,y) if these are valid, 0 otherwise
	 */
	T get_el(unsigned int x, unsigned int y) {
		if (x < this->rows && y < this->cols) {
			return vals[x][y];
		} else {
			return 0;
		}
	}

	/*!
	 * \brief sets the element at the given coordinates to the given value
	 *
	 * If either one of the coordinates is out of bounds, no element is set.
	 *
	 * \param x the row
	 * \param y the column
	 */
	void set_el(unsigned int x, unsigned int y, T value) {
		if (x < this->rows && y < this->cols) {
			vals[x][y] = value;
		}
	}

	/*!
	 * \brief multiplies ALL the elements of the matrix by coeff
	 *
	 * \param coeff the coefficient to be applied to ALL the elements of the matrix
	 */
	void mult_els(T coeff) {
		for (unsigned int i = 0; i < this->rows; i++) {
			for (unsigned int j = 0; j < this->cols; j++) {
				vals[i][j] *= coeff;
			}
		}
	}

	/*!
	 * \brief returns the number of rows of the matrix
	 *
	 * \return the number of rows of the matrix
	 */
	unsigned int getRows() {
		return this->rows;
	}

	/*!
	 * \brief returns the number of columns of the matrix
	 *
	 * \return the number of columns of the matrix
	 */
	unsigned int getCols() {
		return this->cols;
	}

	/*!
	 * \brief copies the element of the given matrix into the elements of the current matrix,
	 * starting from row "from_row" and column "from_col" until the end
	 * of one of the two matrices (the source, "m", and the current, this) is reached
	 *
	 * \param m the matrix to be copied into the current one
	 * \param from_row the starting row
	 * \param from_col the starting column
	 */
	void import_submatrix(CG_Matrix<T>& m, unsigned int from_row, unsigned int from_col) {
		unsigned int maxr, maxc;
		if (this->cols < (m.getCols() + from_col)) {
			maxc = this->cols;
		} else {
			maxc = m.getCols() + from_col;
		}
		if (this->rows < (m.getRows() + from_row)) {
			maxr = this->rows;
		} else {
			maxr = m.getRows() + from_row;
		}
		for (unsigned int i = 0; i + from_row < maxr; i++) {
			for (unsigned int j = 0; j + from_col < maxc; j++) {
				this->vals[from_row + i][from_col + j] = T(m[i][j]);
			}
		}
	}

protected:
	unsigned int rows; /*!< the number of rows */
	unsigned int cols; /*!< the number of columns */
	T** vals; /*!< the pointer to the matrix values */

	friend class CG_Projector;

};



} /* namespace CGLib */
#endif /* CGMATRIX_H_ */
