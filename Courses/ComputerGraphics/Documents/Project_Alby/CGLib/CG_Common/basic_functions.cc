/*
 * basic_functions.cc
 *
 *  Created on: 21/set/2012
 *      Author: alberto
 */

#include <math.h>

#include "CG_Common/basic_entities.hh"
using namespace CGLib;

namespace CGLib {

/*!
 * \fn cg_roundf(float d)
 *
 * \brief rounds d to the closest integer.
 *
 * It differs from roundf() of math.h in the fact that it works also
 * with negative numbers in the expected, mathematical behaviour.
 *
 * \param d the number to round, as floating point
 * \return the integer value closest to d
 */
int cg_roundf(float d) {
	if (d >= 0) {
		return int(floorf(d + float(0.5)));
	} else {
		return int(ceilf(d - float(0.5)));
	}
}

/*!
 * \fn circle_quad(const int x, const int y, const int xcenter, const int ycenter)
 *
 * \brief returns the quadrant of the given (x,y) point with respect to the center (xcenter,ycenter)
 *
 * Given a point (x,y), which is supposed to lie on the circle having center (xcenter,ycenter),
 * this function returns the quadrant the point lies in.
 * Quadrants are numbered from 0 to 3 in clockwise direction.
 *
 * \param x horizontal coordinate of the point on the circle
 * \param y horizontal coordinate of the point on the circle
 * \param xcenter horizontal coordinate of the center
 * \param ycenter horizontal coordinate of the center
 * \return the quadrant number
 */
unsigned int circle_quad(const int x, const int y, const int xcenter, const int ycenter) {
	if (x >= xcenter) {
		if (y >= ycenter) {
			return 0;
		} else {
			return 1;
		}
	} else {
		if (y >= ycenter) {
			return 3;
		} else {
			return 2;
		}
	}
}

/*!
 * \fn circle_portion(const unsigned int parts, const float angle)
 *
 * \brief returns the portion corresponding to the given angle, considering the
 * 360° angle as divided in "parts" parts.
 *
 * Given the angle, which is supposed to be in CLOCKWISE direction, in DEGREES
 * and from north, this function returns the part the angle ends in.
 * Parts are numbered from 0 to parts - 1 in clockwise direction.
 * Values of the angle bigger than 360 will however make the function return parts - 1.
 *
 * \param angle the angle, expected to range from 0 to 360
 * \param parts the parts the circle is to be considered in
 * \return the quadrant number
 */
static int circle_portion(const unsigned int parts, const float angle) {
	unsigned int ret = (unsigned int) ceilf(angle / (((float) 360) / ((float) (parts))))
			- 1;
	if (ret < parts) {
		return ret;
	} else {
		return parts - 1;
	}
}

/*!
 * \fn circle_quad(const unsigned float angle)
 *
 * \brief returns the quadrant corresponding to the given angle
 *
 * Given the angle, which is supposed to be in CLOCKWISE direction and from north,
 * this function returns the quadrant the angles end in.
 * Quadrants are numbered from 0 to 3 in clockwise direction.
 * Values of the angle bigger than 360 will however make the function return 3.
 *
 * \param angle the angle, expected to range from 0 to 360
 * \return the quadrant number
 */
unsigned int circle_quad(const float angle) {
	return circle_portion(4, angle);
}

/*!
 * \fn circle_octant(const unsigned float angle)
 *
 * \brief returns the octant corresponding to the given angle
 *
 * Given the angle, which is supposed to be in CLOCKWISE direction, in DEGREES
 * and from north, this function returns the octant the angle ends in.
 * Octants are numbered from 0 to 7 in clockwise direction.
 * Values of the angle bigger than 360 will however make the function return 7.
 *
 * \param angle the angle, expected to range from 0 to 360
 * \return the quadrant number
 */
unsigned int circle_octant(const float angle) {
	return circle_portion(8, angle);
}

}
