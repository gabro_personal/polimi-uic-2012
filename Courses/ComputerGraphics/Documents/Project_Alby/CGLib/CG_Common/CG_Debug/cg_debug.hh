/*!
 * \file
 *
 * \author Alberto Scolari
 *
 * \brief simple debugging primitives, to be stripped at the final compilation
 * 			by undefining the symbol DBG
 *
 * Also information primitives are present.
 */

#ifndef CG_DEBUG_HH_
#define CG_DEBUG_HH_
#include <iostream>
#include <cstdlib>

#include "CG_Common/CG_Matrices/CG_Vector.hpp"
using namespace CGLib;


namespace CGLib {

#define println(str) std::cout << (str) << std::endl

template<typename T> void print_matrix(CG_Matrix<T>& mat) {
	for (unsigned int i = 0; i < mat.getRows(); i++) {
		for (unsigned int j = 0; j < mat.getCols(); j++) {
			std::cout << T(mat[i][j]);
			std::cout << "\t";
		}
		std::cout << std::endl;
	}
}

inline void print_matrix_f(CG_Matrix<float>& mat) {
	print_matrix<float>(mat);
}

inline void print_matrix_i(CG_Matrix<int>& mat) {
	print_matrix<int>(mat);
}

/************ DEBUG PRIMITIVES **********************/

// approximate  numbers close to 0
//#define APPROX

// debug
//#define DBG

// print info
#define INF

#define DGB_O std::cerr

#define DBG_E std::endl

#ifdef DBG
/*!
 * \def DEBUG(str)
 *
 * \brief brutal debugging primitive to display the string str
 */

#define DEBUG_STM(stm) stm;

#define DEBUG(str) DGB_O << (str) << DBG_E;

#define DEBUG1(str,var1) 	DGB_O << (str);		\
							DGB_O << (var1);	\
							DGB_O << DBG_E;

#define DEBUG2(str, var1, var2) DGB_O << (str);		\
								DGB_O << (var1);		\
								DGB_O << (var2);		\
								DGB_O << DBG_E;

#define DEBUG3(str, var1, var2, var3) 	DGB_O << (str);		\
										DGB_O << (var1);		\
										DGB_O << (var2);		\
										DGB_O << (var3);		\
										DGB_O << DBG_E;

#define DEBUG_COND(cond,if_str) if(cond) {					\
											DEBUG(if_str)		\
										}

#define DEBUG_COND1(cond,if_str, if_var1) if(cond) {					\
											DEBUG1(if_str,if_var1)		\
										}

#define DEBUG_COND3(cond,if_str,if_var1,var2,var3) if(cond) {					\
											DEBUG3(if_str,if_var1,var2,var3)		\
										}

#define DEBUG_COND_ELSE1(cond,if_str, if_var1,else_str,else_var1) if(cond) {		\
											DEBUG1(if_str,if_var1)					\
										} else {									\
											DEBUG1(else_str,else_var1)				\
										}

/*!
 * \def DEBUG_EXIT(str)
 *
 * \brief even more brutal debugging primitive to display the string str
 * and exit the program with an error
 */
#define DEBUG_EXIT(str) 					\
							DEBUG(str)		\
							std::exit(-1);

#else

#define DEBUG_STM(stm)

#define DEBUG(str)

#define DEBUG1(str,var1)

#define DEBUG2(str, var1, var2)

#define DEBUG3(str, var1, var2, var3)

#define DEBUG_COND(cond,if_str)

#define DEBUG_COND1(cond,if_str, if_var1)

#define DEBUG_COND3(cond,if_str,if_var1,var2,var3)

#define DEBUG_COND_ELSE1(cond,if_str, if_var1,else_str,else_var1)

#define DEBUG_EXIT(str)

#endif


}


#endif /* CG_DEBUG_HH_ */


/************ INFO PRIMITIVES **********************/


#ifdef INF

#define INFO(str) std::cout << (str) << std::endl;
#define INFO1(str,var1) 	std::cout << (str);		\
							std::cout << (var1);	\
							std::cout << std::endl;

#else

#define INFO(str)
#define INFO1(str)

#endif
