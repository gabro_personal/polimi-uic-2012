/*!
 * \file
 *
 * \brief basic functions for drawing and filling
 *
 * \author Alberto Scolari
 */
#ifndef BASIC_FUNCTIONS_HH_
#define BASIC_FUNCTIONS_HH_
#include <math.h>


namespace CGLib {

/*!
 * \fn cg_deg_to_rad(int deg)
 *
 * \brief converts integer degrees in floating point radiants.
 *
 * @param deg the degrees to convert
 * @return the value of reg in radiants
 */
inline float cg_deg_to_rad(float deg) {
	return ((deg)) * (float(M_PI)) / (float(180));
}


/*!
 * \fn cg_deg_to_rad(int deg)
 *
 * \brief converts floating point degrees in floating point radiants.
 *
 * @param deg the degrees to convert
 * @return the value of reg in radiants
 */
inline float cg_deg_to_radf(float deg) {
	return (deg* float(M_PI) / ((float) 180));
}


/*!
 * \fn cg_roundf(float d)
 *
 * \brief rounds d to the closest integer.
 *
 * It differs from roundf() of math.h in the fact that it works also
 * with negative numbers in the expected, mathematical behaviour.
 *
 * \param d the number to round, as floating point
 * \return the integer value closest to d
 */
int cg_roundf(float d);

/*!
 * \fn circle_quad(const int x, const int y, const int xcenter, const int ycenter)
 *
 * \brief returns the quadrant of the given (x,y) point with respect to the center (xcenter,ycenter)
 *
 * Given a point (x,y), which is supposed to lie on the circle having center (xcenter,ycenter),
 * this function returns the quadrant the point lies in.
 * Quadrants are numbered from 0 to 3 in clockwise direction.
 *
 * \param x horizontal coordinate of the point on the circle
 * \param y horizontal coordinate of the point on the circle
 * \param xcenter horizontal coordinate of the center
 * \param ycenter horizontal coordinate of the center
 * \return the quadrant number
 */
unsigned int circle_quad(const int x, const int y, const int xcenter,
		const int ycenter);

/*!
 * \fn circle_quad(const unsigned float angle)
 *
 * \brief returns the quadrant corresponding to the given angle
 *
 * Given the angle, which is supposed to be in CLOCKWISE direction, in DEGREES
 * and from north, this function returns the quadrant the angle ends in.
 * Quadrants are numbered from 0 to 3 in clockwise direction.
 * Values of the angle bigger than 360 will however make the function return 3.
 *
 * \param angle the angle, expected to range from 0 to 360
 * \return the quadrant number
 */
unsigned int circle_quad(const float angle);

/*!
 * \fn circle_octant(const unsigned float angle)
 *
 * \brief returns the octant corresponding to the given angle
 *
 * Given the angle, which is supposed to be in CLOCKWISE direction, in DEGREES
 * and from north, this function returns the octant the angle ends in.
 * Octants are numbered from 0 to 7 in clockwise direction.
 * Values of the angle bigger than 360 will however make the function return 7.
 *
 * \param angle the angle, expected to range from 0 to 360
 * \return the quadrant number
 */
unsigned int circle_octant(const float angle);

}  // namespace CGLib



#endif /* BASIC_FUNCTIONS_HH_ */
