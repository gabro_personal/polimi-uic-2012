/*!
 * \file
 *
 * \brief basic data structures to represent vertices and circumferences
 *
 * \author Alberto Scolari
 */

#ifndef BASICENTITIES_H_
#define BASICENTITIES_H_

#include <stddef.h>

namespace CGLib {

typedef struct  {
	int x;
	int y;
	//cg_point2f(int xp, int yp): x(xp),y(yp) {};
} cg_point2f;

//typedef struct cg_point2f cg_point2f;

typedef struct {
	int x;
	int y;
	int radius;
	float begAngle;
	float endAngle;
} cg_circle2f;

typedef enum {
	POINT, CIRCLE
} cg_line_type;

typedef struct cg_line {
	cg_line_type type;
	void* ptr;
} cg_line;

typedef struct {
	unsigned int linesNum;
	//CGlineType* types;
	cg_line* lines;
} multiLine2f;

//typedef struct multiLine2f multiLine2f;

#define p2f(ptr) (*((cg_point2f*)(ptr)))
#define c2f(ptr) (*((cg_circle2f*)(ptr)))

typedef struct {
	unsigned int width;
	unsigned int height;
	unsigned int multilinesNum;
	multiLine2f* multilines;
} cg_figure2f;

}

#endif /* BASICENTITIES_H_ */
