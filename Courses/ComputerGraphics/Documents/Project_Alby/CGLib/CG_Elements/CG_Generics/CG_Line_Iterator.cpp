/*
 * CGLineIterator.cpp
 *
 *  Created on: 16/set/2012
 *      Author: alberto
 */

#include "CG_Line_Iterator.hpp"
#include "CG_Common/basic_entities.hh"
using namespace CGLib;

namespace CGLib {

CG_Line_Iterator::CG_Line_Iterator(const multiLine2f* const obj, unsigned int begin) :
		mline(obj) {
	this->actual = begin;
}

CG_Line_Iterator::CG_Line_Iterator(const CG_Line_Iterator& it) :
		mline(it.mline) {
	this->actual = it.actual;
}

bool CG_Line_Iterator::operator==(const CG_Line_Iterator& it) {
	return (actual == it.actual) && (this->mline == it.mline);
}

bool CG_Line_Iterator::operator!=(const CG_Line_Iterator& it) {
	return (actual != it.actual) || (this->mline != it.mline);
}

CG_Line_Iterator& CG_Line_Iterator::operator++() {
	++actual;
	return *this;
}

CG_Line_Iterator CG_Line_Iterator::operator ++(int) {
	CG_Line_Iterator tmp = CG_Line_Iterator(*this);
	CG_Line_Iterator::operator++();
	return tmp;
}

const cg_line& CG_Line_Iterator::operator*() {
	if (this->actual < this->mline->linesNum) {
		return (this->mline->lines)[actual];
	} else {
		return (this->mline->lines)[this->mline->linesNum - 1];
	}
}

const cg_line* CG_Line_Iterator::operator->() const {
	if (this->actual < (this->mline->linesNum)) {
		return &((this->mline->lines)[actual]);
	} else {
		return &((this->mline->lines)[this->mline->linesNum - 1]);
	}
}

} /* namespace CGLib */
