/*
 * cg_simple_figures.cc
 *
 *  Created on: 26/set/2012
 *      Author: alberto
 */

#include "CG_Common/basic_entities.hh"
#include "CG_Common/basic_entities.hh"
#include "CG_Common/basic_functions.hh"
using namespace CGLib;

#include <cstdlib>
#include <cmath>
#include <climits>

namespace CGLib {

cg_figure2f* cg_triangle(const unsigned int base, const int height, const int distance) {
	cg_figure2f* fig = new cg_figure2f();
	int xmin = INT_MAX, ymin = INT_MAX;

	fig->height = std::abs(height);
	if (((unsigned int)(std::abs(distance))) > base) {
		fig->width = std::abs(distance);
	} else {
		fig->width = distance;
	}

	fig->multilinesNum = 1;
	fig->multilines = new multiLine2f();
	multiLine2f* ml = fig->multilines;
	ml->linesNum = 4;
	ml->lines = new cg_line[4]();

	ml->lines[0].type = POINT;
	ml->lines[1].type = POINT;
	ml->lines[2].type = POINT;
	ml->lines[3].type = POINT;

	ml->lines[0].ptr = (void*) (new cg_point2f());
	ml->lines[1].ptr = (void*) (new cg_point2f());
	ml->lines[2].ptr = (void*) (new cg_point2f());
	ml->lines[3].ptr = (void*) (new cg_point2f());

	/*rebase to 0*/
	if (distance >= 0) {
		xmin = 0;
	} else {
		xmin = -distance;
	}

	if (height >= 0) {
		ymin = 0;
	} else {
		ymin = -height;
	}

	p2f(ml->lines[0].ptr) .x = 0 + xmin;
	p2f(ml->lines[0].ptr) .y = 0 + ymin;

	p2f(ml->lines[1].ptr) .x = base + xmin;
	p2f(ml->lines[1].ptr) .y = 0 + ymin;

	p2f(ml->lines[2].ptr) .x = distance + xmin;
	p2f(ml->lines[2].ptr) .y = height + ymin;

	p2f(ml->lines[3].ptr) .x = 0 + xmin;
	p2f(ml->lines[3].ptr) .y = 0 + ymin;

	return fig;
}

cg_figure2f* cg_trapezium(const unsigned int lowbase, const int height,
		const unsigned int topbase, int topdistance) {

	cg_figure2f* fig = new cg_figure2f();
	int xmin = INT_MAX, ymin = INT_MAX;

	fig->height = std::abs(height);
	if (std::abs(topdistance) + topbase > lowbase) {
		fig->width = std::abs(topdistance) + topbase;
	} else {
		fig->width = lowbase;
	}

	fig->multilinesNum = 1;
	fig->multilines = new multiLine2f();
	multiLine2f* ml = fig->multilines;
	ml->linesNum = 5;
	ml->lines = new cg_line[5]();

	ml->lines[0].type = POINT;
	ml->lines[1].type = POINT;
	ml->lines[2].type = POINT;
	ml->lines[3].type = POINT;
	ml->lines[4].type = POINT;

	ml->lines[0].ptr = (void*) (new cg_point2f());
	ml->lines[1].ptr = (void*) (new cg_point2f());
	ml->lines[2].ptr = (void*) (new cg_point2f());
	ml->lines[3].ptr = (void*) (new cg_point2f());
	ml->lines[4].ptr = (void*) (new cg_point2f());

	if (topdistance >= 0) {
		xmin = 0;
	} else {
		xmin = -topdistance;
	}

	if (height >= 0) {
		ymin = 0;
	} else {
		ymin = -height;
	}

	p2f(ml->lines[0].ptr) .x = 0 + xmin;
	p2f(ml->lines[0].ptr) .y = 0 + ymin;

	p2f(ml->lines[1].ptr) .x = lowbase + xmin;
	p2f(ml->lines[1].ptr) .y = 0 + ymin;

	p2f(ml->lines[2].ptr) .x = topbase + topdistance + xmin;
	p2f(ml->lines[2].ptr) .y = height + ymin;

	p2f(ml->lines[3].ptr) .x = topdistance + xmin;
	p2f(ml->lines[3].ptr) .y = height + ymin;

	p2f(ml->lines[4].ptr) .x = 0 + xmin;
	p2f(ml->lines[4].ptr) .y = 0 + ymin;

	return fig;
}

cg_figure2f* cg_star(const unsigned int spikes, const unsigned int height,
		const unsigned int topbase) {
	cg_figure2f* fig = new cg_figure2f();
	int xmin = INT_MAX, ymin = INT_MAX;

	float b_angle = ((float) (360)) / ((float) (2 * spikes));

	float angle = -b_angle;
	float radius = float(topbase) / (2 * sinf(cg_deg_to_rad(b_angle)));
	float bigrad = radius * cosf(cg_deg_to_rad(b_angle)) + (float) height;

	fig->multilinesNum = 1;
	fig->multilines = new multiLine2f();
	multiLine2f* ml = fig->multilines;
	ml->linesNum = 2 * spikes + 1;
	ml->lines = new cg_line[2 * spikes + 1]();

	for (unsigned int i = 0; i < 2 * spikes + 1; i++) {
		ml->lines[i].type = POINT;
		ml->lines[i].ptr = (void*) (new cg_point2f());
	}

	for (unsigned int i = 0; i < 2 * spikes; i += 2) {
		/*interior points*/
		p2f(ml->lines[i].ptr) .x = cg_roundf(radius * cosf(cg_deg_to_rad((float)90 - angle)));
		if (p2f(ml->lines[i].ptr) .x < xmin) {
			xmin = p2f(ml->lines[i].ptr) .x;
		}
		p2f(ml->lines[i].ptr) .y = cg_roundf(radius * sinf(cg_deg_to_rad((float)90 - angle)));
		if (p2f(ml->lines[i].ptr) .y < ymin) {
			ymin = p2f(ml->lines[i].ptr) .y;
		}

		angle += b_angle;
		/*spikes*/
		p2f(ml->lines[i+1].ptr) .x = cg_roundf(bigrad * cosf(cg_deg_to_rad((float)90 - angle)));
		if (p2f(ml->lines[i+1].ptr) .x < xmin) {
			xmin = p2f(ml->lines[i+1].ptr) .x;
		}
		p2f(ml->lines[i+1].ptr) .y = cg_roundf(bigrad * sinf(cg_deg_to_rad((float)90 - angle)));
		if (p2f(ml->lines[i+1].ptr) .y < ymin) {
			ymin = p2f(ml->lines[i+1].ptr) .y;
		}
		angle += b_angle;
	}

	p2f(ml->lines[2 * spikes].ptr) .x = p2f(ml->lines[0].ptr) .x;
	p2f(ml->lines[2 * spikes].ptr) .y = p2f(ml->lines[0].ptr) .y;

	/*rebase*/
	for (unsigned int i = 0; i < 2 * spikes + 1; i++) {
		p2f(ml->lines[i].ptr) .x -= xmin;
		p2f(ml->lines[i].ptr) .y -= ymin;
	}

	return fig;

}

}

