/*
 * StretchableDefs.h
 *
 *  Created on: 13/set/2012
 *      Author: alberto
 */

#ifndef STRETCHABLEDEFS_H_
#define STRETCHABLEDEFS_H_

#include <stddef.h>
#include "CG_Common/basic_entities.hh"

// actually useless definitions

namespace CGLib {

typedef struct {
	float xw; //width coefficient to calculate x
	float xt; //thickness coefficient to calculate x
	float yh; //height coefficient to calculate y
	float yt; //thickness coefficient to calculate x
} stretch_point2f;

typedef struct {
	/* center */
	float xw; //width coefficient to calculate x of center
	float xt; //thickness coefficient to calculate x of center
	float yh; //height coefficient to calculate y of center
	float yt; //thickness coefficient to calculate x of center
	/* radius */
	float rw; //width coefficient to calculate radius
	float rh; //height coefficient to calculate radius
	float rt; //thickness coefficient to calculate radius
	float begAngle;
	float endAngle;
} stretch_circle2f;

typedef struct {
	cg_line_type type;
	union line {
		stretch_point2f* point;
		stretch_circle2f* circle;
	};
} stretch_line2f;


}

#endif /* STRETCHABLEDEFS_H_ */
