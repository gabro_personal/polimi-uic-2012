/*
 * CGMultiLineIterator.hpp
 *
 *  Created on: 16/set/2012
 *      Author: alberto
 */

#ifndef CGMULTILINEITERATOR_HPP_
#define CGMULTILINEITERATOR_HPP_

#include <iterator>
#include "CG_Line_Iterator.hpp"
#include "CG_Common/basic_entities.hh"
using namespace CGLib;

namespace CGLib {

/*!
 * \class iterator to iterate over the multilines stored in a CG_Figure object.
 *
 * \author Alberto Scolari
 */
class CG_MultiLine_Iterator: public std::iterator<std::input_iterator_tag,
		CGLib::multiLine2f> {

public:
	CG_MultiLine_Iterator(const cg_figure2f* const obj, unsigned int begin);
	CG_MultiLine_Iterator(const CG_MultiLine_Iterator& it);
	bool operator==(const CG_MultiLine_Iterator& it);
	bool operator!=(const CG_MultiLine_Iterator& it);
	CG_MultiLine_Iterator& operator++();
	CG_MultiLine_Iterator operator++(int);
	const multiLine2f& operator*();
	CG_Line_Iterator begin();
	CG_Line_Iterator end();

private:
	const cg_figure2f* const figure;
	unsigned int actual;
};

} /* namespace CGLib */
#endif /* CGMULTILINEITERATOR_HPP_ */
