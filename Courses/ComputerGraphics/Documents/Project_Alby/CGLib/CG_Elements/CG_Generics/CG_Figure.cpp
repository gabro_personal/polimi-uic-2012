/*
 * CGFigure.cpp
 *
 *  Created on: 16/set/2012
 *      Author: alberto
 */

#include "CG_Figure.hpp"
#include <stdio.h>
#include "CG_Common/basic_entities.hh"
#include "CG_MultiLine_Iterator.hpp"
using namespace CGLib;

namespace CGLib {

CG_Figure::CG_Figure(cg_figure2f* fig) {
	this->figure = fig;
}

CG_Figure::~CG_Figure() {
	if (this->figure == NULL) {
		return;
	}
	for (unsigned int i = 0; i < figure->multilinesNum; i++) {
		multiLine2f* ml = figure->multilines;
		for (unsigned int j = 0; j < ml[i].linesNum; j++) {
			switch (ml[i].lines[j].type) {
			case (POINT):
				delete (cg_point2f*)(ml[i].lines[j].ptr);
				break;
			case (CIRCLE):
				delete (cg_circle2f*)(ml[i].lines[j].ptr);
				break;
			}
		}
		delete ml->lines;

	}
	delete figure->multilines;
	delete figure;
}

CG_MultiLine_Iterator CG_Figure::mlbegin() {
	return CG_MultiLine_Iterator(figure, 0);
}

CG_MultiLine_Iterator CG_Figure::mlend() {
	return CG_MultiLine_Iterator(figure, this->figure->multilinesNum);
}

} /* namespace CGLib */
