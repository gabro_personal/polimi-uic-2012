/*!
 * \file
 *
 * \author Alberto Scolari
 *
 * \brief functions to create simple figures
 */

#ifndef CG_SIMPLE_FIGURES_HH_
#define CG_SIMPLE_FIGURES_HH_

namespace CGLib {

/*!
 * \fn cg_triangle(const unsigned int base, const int height, const int distance) of the
 * following shape
 * 							distance
 * 							||
 * 							\/
 * 						   *____*			*
 * 						   |    /\			|
 * 						   |   /  \			|
 * 						   |  /	   \		| <-height
 * 						   | /	    \		|
 * 						   * --------		*
 *
 * 						   *_________*
 * 						   		/\
 * 								||
 * 								base
 * \brief returns a triangle
 *
 * \param base the length of the base
 * \param height the height of the triangle (it can also be negative for the third vertex
 * 			to be under the base)
 * \param distance distance of the third vertex
 * \return the pointer to the cg_figure2f storing the triangle vertices
 */
cg_figure2f* cg_triangle(const unsigned int base, const int height, const int distance);


/*!
 * \brief creates a trapezoid
 *
 * 					topdistance		topbase
 * 							||		  ||
 * 							\/		  \/
 * 						   *____*__________*		*
 * 						   |    /----------\		|
 * 						   |   /			\		|
 * 						   |  /				 \		| <-height
 * 						   | /				  \		|
 * 						   * ------------------		*
 *
 * 						   *____________________*
 * 						   		/\
 * 								||
 * 							lowbase
 *
 * \param lowbase the length of the bottom base
 * \param height the height, negative to "reverse" vertically the trapezoid shape
 * \param topbase the length of the top base
 * \param topdistance the distance of the top left vertex from the vertical line
 * 			beginning in the bottom - left vertex
 * \return the pointer to the cg_figure2f storing the trapezoid vertices
 */
cg_figure2f* cg_trapezium(const unsigned int lowbase, const int height,
		const unsigned int topbase, int topdistance);

/*!
 * \brief creates a star with the given number of spikes, the given height from the
 * 			spike to the basis of the triangle ending into the spike and the given
 * 			triangle base
 *
 *
 *							A SPIKE TRIANGLE (the others are rotated)
 *
 * 						   					*
 * 						       /\			|
 * 						      /  \			|
 * 						     /	  \			| <-height
 * 						    /	   \		|
 * 						   * -------*		*
 *
 * 						   *_________*
 * 						   		/\
 * 								||
 * 							   base
 *
 * \param spikes number of spikes
 * \param height height of each spike-triangle
 * \param base the length of the base
 * @return
 */
cg_figure2f* cg_star(const unsigned int spikes, const unsigned int height,
		const unsigned int base);

}

#endif /* CG_SIMPLE_FIGURES_HH_ */
