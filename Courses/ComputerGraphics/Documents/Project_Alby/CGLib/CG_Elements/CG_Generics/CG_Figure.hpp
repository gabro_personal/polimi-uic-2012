/*
 * CGFigure.hpp
 *
 *  Created on: 16/set/2012
 *      Author: alberto
 */

#ifndef CGFIGURE_HPP_
#define CGFIGURE_HPP_
#include "CG_MultiLine_Iterator.hpp"
#include "CG_Common/basic_entities.hh"
using namespace CGLib;

/*!
 * \class read-only class storing a figure, defined as a sequence of vertices
 * or circumferences
 *
 * \author Alberto Scolari
 */
namespace CGLib {

class CG_Figure {
public:
	CG_Figure(cg_figure2f* fig);
	~CG_Figure();
	CGLib::CG_MultiLine_Iterator mlbegin();
	CGLib::CG_MultiLine_Iterator mlend();


protected:
	cg_figure2f* figure;
};

} /* namespace CGLib */

#endif /* CGFIGURE_HPP_ */
