/*
 * CGMultiLineIterator.cpp
 *
 *  Created on: 16/set/2012
 *      Author: alberto
 */

#include "CG_MultiLine_Iterator.hpp"
#include <stddef.h>
using namespace CGLib;

namespace CGLib {

CG_MultiLine_Iterator::CG_MultiLine_Iterator(const cg_figure2f* const obj,
		unsigned int begin) :
		figure(obj) {
	this->actual = begin;
}

CG_MultiLine_Iterator::CG_MultiLine_Iterator(const CG_MultiLine_Iterator& it) :
		figure(it.figure) {
	this->actual = it.actual;
}

bool CG_MultiLine_Iterator::operator==(const CG_MultiLine_Iterator& it) {
	return (actual == it.actual) && (figure == it.figure);
}

bool CG_MultiLine_Iterator::operator!=(const CG_MultiLine_Iterator& it) {
	return (actual != it.actual) || (figure != it.figure);
}

CG_MultiLine_Iterator& CG_MultiLine_Iterator::operator++() {
	++actual;
	return *this;
}

CG_MultiLine_Iterator CG_MultiLine_Iterator::operator ++(int) {
	CG_MultiLine_Iterator tmp = CG_MultiLine_Iterator(*this);
	CG_MultiLine_Iterator::operator++();
	return tmp;
}

const multiLine2f& CG_MultiLine_Iterator::operator*() {
	if (actual < figure->multilinesNum) {
		return figure->multilines[actual];
	} else {
		// always the last element
		return figure->multilines[figure->multilinesNum-1];
	}
}

CG_Line_Iterator CG_MultiLine_Iterator::begin() {
	return CG_Line_Iterator(&(this->figure->multilines[actual]),0);
}

CG_Line_Iterator CG_MultiLine_Iterator::end() {
	return CG_Line_Iterator(&(this->figure->multilines[actual]),this->figure->multilines[actual].linesNum);
}

} /* namespace CGLib */
