/*
 * CGLineIterator.hpp
 *
 *  Created on: 16/set/2012
 *      Author: alberto
 */

#ifndef CGLINEITERATOR_HPP_
#define CGLINEITERATOR_HPP_

#include "CG_Common/basic_entities.hh"
using namespace CGLib;


namespace CGLib {

/*!
 * \class iterator to iterate over the lines of a single multiline stored in a CG_Figure object.
 *
 * \author Alberto Scolari
 */
class CG_Line_Iterator {

public:
	CG_Line_Iterator(const multiLine2f* const obj, unsigned int begin);
	CG_Line_Iterator(const CG_Line_Iterator& it);
	bool operator==(const CG_Line_Iterator& it);
	bool operator!=(const CG_Line_Iterator& it);
	CG_Line_Iterator& operator++();
	CG_Line_Iterator operator++(int);
	const cg_line& operator*();
	const cg_line* operator->() const;

private:
	const multiLine2f* const mline;
	unsigned int actual;
};

} /* namespace CGLib */
#endif /* CGLINEITERATOR_HPP_ */
