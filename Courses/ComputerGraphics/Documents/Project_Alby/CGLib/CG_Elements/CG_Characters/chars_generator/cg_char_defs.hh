/*
 * cg_char_defs.h
 *
 *  Created on: 13/set/2012
 *      Author: alberto
 */

#ifndef CG_CHAR_DEFS_H_
#define CG_CHAR_DEFS_H_

#include "CG_Common/basic_entities.hh"
#include <stddef.h>

//#define DEF_THICK 10

namespace CGLib {

/*!
 * \fn cg_figure2f* init_letter(char ch)
 *
 * \brief function to get the description of a character in terms of vertices and circumferences
 *
 * \param ch the character to be descrpted
 * \return the pointer to the character representation
 */
cg_figure2f* init_letter(char ch);

}

#endif /* CG_CHAR_DEFS_H_ */
