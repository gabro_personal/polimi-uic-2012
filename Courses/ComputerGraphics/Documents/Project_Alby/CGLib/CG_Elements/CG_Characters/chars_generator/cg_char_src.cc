/*!
 * \file
 *
 * \brief the present file contains the hard-coded representation of some characters
 *
 * The functions are because it is not possible to initialize
 * a so complicated data structure statically.
 *
 * \author Alberto Scolari
 */

#include <stddef.h>

#include "cg_char_defs.hh"
#include "CG_Common/basic_entities.hh"
using namespace CGLib;

namespace CGLib {

/*!
 * \brief basic initializer of a cg_figure2f describing a character
 *
 * \param a pointer to the cg_figure2f
 * \param defh default height of the character
 * \param defw default width of the character
 * \param multilines number of multilines describing the character
 * \param lines number of lines per multiline
 */
static void init_vector(cg_figure2f* const a, const unsigned int defh,
		const unsigned int defw, const unsigned int multilines,
		const unsigned int* const lines) {

	a->height = defh;
	a->width = defw;
	a->multilinesNum = multilines;
	a->multilines = new multiLine2f[multilines];
	for (unsigned int i = 0; i < multilines; i++) {
		a->multilines[i].linesNum = lines[i];
		//a->multilines[i].types = new CGlineType[lines[i]];
		a->multilines[i].lines = new cg_line[lines[i]];
	}

}

/* default measures */
#define DEF_HEIGHT 200
#define DEF_WIDTH 140
#define DEF_THICK 20

/*!
 * \brief initializer of letter A
 * \return the letter representation
 */
static cg_figure2f* init_A(void) {
	cg_figure2f* const a = new cg_figure2f;
	const unsigned int mltl = 2;
	const unsigned int lines[mltl] = { 9, 4 };

	/* multiline 0 */
	init_vector(a, DEF_HEIGHT, DEF_WIDTH, mltl, lines);

	for (unsigned int i = 0; i < a->multilines[0].linesNum; i++) {
		a->multilines[0].lines[i].type = POINT;
		a->multilines[0].lines[i].ptr = (void*) (new cg_point2f);
	}
	/* outer border of letter A */p2f((a->multilines[0].lines[0].ptr)) .x = 0;
	p2f(a->multilines[0].lines[0].ptr) .y = 0;

	p2f(a->multilines[0].lines[1].ptr) .x = DEF_THICK;
	p2f(a->multilines[0].lines[1].ptr) .y = 0;

	p2f(a->multilines[0].lines[2].ptr) .x = DEF_THICK * 2;
	p2f(a->multilines[0].lines[2].ptr) .y = (DEF_THICK * 9) / 5;

	p2f(a->multilines[0].lines[3].ptr) .x = DEF_WIDTH - 2 * DEF_THICK;
	p2f(a->multilines[0].lines[3].ptr) .y = (DEF_THICK * 9) / 5;

	p2f(a->multilines[0].lines[4].ptr) .x = DEF_WIDTH - DEF_THICK;
	p2f(a->multilines[0].lines[4].ptr) .y = 0;

	p2f(a->multilines[0].lines[5].ptr) .x = DEF_WIDTH;
	p2f(a->multilines[0].lines[5].ptr) .y = 0;

	p2f(a->multilines[0].lines[6].ptr) .x = DEF_WIDTH / 2 + DEF_THICK / 2;
	p2f(a->multilines[0].lines[6].ptr) .y = DEF_HEIGHT;

	p2f(a->multilines[0].lines[7].ptr) .x = DEF_WIDTH / 2 - DEF_THICK / 2;
	p2f(a->multilines[0].lines[7].ptr) .y = DEF_HEIGHT;

	p2f(a->multilines[0].lines[8].ptr) .x = 0;
	p2f(a->multilines[0].lines[8].ptr) .y = 0;

	/* internal triangle */
	for (unsigned int i = 0; i < a->multilines[1].linesNum; i++) {
		a->multilines[1].lines[i].type = POINT;
		a->multilines[1].lines[i].ptr = (void*) (new cg_point2f);
	}
	/* outer border of letter A */
	p2f(a->multilines[1].lines[0].ptr) .x = 45;
	p2f(a->multilines[1].lines[0].ptr) .y = 60;

	p2f(a->multilines[1].lines[1].ptr) .x = 95;
	p2f(a->multilines[1].lines[1].ptr) .y = 60;

	p2f(a->multilines[1].lines[2].ptr) .x = DEF_WIDTH / 2;
	p2f(a->multilines[1].lines[2].ptr) .y = DEF_HEIGHT - 2 * DEF_THICK;

	p2f(a->multilines[1].lines[3].ptr) .x = 45;
	p2f(a->multilines[1].lines[3].ptr) .y = 60;

	return a;
}

/*!
 * \brief initializer of letter B
 * \return the letter representation
 */
static cg_figure2f* init_B(void) {
	cg_figure2f* const b = new cg_figure2f;
	const unsigned int mltl = 3;
	const unsigned int lines[mltl] = { 7, 6, 6 };

	/* multiline 0, outer border of B */
	init_vector(b, DEF_HEIGHT, (2 * DEF_THICK + (DEF_HEIGHT - 4 * DEF_THICK) / 2), mltl,
			lines);
	b->multilines[0].lines[0].type = POINT;
	b->multilines[0].lines[0].ptr = (void*) (new cg_point2f);
	b->multilines[0].lines[1].type = POINT;
	b->multilines[0].lines[1].ptr = (void*) (new cg_point2f);
	b->multilines[0].lines[2].type = CIRCLE;
	b->multilines[0].lines[2].ptr = (void*) (new cg_circle2f);
	b->multilines[0].lines[3].type = CIRCLE;
	b->multilines[0].lines[3].ptr = (void*) (new cg_circle2f);
	b->multilines[0].lines[4].type = POINT;
	b->multilines[0].lines[4].ptr = (void*) (new cg_point2f);
	b->multilines[0].lines[5].type = POINT;
	b->multilines[0].lines[5].ptr = (void*) (new cg_point2f);
	b->multilines[0].lines[6].type = POINT;
	b->multilines[0].lines[6].ptr = (void*) (new cg_point2f);




	p2f(b->multilines[0].lines[0].ptr) .x = 0;
	p2f(b->multilines[0].lines[0].ptr) .y = 0;

	p2f(b->multilines[0].lines[1].ptr) .x = DEF_WIDTH / 2;
	/* for future parameterized expansion, could be DEF_WIDTH/2 - DEF_THICK*/
	p2f(b->multilines[0].lines[1].ptr) .y = 0;

	c2f(b->multilines[0].lines[2].ptr).x = DEF_WIDTH / 2;
	c2f(b->multilines[0].lines[2].ptr) .y = DEF_HEIGHT/4;
	c2f(b->multilines[0].lines[2].ptr) .radius = DEF_HEIGHT / 4;
	c2f(b->multilines[0].lines[2].ptr) .begAngle = 0;
	c2f(b->multilines[0].lines[2].ptr) .endAngle = 180;

	c2f(b->multilines[0].lines[3].ptr) .x = DEF_WIDTH / 2;
	c2f(b->multilines[0].lines[3].ptr) .y = 3*DEF_HEIGHT/4;
	c2f(b->multilines[0].lines[3].ptr) .radius = DEF_HEIGHT / 4;
	c2f(b->multilines[0].lines[3].ptr) .begAngle = 0;
	c2f(b->multilines[0].lines[3].ptr) .endAngle = 180;

	p2f(b->multilines[0].lines[4].ptr) .x = DEF_WIDTH / 2;
	p2f(b->multilines[0].lines[4].ptr) .y = DEF_HEIGHT;

	p2f(b->multilines[0].lines[5].ptr) .x = 0;
	p2f(b->multilines[0].lines[5].ptr) .y = DEF_HEIGHT;

	p2f(b->multilines[0].lines[6].ptr) .x = 0;
	p2f(b->multilines[0].lines[6].ptr) .y = 0;

	/* multiline 1, down inner border of B */
	b->multilines[1].lines[0].type = POINT;
	b->multilines[1].lines[0].ptr = (void*) (new cg_point2f);
	b->multilines[1].lines[1].type = POINT;
	b->multilines[1].lines[1].ptr = (void*) (new cg_point2f);
	b->multilines[1].lines[2].type = CIRCLE;
	b->multilines[1].lines[2].ptr = (void*) (new cg_circle2f);
	b->multilines[1].lines[3].type = POINT;
	b->multilines[1].lines[3].ptr = (void*) (new cg_point2f);
	b->multilines[1].lines[4].type = POINT;
	b->multilines[1].lines[4].ptr = (void*) (new cg_point2f);
	b->multilines[1].lines[5].type = POINT;
	b->multilines[1].lines[5].ptr = (void*) (new cg_point2f);

	p2f(b->multilines[1].lines[0].ptr) .x = 3*DEF_THICK/2;
	p2f(b->multilines[1].lines[0].ptr) .y = DEF_THICK;

	p2f(b->multilines[1].lines[1].ptr) .x = DEF_WIDTH / 2 - 2*DEF_THICK/3;
	p2f(b->multilines[1].lines[1].ptr) .y = DEF_THICK;

	c2f(b->multilines[1].lines[2].ptr) .x = DEF_WIDTH / 2 - 2*DEF_THICK/3;
	c2f(b->multilines[1].lines[2].ptr) .y = DEF_HEIGHT/4;
	c2f(b->multilines[1].lines[2].ptr) .radius = DEF_HEIGHT/4 - DEF_THICK;
	c2f(b->multilines[1].lines[2].ptr) .begAngle = 0;
	c2f(b->multilines[1].lines[2].ptr) .endAngle = 180;

	p2f(b->multilines[1].lines[3].ptr) .x = DEF_WIDTH / 2 - 2*DEF_THICK/3;
	p2f(b->multilines[1].lines[3].ptr) .y = DEF_HEIGHT / 2 - DEF_THICK;

	p2f(b->multilines[1].lines[4].ptr) .x = 3*DEF_THICK/2;
	p2f(b->multilines[1].lines[4].ptr) .y = DEF_HEIGHT / 2 - DEF_THICK;

	p2f(b->multilines[1].lines[5].ptr) .x = 3*DEF_THICK/2;
	p2f(b->multilines[1].lines[5].ptr) .y = DEF_THICK;



	/* multiline 2, top inner border of B */
	b->multilines[2].lines[0].type = POINT;
	b->multilines[2].lines[0].ptr = (void*) (new cg_point2f);
	b->multilines[2].lines[1].type = POINT;
	b->multilines[2].lines[1].ptr = (void*) (new cg_point2f);
	b->multilines[2].lines[2].type = CIRCLE;
	b->multilines[2].lines[2].ptr = (void*) (new cg_circle2f);
	b->multilines[2].lines[3].type = POINT;
	b->multilines[2].lines[3].ptr = (void*) (new cg_point2f);
	b->multilines[2].lines[4].type = POINT;
	b->multilines[2].lines[4].ptr = (void*) (new cg_point2f);
	b->multilines[2].lines[5].type = POINT;
	b->multilines[2].lines[5].ptr = (void*) (new cg_point2f);

	p2f(b->multilines[2].lines[0].ptr) .x = 3*DEF_THICK/2;
	p2f(b->multilines[2].lines[0].ptr) .y = DEF_HEIGHT / 2 + DEF_THICK;

	p2f(b->multilines[2].lines[1].ptr) .x = DEF_WIDTH / 2 - 2*DEF_THICK/3;
	p2f(b->multilines[2].lines[1].ptr) .y = DEF_HEIGHT / 2 + DEF_THICK;

	c2f(b->multilines[2].lines[2].ptr) .x = DEF_WIDTH / 2 - 2*DEF_THICK/3;
	c2f(b->multilines[2].lines[2].ptr) .y = 3*DEF_HEIGHT/4;
	c2f(b->multilines[2].lines[2].ptr) .radius = DEF_HEIGHT/4 - DEF_THICK;
	c2f(b->multilines[2].lines[2].ptr) .begAngle = 0;
	c2f(b->multilines[2].lines[2].ptr) .endAngle = 180;

	p2f(b->multilines[2].lines[3].ptr) .x = DEF_WIDTH / 2 - 2*DEF_THICK/3;
	p2f(b->multilines[2].lines[3].ptr) .y = DEF_HEIGHT - DEF_THICK;

	p2f(b->multilines[2].lines[4].ptr) .x = 3*DEF_THICK/2;
	p2f(b->multilines[2].lines[4].ptr) .y = DEF_HEIGHT - DEF_THICK;
	p2f(b->multilines[2].lines[5].ptr) .x = 3*DEF_THICK/2;
	p2f(b->multilines[2].lines[5].ptr) .y = DEF_HEIGHT / 2 + DEF_THICK;

	return b;
}

/*!
 * \brief initializer of letter C
 * \return the letter representation
 */
static cg_figure2f* init_C(void) {
	cg_figure2f* const c = new cg_figure2f;
	const unsigned int mltl = 1;
	const unsigned int lines[mltl] = { 6 };

	init_vector(c, DEF_HEIGHT, DEF_HEIGHT, mltl, lines);
	c->multilines[0].lines[0].type = POINT;
	c->multilines[0].lines[0].ptr = (void*) (new cg_point2f);
	c->multilines[0].lines[1].type = POINT;
	c->multilines[0].lines[1].ptr = (void*) (new cg_point2f);
	c->multilines[0].lines[2].type = CIRCLE;
	c->multilines[0].lines[2].ptr = (void*) (new cg_circle2f);
	c->multilines[0].lines[3].type = POINT;
	c->multilines[0].lines[3].ptr = (void*) (new cg_point2f);
	c->multilines[0].lines[4].type = POINT;
	c->multilines[0].lines[4].ptr = (void*) (new cg_point2f);
	c->multilines[0].lines[5].type = CIRCLE;
	c->multilines[0].lines[5].ptr = (void*) (new cg_circle2f);

	p2f(c->multilines[0].lines[0].ptr) .x = DEF_HEIGHT+1;
	p2f(c->multilines[0].lines[0].ptr) .y = 1;

	p2f(c->multilines[0].lines[1].ptr) .x = DEF_HEIGHT+1;
	p2f(c->multilines[0].lines[1].ptr) .y = DEF_THICK-1;

	c2f(c->multilines[0].lines[2].ptr) .x = DEF_HEIGHT;
	c2f(c->multilines[0].lines[2].ptr) .y = DEF_HEIGHT / 2;
	c2f(c->multilines[0].lines[2].ptr) .radius = DEF_HEIGHT / 2 - DEF_THICK;
	c2f(c->multilines[0].lines[2].ptr) .begAngle = 180;
	c2f(c->multilines[0].lines[2].ptr) .endAngle = 360;

	p2f(c->multilines[0].lines[3].ptr) .x = DEF_HEIGHT+1;
	p2f(c->multilines[0].lines[3].ptr) .y = DEF_HEIGHT - DEF_THICK;

	p2f(c->multilines[0].lines[4].ptr) .x = DEF_HEIGHT+1;
	p2f(c->multilines[0].lines[4].ptr) .y = DEF_HEIGHT-1;

	c2f(c->multilines[0].lines[5].ptr) .x = DEF_HEIGHT;
	c2f(c->multilines[0].lines[5].ptr) .y = DEF_HEIGHT / 2;
	c2f(c->multilines[0].lines[5].ptr) .radius = DEF_HEIGHT / 2;
	c2f(c->multilines[0].lines[5].ptr) .begAngle = 180;
	c2f(c->multilines[0].lines[5].ptr) .endAngle = 360;
	return c;
}

/*!
 * \brief initializer of letter L
 * \return the letter representation
 */
static cg_figure2f* init_L(void) {
	cg_figure2f* const l = new cg_figure2f;
	const unsigned int mltl = 1;
	const unsigned int lines[mltl] = { 7 };

	/* multiline 0, outer border of B */
	init_vector(l, DEF_HEIGHT, (3 * DEF_WIDTH) / 4, mltl, lines);
	for (unsigned int i = 0; i < l->multilines[0].linesNum; i++) {
		l->multilines[0].lines[i].type = POINT;
		l->multilines[0].lines[i].ptr = (void*) (new cg_point2f);
	}

	p2f(l->multilines[0].lines[0].ptr) .x = 0;
	p2f(l->multilines[0].lines[0].ptr) .y = 0;

	p2f(l->multilines[0].lines[1].ptr) .x = (3 * DEF_WIDTH) / 4;
	p2f(l->multilines[0].lines[1].ptr) .y = 0;

	p2f(l->multilines[0].lines[2].ptr) .x = (3 * DEF_WIDTH) / 4;
	p2f(l->multilines[0].lines[2].ptr) .y = DEF_THICK;

	p2f(l->multilines[0].lines[3].ptr) .x = DEF_THICK;
	p2f(l->multilines[0].lines[3].ptr) .y = DEF_THICK;

	p2f(l->multilines[0].lines[4].ptr) .x = DEF_THICK;
	p2f(l->multilines[0].lines[4].ptr) .y = DEF_HEIGHT;

	p2f(l->multilines[0].lines[5].ptr) .x = 0;
	p2f(l->multilines[0].lines[5].ptr) .y = DEF_HEIGHT;

	p2f(l->multilines[0].lines[6].ptr) .x = 0;
	p2f(l->multilines[0].lines[6].ptr) .y = 0;

	return l;
}

/*!
 * \brief initializer of letter O
 * \return the letter representation
 */
static cg_figure2f* init_O(void) {
	cg_figure2f* const o = new cg_figure2f;
	const unsigned int mltl = 1;
	const unsigned int lines[mltl] = { 2 };

	/* multiline 0, outer border of B */
	init_vector(o, DEF_HEIGHT, DEF_HEIGHT, mltl, lines);
	for (unsigned int i = 0; i < o->multilines[0].linesNum; i++) {
		o->multilines[0].lines[i].type = CIRCLE;
		o->multilines[0].lines[i].ptr = (void*) (new cg_circle2f);
	}

	c2f(o->multilines[0].lines[0].ptr) .x = DEF_HEIGHT / 2;
	c2f(o->multilines[0].lines[0].ptr) .y = DEF_HEIGHT / 2;
	c2f(o->multilines[0].lines[0].ptr) .radius = DEF_HEIGHT / 2 - DEF_THICK;
	c2f(o->multilines[0].lines[0].ptr) .begAngle = 0;
	c2f(o->multilines[0].lines[0].ptr) .endAngle = 360;

	c2f(o->multilines[0].lines[1].ptr) .x = DEF_HEIGHT / 2;
	c2f(o->multilines[0].lines[1].ptr) .y = DEF_HEIGHT / 2;
	c2f(o->multilines[0].lines[1].ptr) .radius = DEF_HEIGHT / 2;
	c2f(o->multilines[0].lines[1].ptr) .begAngle = 0;
	c2f(o->multilines[0].lines[1].ptr) .endAngle = 360;

	return o;
}

/*!
 * \brief initializer of letter S
 * \return the letter representation
 */
static cg_figure2f* init_S(void) {
	cg_figure2f* const s = new cg_figure2f;
	const unsigned int mltl = 1;
	const unsigned int lines[mltl] = { 13 };

	/* multiline 0, outer border of B */
	init_vector(s, DEF_HEIGHT, (DEF_HEIGHT - 3 * DEF_THICK) / 2 - DEF_THICK, mltl, lines);

	s->multilines[0].lines[0].type = POINT;
	s->multilines[0].lines[0].ptr = (void*) (new cg_point2f);
	s->multilines[0].lines[1].type = POINT;
	s->multilines[0].lines[1].ptr = (void*) (new cg_point2f);
	s->multilines[0].lines[2].type = CIRCLE;
	s->multilines[0].lines[2].ptr = (void*) (new cg_circle2f);
	s->multilines[0].lines[3].type = CIRCLE;
	s->multilines[0].lines[3].ptr = (void*) (new cg_circle2f);
	s->multilines[0].lines[4].type = POINT;
	s->multilines[0].lines[4].ptr = (void*) (new cg_point2f);
	s->multilines[0].lines[5].type = POINT;
	s->multilines[0].lines[5].ptr = (void*) (new cg_point2f);
	s->multilines[0].lines[6].type = POINT;
	s->multilines[0].lines[6].ptr = (void*) (new cg_point2f);
	s->multilines[0].lines[7].type = POINT;
	s->multilines[0].lines[7].ptr = (void*) (new cg_point2f);
	s->multilines[0].lines[8].type = CIRCLE;
	s->multilines[0].lines[8].ptr = (void*) (new cg_circle2f);
	s->multilines[0].lines[9].type = CIRCLE;
	s->multilines[0].lines[9].ptr = (void*) (new cg_circle2f);
	s->multilines[0].lines[10].type = POINT;
	s->multilines[0].lines[10].ptr = (void*) (new cg_point2f);
	s->multilines[0].lines[11].type = POINT;
	s->multilines[0].lines[11].ptr = (void*) (new cg_point2f);
	s->multilines[0].lines[12].type = POINT;
	s->multilines[0].lines[12].ptr = (void*) (new cg_point2f);

	p2f(s->multilines[0].lines[0].ptr) .x = 0;
	p2f(s->multilines[0].lines[0].ptr) .y = 0;

	p2f(s->multilines[0].lines[1].ptr) .x = (DEF_HEIGHT - 3 * DEF_THICK) / 4+ DEF_THICK;
	p2f(s->multilines[0].lines[1].ptr) .y = 0;

	c2f(s->multilines[0].lines[2].ptr) .x = (DEF_HEIGHT - 3 * DEF_THICK)/ 4+ DEF_THICK;
	c2f(s->multilines[0].lines[2].ptr) .y = (DEF_HEIGHT - 3 * DEF_THICK) / 4+ DEF_THICK;
	c2f(s->multilines[0].lines[2].ptr) .radius = (DEF_HEIGHT - 3 * DEF_THICK) / 4 + DEF_THICK;
	c2f(s->multilines[0].lines[2].ptr) .begAngle = 0;
	c2f(s->multilines[0].lines[2].ptr) .endAngle = 180;

	c2f(s->multilines[0].lines[3].ptr) .x = (DEF_HEIGHT - 3 * DEF_THICK) / 4+ DEF_THICK;
	c2f(s->multilines[0].lines[3].ptr) .y = DEF_HEIGHT - (DEF_HEIGHT - 3 * DEF_THICK) / 4 - DEF_THICK;
	c2f(s->multilines[0].lines[3].ptr) .radius = (DEF_HEIGHT - 3 * DEF_THICK) / 4;
	c2f(s->multilines[0].lines[3].ptr) .begAngle = 180;
	c2f(s->multilines[0].lines[3].ptr) .endAngle = 360;

	p2f(s->multilines[0].lines[4].ptr) .x = (DEF_HEIGHT - 3 * DEF_THICK) / 4+ DEF_THICK;
	p2f(s->multilines[0].lines[4].ptr) .y = DEF_HEIGHT - DEF_THICK;

	p2f(s->multilines[0].lines[5].ptr) .x = (DEF_HEIGHT - 3 * DEF_THICK) / 2 + 2*DEF_THICK;
	p2f(s->multilines[0].lines[5].ptr) .y = DEF_HEIGHT - DEF_THICK;

	p2f(s->multilines[0].lines[6].ptr) .x = (DEF_HEIGHT - 3 * DEF_THICK) / 2 + 2*DEF_THICK;
	p2f(s->multilines[0].lines[6].ptr) .y = DEF_HEIGHT;

	p2f(s->multilines[0].lines[7].ptr) .x = (DEF_HEIGHT - 3 * DEF_THICK) / 4+ DEF_THICK;
	p2f(s->multilines[0].lines[7].ptr) .y = DEF_HEIGHT;

	c2f(s->multilines[0].lines[8].ptr) .x = (DEF_HEIGHT - 3 * DEF_THICK) / 4+ DEF_THICK;
	c2f(s->multilines[0].lines[8].ptr) .y = DEF_HEIGHT - (DEF_HEIGHT - 3 * DEF_THICK) / 4- DEF_THICK;
	c2f(s->multilines[0].lines[8].ptr) .radius = (DEF_HEIGHT - 3 * DEF_THICK) / 4+ DEF_THICK;
	c2f(s->multilines[0].lines[8].ptr) .begAngle = 180;
	c2f(s->multilines[0].lines[8].ptr) .endAngle = 360;

	c2f(s->multilines[0].lines[9].ptr) .x = (DEF_HEIGHT - 3 * DEF_THICK) / 4+ DEF_THICK;
	c2f(s->multilines[0].lines[9].ptr) .y = (DEF_HEIGHT - 3 * DEF_THICK) / 4+ DEF_THICK;
	c2f(s->multilines[0].lines[9].ptr) .radius = (DEF_HEIGHT - 3 * DEF_THICK) / 4;
	c2f(s->multilines[0].lines[9].ptr) .begAngle = 0;
	c2f(s->multilines[0].lines[9].ptr) .endAngle = 180;

	p2f(s->multilines[0].lines[10].ptr) .x = (DEF_HEIGHT - 3 * DEF_THICK) / 4+ DEF_THICK;
	p2f(s->multilines[0].lines[10].ptr) .y = DEF_THICK;

	p2f(s->multilines[0].lines[11].ptr) .x = 0;
	p2f(s->multilines[0].lines[11].ptr) .y = DEF_THICK;

	p2f(s->multilines[0].lines[12].ptr) .x = 0;
	p2f(s->multilines[0].lines[12].ptr) .y = 0;

	return s;
}

/*!
 * \brief initializer of letter Y
 * \return the letter representation
 */
static cg_figure2f* init_Y(void) {
	cg_figure2f* const y = new cg_figure2f;
	const unsigned int mltl = 1;
	const unsigned int lines[mltl] = { 10 };

	/* multiline 0, outer border of B */
	init_vector(y, DEF_HEIGHT, DEF_WIDTH, mltl, lines);

	for (unsigned int i = 0; i < y->multilines[0].linesNum; i++) {
		y->multilines[0].lines[i].type = POINT;
		y->multilines[0].lines[i].ptr = (void*) (new cg_point2f());
	}

	p2f(y->multilines[0].lines[0].ptr) .x = DEF_WIDTH / 2 - DEF_THICK / 2;
	p2f(y->multilines[0].lines[0].ptr) .y = 0;

	p2f(y->multilines[0].lines[1].ptr) .x = DEF_WIDTH / 2 + DEF_THICK / 2;
	p2f(y->multilines[0].lines[1].ptr) .y = 0;

	p2f(y->multilines[0].lines[2].ptr) .x = DEF_WIDTH / 2 + DEF_THICK / 2;
	p2f(y->multilines[0].lines[2].ptr) .y = DEF_HEIGHT / 2;

	p2f(y->multilines[0].lines[3].ptr) .x = DEF_WIDTH;
	p2f(y->multilines[0].lines[3].ptr) .y = DEF_HEIGHT;

	p2f(y->multilines[0].lines[4].ptr) .x = DEF_WIDTH - DEF_THICK;
	p2f(y->multilines[0].lines[4].ptr) .y = DEF_HEIGHT;

	p2f(y->multilines[0].lines[5].ptr) .x = DEF_WIDTH / 2;
	p2f(y->multilines[0].lines[5].ptr) .y = DEF_HEIGHT / 2 + DEF_THICK;

	p2f(y->multilines[0].lines[6].ptr) .x = DEF_THICK;
	p2f(y->multilines[0].lines[6].ptr) .y = DEF_HEIGHT;

	p2f(y->multilines[0].lines[7].ptr) .x = 0;
	p2f(y->multilines[0].lines[7].ptr) .y = DEF_HEIGHT;

	p2f(y->multilines[0].lines[8].ptr) .x = DEF_WIDTH / 2 - DEF_THICK / 2;
	p2f(y->multilines[0].lines[8].ptr) .y = DEF_HEIGHT / 2;

	p2f(y->multilines[0].lines[9].ptr) .x = DEF_WIDTH / 2 - DEF_THICK / 2;
	p2f(y->multilines[0].lines[9].ptr) .y = 0;

	return y;
}

#define UNDEF_CHAR &init_A /*!< arbitrary pointer for non-implemented character shapes */

typedef cg_figure2f* char_init(void);

/*!
 * \var static char_init* c_init[26]
 *
 * \brief array of letters initializers
 */
static char_init* c_init[26] = {

/* letter A */&init_A,
/* letter B */&init_B,
/* letter C */&init_C, /* letter D */UNDEF_CHAR, /* letter E */UNDEF_CHAR,
		/* letter F */UNDEF_CHAR, /* letter G */UNDEF_CHAR, /* letter H */UNDEF_CHAR,
		/* letter I */UNDEF_CHAR, /* letter J */UNDEF_CHAR, /* letter K */UNDEF_CHAR,
		/* letter L */&init_L, /* letter M */UNDEF_CHAR, /* letter N */UNDEF_CHAR,
		/* letter O */&init_O, /* letter P */UNDEF_CHAR, /* letter Q */UNDEF_CHAR,
		/* letter R */UNDEF_CHAR,
		/* letter S */&init_S, /* letter T */UNDEF_CHAR, /* letter U */UNDEF_CHAR,
		/* letter V */UNDEF_CHAR, /* letter W */UNDEF_CHAR, /* letter X */UNDEF_CHAR,
		/* letter Y */&init_Y, /* letter Z */UNDEF_CHAR };

#define CHAR_INDEX(ch) (((unsigned int)(ch))-((unsigned int)'A')) /*!< ASCII number representing character ch */

/*!
 * \fn cg_figure2f* init_letter(char ch)
 *
 * \brief character initializer function: it is invoked outisde
 *
 * \param ch character to be represented
 * \return the pointer to the character representation
 */
cg_figure2f* init_letter(char ch) {
	if (ch < 'A' || ch > 'Z') {
		return (*UNDEF_CHAR)();
	} else {
		return c_init[CHAR_INDEX(ch)]();
	}
}

}

