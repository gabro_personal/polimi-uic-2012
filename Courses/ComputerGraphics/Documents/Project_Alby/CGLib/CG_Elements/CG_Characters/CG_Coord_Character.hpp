/*
 * CGCharacter.h
 *
 *  Created on: 13/set/2012
 *      Author: alberto
 */

#ifndef CGCHARACTER_H_
#define CGCHARACTER_H_

#include "CG_Elements/CG_Generics/CG_Figure.hpp"
#include "chars_generator/cg_char_defs.hh"
using namespace CGLib;

namespace CGLib {

/*!
 * \class class holding a character representation defined in terms of vertices
 *
 * \author Alberto Scolari
 */
class CG_Coord_Character: public CG_Figure {
public:
	CG_Coord_Character(char ch): CG_Figure(init_letter(ch)){};
	unsigned int getWidth();
	unsigned int getHeight();
};

} /* namespace CGLib */
#endif /* CGCHARACTER_H_ */
