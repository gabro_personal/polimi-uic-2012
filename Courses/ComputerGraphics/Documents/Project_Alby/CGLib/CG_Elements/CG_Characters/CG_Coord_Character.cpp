/*
 * CGCharacter.cpp
 *
 *  Created on: 13/set/2012
 *      Author: alberto
 */

#include "CG_Coord_Character.hpp"
#include "chars_generator/cg_char_defs.hh"
using namespace CGLib;

namespace CGLib {

unsigned int CG_Coord_Character::getWidth() {
	return this->figure->width;
}

unsigned int CG_Coord_Character::getHeight() {
	return this->figure->height;
}

} /* namespace CGLib */
