/*
 * cg_draw_routines.cc
 *
 *  Created on: 18/set/2012
 *      Author: alberto
 */
#include <cmath>

#include <stddef.h>
#include "cg_draw_routines.hh"
#include "CG_Common/basic_entities.hh"
#include "CG_Elements/CG_Generics/CG_MultiLine_Iterator.hpp"
#include "CG_Elements/CG_Generics/CG_Line_Iterator.hpp"
#include "CG_Engine/CG_2D/cg_drawing_primitives.hh"
#include "CG_Common/basic_functions.hh"
#include "CG_Elements/CG_Generics/CG_Figure.hpp"
#include "CG_Engine/CG_2D/cg_filling_primitives.hh"
using namespace CGLib;

typedef CG_MultiLine_Iterator ml_iterator;
typedef CG_Line_Iterator l_iterator;

namespace CGLib {

void cg_draw_segment_relative(const cg_point2f& old, const cg_point2f& cur,
		const int xrel, const int yrel) {
	cg_draw_segment(old.x + xrel, old.y + yrel, cur.x + xrel, cur.y + yrel);
}

void cg_draw_circle_relative(const cg_circle2f& cir, const int xrel, const int yrel) {
	cg_draw_circumference(cir.x + xrel, cir.y + yrel, cir.radius, cir.begAngle,
			cir.endAngle);
}

void cg_draw_figure(CG_Figure& fig, int x_begin, int y_begin) {
	cg_point2f* old_ptr = NULL;
	cg_point2f* cur_ptr = NULL;

	for (ml_iterator ml = fig.CG_Figure::mlbegin(); ml != fig.CG_Figure::mlend(); ml++) {
		old_ptr = NULL;
		for (l_iterator lit = ml.begin(); lit != ml.end(); lit++) {

			switch (lit->type) {

			case POINT:
				cur_ptr = ((cg_point2f*) (lit->ptr));
				/* don't draw at the beginning */
				if (old_ptr != NULL) {
					cg_draw_segment_relative(*old_ptr, *cur_ptr, x_begin, y_begin);
				}
				old_ptr = cur_ptr;
				break;

			case CIRCLE:
				cg_draw_circle_relative((*((cg_circle2f*) (lit->ptr))), x_begin, y_begin);
				old_ptr = NULL;
				break;

			default:
				break;
			}
		}
	}
}

/*
 void cg_draw_figure_rough(cg_figure2f* fig, int x_begin, int y_begin) {
 cg_point2f* old_ptr = NULL;
 cg_point2f* cur_ptr = NULL;

 for (int ml = 0; ml < fig->multilinesNum; ml++) {
 old_ptr = NULL;
 for (int lit = 0; lit < fig->multilines[ml].linesNum; lit++) {

 cg_line lin = fig->multilines[ml].lines[lit];

 switch (lin.type) {

 case POINT:
 cur_ptr = ((cg_point2f*) (lin.ptr));
 if (old_ptr != NULL) {
 cg_draw_segment_relative(*old_ptr, *cur_ptr, x_begin, y_begin);
 }
 old_ptr = cur_ptr;
 break;

 case CIRCLE:
 cg_draw_circle_relative((*((cg_circle2f*) (lin.ptr))), x_begin, y_begin);
 old_ptr = NULL;
 break;

 default:
 break;
 }
 }
 }
 }
 */

void cg_polygon_fill_figure(CG_Figure& fig, int x_begin, int y_begin) {

	cg_polygon_new_fill();
	for (ml_iterator ml = fig.CG_Figure::mlbegin(); ml != fig.CG_Figure::mlend(); ml++) {
		for (l_iterator lit = ml.begin(); lit != ml.end(); lit++) {

			switch (lit->type) {

			case POINT:
				cg_polygon_add_vertex(p2f(lit->ptr) .x + x_begin,
						p2f(lit->ptr) .y + y_begin);
				break;

			case CIRCLE:
				cg_polygon_add_circle(c2f(lit->ptr) .x + x_begin,
						c2f(lit->ptr) .y + y_begin, c2f(lit->ptr) .radius,
						c2f(lit->ptr) .begAngle, c2f(lit->ptr) .endAngle);
				break;

			default:
				break;
			}
		}
		cg_close_polygon();
	}
	cg_polygon_draw();
}


}  // namespace CGLib
