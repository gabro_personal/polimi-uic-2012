/*!
 * \file
 *
 * \brief drawing and filling primitives for complex data structures
 * 			like cg_ structs and CG_Figure
 *
 * \author Alberto Scolari
 */

#ifndef CG_DRAW_ROUTINES_HH_
#define CG_DRAW_ROUTINES_HH_

#include "CG_Common/basic_entities.hh"
#include "CG_Elements/CG_Generics/CG_Figure.hpp"
using namespace CGLib;

namespace CGLib {

/*!
 * \fn void cg_draw_segment_relative(const cg_point2f& beg_point, const cg_point2f& end_point, const int x_begin, const int y_begin)
 *
 * \brief draws a segment specified by two vertices and the displacement coordinates to add
 * to the vertices
 *
 * \param beg_point the initial vertex
 * \param end_point the final vertex
 * \param x_begin the displacement x coordinate
 * \param y_begin the displacement y coordinate
 */
void cg_draw_segment_relative(const cg_point2f& beg_point,
		const cg_point2f& end_point, const int x_begin, const int y_begin);

/*!
 * \fn cg_draw_circle_relative(const cg_circle2f& cir, const int x_begin, const int y_begin)
 *
 * \brief draws a circle with the center specified with respect to x_begin and y_begin
 *
 * \param cir the circle to be drawn
 * \param x_begin the displacement x coordinate
 * \param y_begin the displacement y coordinate
 */
void cg_draw_circle_relative(const cg_circle2f& cir, const int x_begin,
		const int y_begin);

/*!
 * \fn void cg_draw_figure(CG_Figure& fig, const int x_begin, const int y_begin)
 *
 * \brief draws a CG_Figure
 *
 * \param fig
 * \param x_begin the displacement x coordinate
 * \param y_begin the displacement y coordinate
 */
void cg_draw_figure(CG_Figure& fig, const int x_begin, const int y_begin);

/*!
 * \fn void cg_polygon_fill_figure(CG_Figure& fig, int x_begin, int y_begin)
 *
 * \brief fills a CG_Figure
 *
 * \param fig the figure to be filled
 * \param x_begin the displacement x coordinate
 * \param y_begin the displacement y coordinate
 */
void cg_polygon_fill_figure(CG_Figure& fig, int x_begin, int y_begin);

}  // namespace CGLib

#endif /* CG_DRAW_ROUTINES_HH_ */
