/*!
 * \file
 *
 * \brief definitions of basic interface between CGLib and OpenGL
 *
 * \author Alberto Scolari
 */

#ifndef DRAW_PRIMITIVE_HH_
#define DRAW_PRIMITIVE_HH_

#include <GL/glut.h>

namespace CGLib {

//void cg_init_window(int width, int height);

void inline cg_draw_point_2d(int x, int y) {
	glVertex2f(float(x), float(y));
}

inline void cg_draw(void) {
	glutSwapBuffers();
}

inline void cg_set_color(float red, float green, float blue) {
	glColor3f(red,green,blue);
}


}


#endif /* DRAW_PRIMITIVE_HH_ */
