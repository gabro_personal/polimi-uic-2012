/*
 * cg_draw_primitives.cc
 *
 *  Created on: 18/set/2012
 *      Author: alberto
 */

#include <cmath>

#include "cg_drawing_primitives.hh"
#include "Basic/cg_gl_interface.hh"
#include "CG_Common/basic_entities.hh"
#include "CG_Common/basic_functions.hh"
using namespace CGLib;

namespace CGLib {

int cg_win_height = 0;

int cg_win_width = 0;

void cg_init_window(int width, int height) {
	cg_win_width = width;
	cg_win_height = height;
	//glutInitWindowSize(width, height);
}

/*! \fn void cg_draw_segment(int xb, int yb, int xe, int ye)
 *
 * \brief draws a segment given the end points coordinates
 *
 * It draws a straight segment from (xb,yb) to (xe,ye).
 * The implementation is made accordingly to the Pittway - Van Aken algorithm.
 * This implementation tries to optimize the cycle execution by limiting branches
 * inside the main cycle and setting up the variables before, according to the slope value.
 * The sub-cases are:
 *
 * 	1) vertical line: here just the y coordinate is incremented
 *
 * 	2) slope not infinite: sub-cases depending on abs(slope);
 *
 * 		a) abs(slope) > 1: the x and y coordinates are inverted, and the referred
 * 			variables are swapped accordingly;
 *
 * 		b) abs(slope) <= 1: this is the default case, where no variable pair is swapped;
 *
 * See the code comments for more details.
 *
 *
 * \param xb	horizontal coordinate of initial point
 * \param yb	vertical coordinate of initial point
 * \param xe	horizontal coordinate of final point
 * \param ye	vertical coordinate of final point
 */
void cg_draw_segment(int xb, int yb, int xe, int ye) {

	int d = 0, dx = 0, dy = 0, tmp, yinc = 1, ince = 0, incne = 0;
	int* x; /*!< x's increment is always 1 ; it points to the horizontal
	 coordinate if the slope is between -1 and 1, otherwise it points to the vertical coordinate
	 */
	int* y; /*!< y's increment is always determined by the slope; it points to
	 the vertical coordinate if the slope is between -1 and 1, otherwise it points
	 to the horizontal coordinate
	 */
	int xend, xinc = 1;

	/* SETUP PART: data preparation */
	if (xb == xe) {
		/* special case for vertical slope */
		x = &yb; /* the "true" variable to increment is yb*/
		y = &xb; /* xb must not be incremented */
		yinc = 0; /* the increment on "virtual" y must be 0*/
		if (yb > ye) {
			/* swap ys to add positive increment */
			tmp = yb;
			yb = ye;
			ye = tmp;
		}
		/* final point*/
		xend = ye;
	} else {
		if (xb > xe) {
			/* swap values to obtain canonical form and increment rightward  */
			tmp = xb;
			xb = xe;
			xe = tmp;
			tmp = yb;
			yb = ye;
			ye = tmp;
		}

		/* normal (= not infinite) slope */
		/* standard delta definitions */
		dx = xe - xb;
		dy = ye - yb;
		if (std::abs(dy) > std::abs(dx)) {
			/* if the slope is below -1 or above 1, swap deltas and coordinates */
			x = &yb;
			y = &xb;
			xend = ye;
			if (dy > 0) {
				tmp = dx;
				dx = dy;
				dy = tmp;
			} else {
				tmp = -dx;
				dx = -dy;
				dy = tmp;
				xinc = -1;
			}
		} else {
			/* default case, set values properly */
			xend = xe;
			x = &xb;
			y = &yb;
			if (dy < 0) {
				yinc = -1;
			}
		}
		/*
		 * now check the slope sign to be positive or negative: note that it depends
		 * only on dy, independently from the increment coordinate or the points order
		 * (that's why we have set up all the stuff before)
		 */

		incne = 2 * (xinc * dy - yinc * dx);
		d = 2 * xinc * dy - yinc * dx;
		ince = 2 * xinc * dy;
		/* initialize the decision variable */

	}

	/* DRAWING: and now, let's go with the cycle! ... */
	/* initial point */
	cg_draw_point_2d(xb, yb);
	while ((*x) * xinc < xend * xinc) {
		/*
		 * yinc * d for positive or negative slope: for positive slope (yinc = 1) increment if
		 * the midpoint is BELOW the line (d= 1*d = yinc*d > 0), for negative slope decrement when
		 * the midpoint id ABOVE the line (d < 0, i.e. if (-1)*d = yinc*d > 0)
		 */
		if (yinc * d <= 0) {
			d += ince;
		} else {
			d += incne;
			(*y) += yinc;
		}
		(*x) += xinc;
		/*
		 * note that we must ALWAYS draw points by coordinates xb, yb in THIS order;
		 * the previous pointers and increment handling is aimed to increment the proper
		 * variable inside this cycle
		 */
		cg_draw_point_2d(xb, yb);
	}
}

/*!
 * \fn cg_draw_circumference(const int xc, const int yc, const unsigned int rad,
 * 		const float beg_angle, const float end_angle)
 *
 * \brief draws a circumference on the screen
 *
 * It draws a circumference on the screen using the Bresenham algorithm.
 * In addition, this function can draw a portion of an entire circumference by specifying
 * the initial and the final angles. These angles are represented as floating point numbers
 * (float) and are supposed to be expressed in DEGREES, in CLOCKWISE direction, the angle
 * with value 0 being the vertical angle towards north (or equivalently 0 a.m.), and ranging
 * from 0 to 360.
 * The drawn circumference opens from the initial angle to the final angle.
 *
 * \param xc horizontal coordinate of the center
 * \param yc vertical coordinate of the center
 * \param rad radius
 * \param beg_angle initial angle, in clockwise direction from north
 * \param end_angle final angle, in clockwise direction from north
 */
void cg_draw_circumference(const int xc, const int yc, const unsigned int rad,
		const float beg_angle, const float end_angle) {
	/* b stands for beginning, d is the decision variable */
	int xb, yb, d;
	unsigned int octb = 0, octe = 7; /*!< initial and end quadrants */
	const int xinc[8] = { 1, -1, -1, -1, -1, 1, 1, 1 }; /*!< increments of x in each quadrant */
	const int yinc[8] = { -1, 1, -1, -1, 1, -1, 1, 1 }; /*!< increments of y in each quadrant */
	int xmax[8]; /*!< limits of the x coordinate for each quadrant */
	int* const x[8] = { &xb, &yb, &yb, &xb, &xb, &yb, &yb, &xb };
	int* const y[8] = { &yb, &xb, &xb, &yb, &yb, &xb, &xb, &yb };
	const int xcen[8] = { xc, yc, yc, xc, xc, yc, yc, xc };
	const int ycen[8] = { yc, xc, xc, yc, yc, xc, xc, yc };
	int inc_dir = 1;
	int half = cg_roundf(((float) rad) * ((float) (0.707106781)));

	/* SETUP PART: initialize data */
	/* initialize default limits for a full circumference */
	xmax[0] = xc + half;
	xmax[1] = yc;
	xmax[2] = yc - half;
	xmax[3] = xc;
	xmax[4] = xc - half;
	xmax[5] = yc;
	xmax[6] = yc + half;
	xmax[7] = xc;

	/* set the coordinates of the starting point and the quadrants limits */
	xb = xc;
	yb = yc;
	if (beg_angle > 0) {
		/* calculate initial coordinates by usual trigonometry formulas */
		xb += cg_roundf(((float) rad) * cosf(cg_deg_to_rad(((float) (90)) - beg_angle)));
		yb += cg_roundf(((float) rad) * sinf(cg_deg_to_rad(((float) (90)) - beg_angle)));
		octb = circle_octant(beg_angle);
	} else {
		yb += rad;
	}

	if (end_angle < 360) {
		/* calculate final coordinates by usual trigonometry formulas */
		octe = circle_octant(end_angle);
		if (x[octe] == &xb) {
			xmax[octe] = xc
					+ cg_roundf(
							((float) rad)
									* cosf(cg_deg_to_rad(((float) (90)) - end_angle)));
		} else {
			xmax[octe] = yc
					+ cg_roundf(
							((float) rad)
									* sinf(cg_deg_to_rad(((float) (90)) - end_angle)));
		}
	}

	if (octb % 2 == 1) {
		/* set the increment direction */
		inc_dir = -1;
	}

	/* DRAWING PART: let's go with the cycle... */

	cg_draw_point_2d(xb, yb);
	for (; octb <= octe; octb++) {
		/*
		 * For every quadrant, set the initial value of the decision variable.
		 * This value depends on the increment of the variables x and y
		 * (more precisely, on their sign)
		 */
		int incx = xinc[octb];
		d = 2 * incx * ((*(x[octb])) - xcen[octb])
				+ yinc[octb] * ((*(y[octb])) - ycen[octb]) + 1;

		/*
		 * Now let's draw all the points of the current quadrant.
		 * The product with incx is aimed to control the sign of the comparison:
		 * note that, where the increment of x (going in clockwise direction) is positive
		 * (1st and 4th quadrants) x must be smaller or equal to xmax[qbeg], while where
		 * the increment is negative (2nd and 3rd quadrants) x must be greater or equal.
		 */
		while (((*(x[octb])) * incx) <= (xmax[octb]) * incx) {
			if (inc_dir * d < 0) {
				/* east, i.e. don't increment */
				d += 2 * incx * ((*(x[octb])) - xcen[octb]) + 3;
			} else {
				/* south - east, i.e. increment */
				d += 2 * incx * ((*(x[octb])) - xcen[octb])
						+ 2 * yinc[octb] * ((*(y[octb])) - ycen[octb]) + 5;
				*(y[octb]) += yinc[octb];
			}
			*(x[octb]) += incx;
			cg_draw_point_2d(xb, yb);
		}
		/* reverse the increment direction, i.e. the increment case, for every octant */
		inc_dir = -inc_dir;
	}

}

}  // namespace CGLib
