/*!
 * \file
 * \author Alberto Scolari
 *
 * \brief This file contains the implementation of the filling functions
 * for basic entities (i.e. polygons made of vertices and circumferences, not for classes)
 *
 * Vertices and circumferences extrema are automatically linked together in order to close
 * the current figure; this behavior can be toggled by the call cg_set_auto_close().
 */

#include "cg_filling_primitives.hh"
#include "CG_Common/basic_entities.hh"
#include "CG_Common/CG_Debug/cg_debug.hh"
#include "CG_Common/basic_functions.hh"
#include "Basic/cg_gl_interface.hh"
using namespace CGLib;

#include <cmath>
#include <cstddef>
#include <climits>

namespace CGLib {

/*!
 * \struct aet_entry
 *
 * \brief struct to store an active edge table entry
 */
typedef struct aet_entry {
	int ymax; /*!< stores the maximum y coordinate of the line */
	int xcurrent; /*!< stores the current x coordinate of the line */
	int num; /*!< stores the numerator (i.e. the fractional part) of the current point */
	struct aet_entry *next; /*!< pointer to the next element of the list */

	aet_entry(int yma, int xc, int n) :
			ymax(yma), xcurrent(xc), num(n), next(NULL) {
	} /*!< constructor with default initialization of fields */

	bool operator ==(const struct aet_entry& el) const {
		return (xcurrent == el.xcurrent);
	}/*!< operator ==, which compares only the value of xcurrent */

	bool operator !=(const struct aet_entry& el) const {
		return (xcurrent != el.xcurrent);
	}/*!< operator !=, which compares only the value of xcurrent */

	bool operator <(const struct aet_entry& el) const {
		return (xcurrent < el.xcurrent);
	}/*!< operator <, which compares only the value of xcurrent */

	bool operator >(const struct aet_entry& el) const {
		return (xcurrent > el.xcurrent);
	}/*!< operator >, which compares only the value of xcurrent */

	bool operator >=(const struct aet_entry& el) const {
		return ((xcurrent > el.xcurrent) || (xcurrent == el.xcurrent));
	}/*!< operator >=, which compares only the value of xcurrent; it must be defined */

} aet_entry;

/*! \struct et_entry
 *
 * \brief struct to store an edge table entry
 */
typedef struct et_entry {
	int ymax; /*!< maximum y coordinate of the line */
	int xmin; /*!< minimum x coordinate of the line */
	int dx; /*!< difference between the x coordinates */
	int dy; /*!< difference between the y coordinates; dy should be always positive, while dx can have any sign */
	struct et_entry *next; /*!< pointer to the next element of the list */

	et_entry(int yma, int xmi, int deltax, int deltay) :
			ymax(yma), xmin(xmi), dx(deltax), dy(deltay), next(NULL) {
	} /*!< constructor with default initialization of fields */

	bool operator <(const struct et_entry& el) const {
		return (ymax < el.ymax);
	}/*!< operator <, which compares only the value of ymax */

	bool operator >(const struct et_entry& el) const {
		return (ymax > el.ymax);
	}/*!< operator >, which compares only the value of ymax */

	bool operator >=(const struct et_entry& el) const {
		return ((ymax > el.ymax) || (ymax == el.ymax));
	}/*!< operator >=, which compares only the value of ymax */

} et_entry;

/*!
 * \var win_height
 *
 * \brief external variable (from cg_drawing_primitives.cc) storing the height of the current window
 */
extern int cg_win_height;

/*!
 * \var static aet_entry **active_edge_table = NULL
 *
 * \brief active edge table
 */
static aet_entry **active_edge_table = NULL;

/*!
 * \var static et_entry **edge_table = NULL
 *
 * \brief edge table
 */
static et_entry **edge_table = NULL;

/*!
 * \var static int ymin_aet = INT_MAX
 *
 * \brief it stores the minimum y coordinate of all the points of the polygon
 */
static int ymin_aet = INT_MAX;

/*!
 * \var static int ymax_aet = INT_MIN
 *
 * \brief it stores the maximum y coordinate of all the points of the polygon
 */
static int ymax_aet = INT_MIN;

/*!
 * \fn static void update_yminmax(const int y)
 *
 * \brief checks wether y is bigger than ymax_aet or smaller than ymin_aet
 * and in case updates the values accordingly.
 *
 * \param y the actual scanline
 */
static void update_yminmax(const int y) {
	if (y > ymax_aet) {
		ymax_aet = y;
	} else if (y < ymin_aet) {
		ymin_aet = y;
	}
}

/*!
 * \var static int xfirst = 0
 *
 * \brief it stores the x coordinate of the first point of the polygon
 */
static int xfirst = INT_MIN;

/*!
 * \var static int yfirst = 0
 *
 * \brief it stores the y coordinate of the first point of the polygon
 */
static int yfirst = INT_MIN;

/*!
 * \var static int xlast = INT_MAX
 *
 * \brief it stores the x coordinate of the last point of the polygon
 */
static int xlast = INT_MIN;

/*!
 * \var static int ylast = INT_MAX
 *
 * \brief it stores the y coordinate of the last point of the polygon
 */
static int ylast = INT_MIN;

/*!
 * \fn static void reset_vertices_mem(void)
 *
 * \brief resets the memory of first and last vertices to the original
 * meaningless values
 */
static void reset_vertices_mem(void) {
	xlast = INT_MIN;
	ylast = INT_MIN;
	xfirst = INT_MIN;
	yfirst = INT_MIN;
}

/*!
 * \var static bool auto_close = true
 *
 * \brief tells whether the polygon must be closed automatically
 */
static bool auto_close = true;

/*!
 * \fn static void insert_point_into_table(const E* entry, const int sl, E** table_list)
 *
 * \brief template to insert the point pointed by entry into the scanline sl of the provided edge table
 * in the proper order
 *
 * This template requires the struct pointed by entry to have a field pointer called next and
 * a partial order < between elements
 *
 * \tparam E the struct type
 * \param entry the pointer of the entry to add
 * \param sl scanline to insert entriy inside
 * \param table_list the list of entries, implemented as a vector of pointers
 */
template<typename T> static void insert_point_into_table(T *entry, const int sl,
		T** table) {
	T *cur;
	cur = table[sl];
	if (cur == NULL) {
		table[sl] = entry;
		entry->next = NULL;
		return;
	} else if (*cur >= *entry) {
		entry->next = cur;
		table[sl] = entry;
	} else {
		while (cur->next != NULL) {
			if (*(cur->next) >= *entry) {
				break;
			} else {
				cur = cur->next;
			}
		}
		entry->next = cur->next;
		cur->next = entry;
	}
}

/*!
 * \fn static void insert_point_into_et(et_entry* const entry, const int sl)
 *
 * \brief inserts a point into the edge table according to its order (i.e. by ymax)
 *
 * \param entry the entry to be inserted
 * \param sl the scanline to insert the entry
 */
static void insert_point_into_et(et_entry* const entry, const int sl) {
	insert_point_into_table<et_entry>(entry, sl, edge_table);

}

/*!
 * \fn static void insert_point_into_aet(aet_entry* const entry, const int sl)
 *
 * \brief inserts a point into the active edge table according to its order (i.e. by xcurrent)
 *
 * \param entry the entry to be inserted
 * \param sl the scanline to insert the entry
 */
static void insert_point_into_aet(aet_entry* const entry, const int sl) {
	//DEBUG_COND3(sl==213, "insert ", entry->xcurrent," ymax ",entry->ymax)
	insert_point_into_table<aet_entry>(entry, sl, active_edge_table);
}

/*!
 * \fn static void add_segment_to_et(const int xold, const int yold, const int x, const int y)
 *
 * \brief adds an entry describing a segment into the edge table in the correct order (i.e. by ymi)
 *
 * \param xold x coordinate of the first point
 * \param yold y coordinate of the first point
 * \param x x coordinate of the last point
 * \param y y coordinate of the last point
 */
static void add_segment_to_et(const int xb, const int yb, const int xe, const int ye) {
	int ymax, ymin, dx, xmin;

	/* find minimum and maximum for y: dy must be GREATER OR EQUAL to 0 */
	if (ye > yb) {
		ymax = ye;
		ymin = yb;
		dx = xe - xb;
		xmin = xb;
	} else {
		ymax = yb;
		ymin = ye;
		dx = xb - xe;
		xmin = xe;
	}

	insert_point_into_et(new struct et_entry(ymax, xmin, dx, (ymax - ymin)), ymin);
}

/*!
 * \fn static void reset_data(void)
 *
 * \brief resets all the data of used by all the filling primitives,
 * including the edge tables, the global variables and the vertices.
 *
 * Allocated memory is properly freed.
 */
static void reset_data(void) {

	/* empty active edge table and delete it */
	if (active_edge_table != NULL) {
		for (int sl = ymin_aet; sl <= ymax_aet; sl++) {
			aet_entry *old, *cur = active_edge_table[sl];
			while (cur != NULL) {
				old = cur;
				cur = cur->next;
				delete old;
			}
		}
		delete active_edge_table;

	}
	/* empty edge table and delete it */
	if (edge_table != NULL) {
		for (int sl = ymin_aet; sl <= ymax_aet; sl++) {
			et_entry *old, *cur = edge_table[sl];
			while (cur != NULL) {
				old = cur;
				cur = cur->next;
				delete old;
			}
		}
		delete edge_table;

	}

	/* reset values */
	active_edge_table = NULL;
	edge_table = NULL;
	ymin_aet = INT_MAX;
	ymax_aet = INT_MIN;
	reset_vertices_mem();
	auto_close = true;
}

/*!
 * \fn static void add_segment_to_aet(const int ymaxs, const int xmins, const int dx, const int dy)
 *
 * \brief adds a segment to the active edge table by adding for every scanline the proper point
 *
 * The segment starts from (xmins, ymaxs - dy) to (xmins + dx, ymaxs).
 *
 * dy IS EXPECTED TO BE NON - NEGATIVE !
 *
 * in case it is negative the algorithm doesn't work
 *
 *
 * \param ymax maximum y coordinate of the line
 * \param x_ymin x coordinate of the vertex having minimum y coordinate
 * \param dx difference between x coordinates
 * \param dy difference between x coordinates: MUST BE >= 0
 */
static void add_segment_to_aet(const int ymax, const int x_ymin, int const dx,
		int const dy) {
	int x_inc_fixed = 0, inc_sign = 1, inc = 0, x, ymin, inct = 0;
	DEBUG_COND(dy < 0, "dy negative: reverse deltas!")
	ymin = ymax - dy;
	if (dy == 0) {
		/*
		 * horizontal line: add vertices on the same line and done!
		 */
		int xmax, xmin;
		if (dx >= 0) {
			xmax = x_ymin + dx;
			xmin = x_ymin;
		} else {
			xmax = x_ymin;
			xmin = x_ymin + dx;
		}
		insert_point_into_aet(new struct aet_entry(ymax, xmax, 0), ymin);
		insert_point_into_aet(new struct aet_entry(ymax, xmin, 0), ymin);
		return;
	}

	if (dx != 0) {
		/*
		 * finite slope: x must be incremented by 0 or more for every scanline
		 */
		if (dx < 0) {
			/* to control the direction of x growth and the comparisons */
			inc_sign = -1;
		}
		/*
		 * increment x_inc_fixed as many times as dy "is in" abs(dx) (xincf = abs(dx) mod dy):
		 * thus xincf represent the module of the fixed increment of x for each scanline;
		 * then the sign is adjusted by multiplying by incsign
		 */
		/*
		 * REMEMBER!!!!!
		 *
		 * 					dx		numerator
		 * aet slope = 		__	=	___________
		 *
		 * 					dy		denominator
		 *
		 */
		for (inc = dy; inc < dx * inc_sign; inc += dy) {
			x_inc_fixed++;
		}

		x_inc_fixed *= inc_sign;
		// the true increment per cycle
		inct = dx - dy * x_inc_fixed;
		/* reset inc before the main cycle*/
		inc = 0;
	} else {
		/*
		 * vertical slope: no increment along x
		 */
		inc_sign = 0;
	}

	/*
	 * main cycle: let's go!
	 */
	x = x_ymin;
	for (int sl = ymin; sl <= ymax; sl++) {
		/*
		 * insert the point into scanline: note that the increment (the numerator)
		 * is inc*incsign to take in account the sign of the slope, which is stored
		 * in incsign (being it only -1 or +1)
		 */
		insert_point_into_aet(new struct aet_entry(ymax, x, inc * inc_sign), sl);
		/* add the denominator to the increment */
		inc += inct;
		/*
		 * note that the "overflow" is checked by calculating the module of inc, i.e. inc * incsign
		 */
		if (inc * inc_sign >= dy) {
			/*
			 * if the numerator inc "overflows" the denominator dy (in module) then "restart"
			 * by subtracting dy  from the module of inc and by incrementing
			 * x of incsign, i.e. into the right direction of growth of the line.
			 */
			x += inc_sign;
			inc -= (inc_sign * dy);
		}
		/* always add the fixed increment */
		x += x_inc_fixed;
	}
}

/*!
 * \fn static inline void fill_scanline(const int sl, const int xb, const int xe)
 *
 * \brief fills the scanline sl from xb to xe included
 *
 * \param sl number of scanline to fill
 * \param xb initial coordinate to fill from
 * \param xe final coordinate to fill
 */
static inline void fill_scanline(const int sl, const int xb, const int xe) {
	for (int x = xb; x <= xe; x++) {
		//DEBUG1("x: ", x);
		cg_draw_point_2d(x, sl);
	}
}

/*!
 * \def invert(val)
 *
 * \brief inverts the boolean value of val and store it
 */
#define invert(val) ((val) = !(val))

/*!
 * \def is_fractional(ptr)
 *
 * \brief states whether ptr points to a a fractional point inside the scanline
 */
#define is_fractional(ptr) ((ptr)->num!=0)

/*!
 * \fn static inline int round_up(aet_entry** const en)
 *
 * \brief rounds up the x coordinate of the point pointed by en
 * according to num: if num > 0 rounds up, otherwise return xcurrent
 *
 * \param en (double) pointer to the actual scanline point
 * \return (*en)->xcurrent properly rounded up
 */
static inline int round_up(aet_entry* const en) {
	if ((en)->num > 0) {
		return (en)->xcurrent + 1;
	} else {
		return (en)->xcurrent;
	}
}

/*!
 * \fn static inline int round_down(aet_entry** const en)
 *
 * \brief rounds down the x coordinate of the point pointed by en
 * according to num: if num < 0 rounds down, otherwise return xcurrent
 *
 * \param en (double) pointer to the actual scanline point
 * \return (*en)->xcurrent properly rounded down
 */
static inline int round_down(aet_entry* const en) {
	if ((en)->num < 0) {
		return (en)->xcurrent - 1;
	} else {
		return (en)->xcurrent;
	}
}

/*!
 * \fn static inline void fill_polygon(void)
 *
 * \brief fills the polygon by iterating over the scanlines and analyzing each vertex
 * according to polygon filling rules.
 */
static inline void fill_polygon(void) {
	int sl, xb = 0, xe = 0;
	aet_entry* cur;
	bool inside = false;
	bool draw = false;

	for (sl = ymin_aet; sl <= ymax_aet; sl++) {
		cur = active_edge_table[sl];
		while (cur != NULL) {

			if (is_fractional(cur)) {
				/*
				 * rule A: fractional point
				 */
				if (!inside) {
					/*
					 * case 1: from outside to inside
					 */
					xb = round_up(cur);
				} else {
					/*
					 * case 2: from inside to outside
					 */
					xe = round_down(cur);
					draw = true;
				}
				invert(inside);
			} else if (cur->ymax != sl) {
				/*
				 * rules B and C: integer coordinates
				 *
				 * note that, as of rule C case 2 (ymax point), if the point is
				 * ymax it must be ignored (no parity flip)
				 */
				if (!inside) {
					/*
					 * rule B, case 1: from outside to inside
					 * rule C, case 1: non ymax point, from outside to insides
					 */
					xb = cur->xcurrent;
				} else {
					if (cur->xcurrent == xb) {
						/*
						 * rule C, case 2: non ymax, flip parity
						 */
						xe = cur->xcurrent;
					} else {
						/*
						 * rule B, case 2: xcurrent - 1 is interior
						 */
						xe = cur->xcurrent - 1;
					}
					draw = true;
				}
				invert(inside);
			}
			cur = cur->next;
			if (draw) {
				/*
				 * fill the scanline ONLY if we are inside the polygon:
				 * in the special case cur != NULL and we are outside here,
				 * this means that this point (xb,sl) is a shared max-max vertex,
				 * and thus must NOT be drawn.
				 */
				fill_scanline(sl, xb, xe);
				/* invert "insideness", because after filling we are out of the polygon */
				inside = false;
				draw = false;
			}
		}
		/* if something went wrong, reset for the next scanline */
		inside = false;
		draw = false;
	}
}

/*!
 * \fn static void insert_point_circle_into_aet(int const sl, int const x, int const d)
 *
 * \brief inserts the point of a cirumference into the active edge table,
 * and updates the minimum and maximum written y coordinates
 * (i.e. ymin_aet and ymin_aet)
 *
 * \param sl the scanline to insert the new entry
 * \param x the x coordinate of the new point
 * \param d the decision variable
 */
inline static void insert_point_circle_into_aet(int const sl, int const x, int const d) {
	update_yminmax(sl);
	insert_point_into_aet(new struct aet_entry(sl + 1, x, d), sl);
}

/*!
 * \fn static void check_closed_polygon(void)
 *
 * \brief check whether the polygon has been closed, i.e. if the last vertex
 * has been linked to the first one
 */
static void check_closed_polygon(void) {
	if (((xlast != xfirst) || (ylast != yfirst)) && auto_close) {
		add_segment_to_et(xlast, ylast, xfirst, yfirst);
	}
}

/*! \fn void cg_new_polygon(void)
 *
 * \brief function TO BE CALLED before drawing a new polygon
 *
 * It initializes all the data structure needed to draw and fill a new polygon
 *
 */
void cg_polygon_new_fill(void) {
	/* never rely on the user to have drawn the previous polygon */
	if (active_edge_table != NULL || edge_table != NULL) {
		reset_data();
	}

	/* initialize aet */
	active_edge_table = new aet_entry*[cg_win_height];
	for (int i = 0; i < cg_win_height; i++) {
		active_edge_table[i] = NULL;
	}

	/* initialize et */
	edge_table = new et_entry*[cg_win_height];
	for (int i = 0; i < cg_win_height; i++) {
		edge_table[i] = NULL;
	}
}

/*!
 * \fn void cg_polygon_add_vertex(int x, int y)
 *
 * \brief adds a vertex to the current polygon; the current vertex is automatically linked
 * to the previous one (if any) to form an edge
 *
 * The input values are checked to be greater or equal to 0; if they are not, the function
 * returns without adding them. The same case applies if the added vertex is equal to the last
 * added one. This behavior is particularly useful in case of a circle to be filled: if the user
 * has already added the initial point of the circle as a vertex, it is not added again, which
 * would cause the parity to be wrong and the vertex not to be drawn (if it should). The same
 * applies the end point of the circle: if the user adds it again as a new vertex, it is ignored.
 *
 * \param x x coordinate of the new vertex
 * \param y y coordinate of the new vertex
 *
 */
void cg_polygon_add_vertex(int x, int y) {
	/* input check: if negative or already added, refuse */
	if (x < 0 || y < 0 || (x == xlast && y == ylast)) {
		return;
	}
	if (xlast > INT_MIN) {
		/* if not at the beginning, add new segment to edge table */
		add_segment_to_et(xlast, ylast, x, y);
	} else {
		/* if at the beginning, set first points and do not add a new segment */
		xfirst = x;
		yfirst = y;
	}
	/* save last inserted point */
	xlast = x;
	ylast = y;
	/* then remember min and max y to access the edge table more quickly */
	update_yminmax(y);
}

/*!
 * \fn void cg_close_polygon(void)
 *
 * \brief closes the current polygon by linking the last vertex with the first one
 * and resetting the saved first and last vertices to meaningless values.
 */
void cg_close_polygon(void) {
	check_closed_polygon();
	reset_vertices_mem();
}

/*!
 * \fn void cg_polygon_add_circle(const int xc, const int yc, const unsigned int rad,
		const float beg_angle, const float end_angle)
 *
 * \brief adds a circle to the current polygon; the circle is immediately rendered
 * into the active edge table, and the beginning point is automatically linked to
 * the previous vertex and final point to the next added vertex
 *
 * The current implementation is based on the same algorithm of the drawing primitive,
 * the decision variable d being the one which reveals whether the added point is internal
 * or external to the circle. Differently from the line algorithm, d does not give the exact
 * proportion of the distance between the real value of the point in a R^2 world and its approximation,
 * (like the line numerator compared to the denominator) but only its sign is important to state whether
 * we are in or out of the circle.
 *
 * \param xc x coordinate of the center
 * \param yc y coordinate of the center
 * \param rad radius
 * \param beg_angle initial angle, in clockwise direction, in degrees and from north
 * \param end_angle final angle, in clockwise direction, in degrees and from north
 */
void cg_polygon_add_circle(const int xc, const int yc, const unsigned int rad,
		const float beg_angle, const float end_angle) {

	int xb, yb, yf;
	unsigned int quadb = 0, quade = 3; /*!< initial and end quadrants */
	const int xinc[4] = { 1, -1, -1, 1 }; /*!< increments of x in each quadrant */
	const int yinc[4] = { -1, -1, 1, 1 }; /*!< increments of y in each quadrant */
	const int comp[4] = { 1, -1, 1, -1 };
	int ymax[4]; /*!< limits of the x coordinate for each quadrant */
	int d = 0;
	int old_d = 0;
	int yin = 0;
	int xin = 0;
	int deltax = 0;

	/* SETUP PART: initialize data */
	/* initialize default limits for a full circumference */
	ymax[0] = yc;
	ymax[1] = yc - rad;
	ymax[2] = yc;
	ymax[3] = yc + rad;

	/* set the coordinates of the starting point and the quadrants limits */
	xb = xc;
	yb = yc;
	if (beg_angle > 0) {
		/* calculate initial coordinates by usual trigonometry formulas */
		float inc = ((float) rad) * cosf(cg_deg_to_rad(((float) (90)) - beg_angle));
		if (beg_angle < 180) {
			xb += int(floorf(inc));
		} else {
			xb += int(ceilf(inc));
		}
		yb += int(floorf(((float) rad) * sinf(cg_deg_to_rad(((float) (90)) - beg_angle))));
		quadb = circle_quad(beg_angle);
	} else {
		yb += rad;
	}

	/* add initial point as a vertex to complete the polygon */
	if (auto_close) {
		cg_polygon_add_vertex(xb, yb);
	}

	if (end_angle < 360) {
		/* calculate final coordinates by usual trigonometry formulas */
		quade = circle_quad(end_angle);
		float inc = ((float) rad) * sinf(cg_deg_to_rad((float) (90) - end_angle));
		if (end_angle < 180) {
			yf = yc + int(floorf(inc));
		} else {
			yf = yc + int(ceilf(inc));
		}
		ymax[quade] = yf;
	} else {
		yf = yb;
	}

	/* DRAWING PART: let's go with the cycle... */
	insert_point_circle_into_aet(yb, xb, d);
	//init d
	d = (xb - xc) * (xb - xc) + (yb - yc) * (yb - yc) - rad * rad;
	for (; quadb <= quade; quadb++) {
		yin = yinc[quadb];
		xin = xinc[quadb];
		if (quadb % 2 == 0) {
			insert_point_circle_into_aet(yb, xb, d);
		}
		do {
			yb += yin;
			d += 2 * (yb - yc) * yin - 1;
			deltax = 0;
			while (d * comp[quadb] < 0) {
				old_d = d;
				d += 2 * (xb - xc + deltax) * xin + 1;
				deltax += xin;
			}
			//d = old_d;
			if (d == 0 || old_d > 0) {
				xb += (deltax);
			} else {
				xb += (deltax - xin);
				d = old_d;
			}
			insert_point_circle_into_aet(yb, xb, d);
		} while (yb * yin < ymax[quadb] * yin);
	}
	//insert_point_circle_into_aet(yb, xb, d);
	/*
	 * store final point as last vertex, so that next vertex will be linked to this one
	 * (and the polygon will be closed)
	 */
	if (auto_close) {
		xlast = xb;
		ylast = yb;
	} else {
		reset_vertices_mem();
	}
}

/*!
 * \fn void cg_polygon_draw(void)
 *
 * \brief draws the polygon whose vertices have been sent by the user with the set color
 * and resets all the data
 *
 * If the last added vertex has not the same coordinates of the first one,
 * the two vertices are automatically connected in order to close the polygon.
 * After drawing, the allocated data are deleted and the auto closure is set
 * tu true.
 */
void cg_polygon_draw(void) {

	if (active_edge_table == NULL || edge_table == NULL) {
		reset_data();
		return;
	}

	/* first of all, check that the polygon has been closed by user; in case it has not, close it */
	check_closed_polygon();

	/* build active edge table */
	int line = 0;
	for (int sl = ymin_aet; sl <= ymax_aet; sl++) {
		for (et_entry* cur = edge_table[sl]; cur != NULL; cur = cur->next) {
			//DEBUG1("adding line ", line)
			//DEBUG3("ymax: ",cur->ymax," xmin: ", cur->xmin)
			//DEBUG3("dx: ",cur->dx," dy: ", cur->dy)
			line++;
			add_segment_to_aet(cur->ymax, cur->xmin, cur->dx, cur->dy);
		}
	}

	fill_polygon();
	/* reset everything */
	reset_data();
	//DEBUG_EXIT("exit!")
}

/*!
 * \fn void cg_set_auto_close(bool set)
 *
 * \brief sets/unset the automatic closure of the polygon
 *
 * \param set true if the polygon must be closed automatically, false otherwise
 */
void cg_set_auto_close(bool set) {
	auto_close = set;
}

}
