/*!
 * \file
 *
 * \brief primitives for drawing given simple coordinates
 *
 * \author Alberto Scolari
 */

#ifndef CG_ENGINE_HH_
#define CG_ENGINE_HH_
#include "CG_Common/basic_entities.hh"
using namespace CGLib;

namespace CGLib {

void cg_init_window(int width, int height);

/*! \fn void cg_draw_segment(int xb, int yb, int xe, int ye)
 *
 * \brief draws a segment given the end points coordinates
 *
 * It draws a straight segment from (xb,yb) to (xe,ye).
 * The implementation is made accordingly to the Pittway - Van Aken algorithm.
 * This implementation tries to optimize the cycle execution by limiting branches
 * inside the main cycle and setting up the variables before, according to the slope value.
 * The sub-cases are:
 *
 * 	1) vertical line: here just the y coordinate is incremented
 *
 * 	2) slope not infinite: sub-cases depending on abs(slope);
 *
 * 		a) abs(slope) > 1: the x and y coordinates are inverted, and the referred
 * 			variables are swapped accordingly;
 *
 * 		b) abs(slope) <= 1: this is the default case, where no variable pair is swapped;
 *
 * See the code comments for more details.
 *
 *
 * \param xb	horizontal coordinate of initial point
 * \param yb	vertical coordinate of initial point
 * \param xe	horizontal coordinate of final point
 * \param ye	vertical coordinate of final point
 */
void cg_draw_segment(int xb, int yb, int xe, int ye);

/*!
 * \fn cg_draw_circumference(const int xc, const int yc, const unsigned int rad,
 * 		const unsigned float beg_angle, const unsigned float end_angle)
 *
 * \brief draws a circumference on the screen
 *
 * It draws a circumference on the screen using the Bresenham algorithm.
 * In addition, this function can draw a portion of an entire circumference by specifying
 * the initial and the final angles. These angles are represented as floating point numbers
 * (float) and are supposed to be expressed in DEGREES, in CLOCKWISE direction, the angle
 * with value 0 being the vertical angle towards north (or equivalently 0 a.m.), and ranging
 * from 0 to 360.
 * The drawn circumference opens from the initial angle to the final angle.
 *
 * \param xc horizontal coordinate of the center
 * \param yc vertical coordinate of the center
 * \param rad radius
 * \param beg_angle initial angle, in clockwise direction from north
 * \param end_angle final angle, in clockwise direction from north
 */
void cg_draw_circumference(const int xc, const int yc, const unsigned int rad,
		const float beg_angle, const float end_angle);

}

#endif /* CG_ENGINE_HH_ */
