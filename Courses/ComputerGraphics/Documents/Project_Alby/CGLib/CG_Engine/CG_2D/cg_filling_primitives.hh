/*!
 * \file
 *
 * \brief primitives for filling given simple coordinates
 *
 * \author Alberto Scolari
 */

#ifndef CG_FILLING_PRIMITIVES_HH_
#define CG_FILLING_PRIMITIVES_HH_

namespace CGLib {

/*! \fn void cg_new_polygon(void)
 *
 * \brief function TO BE CALLED before drawing a new polygon
 *
 * It initializes all the data structure needed to draw and fill a new polygon
 *
 */
void cg_polygon_new_fill(void);

/*!
 * \fn void cg_polygon_add_vertex(int x, int y)
 *
 * \brief adds a vertex to the current polygon; the current vertex is automatically linked
 * to the previous one (if any) to form an edge
 *
 * The input values are checked to be greater or equal to 0; if they are not, the function
 * returns without adding them. The same case applies if the added vertex is equal to the last
 * added one. This behavior is particularly useful in case of a circle to be filled: if the user
 * has already added the initial point of the circle as a vertex, it is not added again, which
 * would cause the parity to be wrong and the vertex not to be drawn (if it should). The same
 * applies the end point of the circle: if the user adds it again as a new vertex, it is ignored.
 *
 * \param x x coordinate of the new vertex
 * \param y y coordinate of the new vertex
 */
void cg_polygon_add_vertex(const int x, const int y);

/*!
 * \fn void cg_polygon_add_circle(int xc, int yc, int rad)
 *
 * \brief adds a circle to the current polygon; the circle is immediately rendered
 * into the active edge table, and the beginning point is automatically linked to
 * the previous vertex and final point to the next added vertex
 *
 * The current implementation is based on the same algorithm of the drawing primitive,
 * the decision variable d being the one which reveals whether the added point is internal
 * or external to the circle. Differently from the line algorithm, d does not give the exact
 * proportion of the distance between the real value of the point in a R^2 world and its approximation,
 * (like the line numerator compared to the denominator) but only its sign is important to state whether
 * we are in or out of the circle.
 *
 * \param xc x coordinate of the center
 * \param yc y coordinate of the center
 * \param rad radius
 * \param beg_angle initial angle, in clockwise direction, in degrees and from north
 * \param end_angle final angle, in clockwise direction, in degrees and from north
 */
void cg_polygon_add_circle(const int xc, const int yc, const unsigned int rad,
		const float beg_angle, const float end_angle);

/*!
 * \fn void cg_polygon_draw(void)
 *
 * \brief draws the polygon whose vertices have been sent by the user with the set color
 * and resets all the data
 *
 * If the last added vertex has not the same coordinates of the first one,
 * the two vertices are automatically connected in order to close the polygon.
 * After drawing, the allocated data are deleted and the auto closure is set
 * tu true.
 */
void cg_polygon_draw(void);

/*!
 * \fn void cg_close_polygon(void)
 *
 * \brief closes the current polygon by linking the last vertex with the first one
 * and resetting the saved first and last vertices to meaningless values.
 */
void cg_close_polygon(void);

/*!
 * \fn void cg_set_auto_close(bool set)
 *
 * \brief sets/unset the automatic closure of the polygon
 *
 * \param set true if the polygon must be closed automatically, false otherwise
 */
void cg_set_auto_close(bool set);

}


#endif /* CG_FILLING_PRIMITIVES_HH_ */
