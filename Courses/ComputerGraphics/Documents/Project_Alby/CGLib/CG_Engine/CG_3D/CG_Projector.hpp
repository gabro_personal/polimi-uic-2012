/*!
 * \file
 *
 * \brief definition of CG_Projector
 *
 * \author Alberto Scolari
 */

#include <list>

#ifndef CG_PERSPECTIVE_HPP_
#define CG_PERSPECTIVE_HPP_
#include "CG_Common/CG_Matrices/CG_Vector.hpp"
using namespace CGLib;

namespace CGLib {

/*
 * abbreviations:
 *
 * VRP: view reference point
 *
 * VPN: viewing plane normal
 *
 * VUP: viewing up vector
 *
 * CW: center of the (viewing) window
 *
 */

/*!
 * \brief generic interface for a projector, defining the methods
 * to set all the parameters of a projection
 */
class CG_Projector {
public:

	/*!
	 * \brief object constructor which initializes the data and the transformation matrices
	 * stored inside the object
	 *
	 * The coordinates of the viewport are by default set to 0,800 for each dimension;
	 * to change them use the method set_viewport().
	 *
	 * No parameter checking is performed, so PAY ATTENTION!!!
	 *
	 * \param p_vrp
	 * \param p_vpn
	 * \param p_vup
	 * \param p_umax
	 * \param p_umin
	 * \param p_vmax
	 * \param p_vmin
	 * \param p_prp
	 * \param p_front_plane
	 * \param p_back_plane
	 * \param xo
	 * \param yo
	 * \param count
	 */
	CG_Projector(CG_Cartesian_f *p_vrp, CG_Cartesian_f* p_vpn, CG_Cartesian_f* p_vup, float p_umax,
			float p_umin, float p_vmax, float p_vmin, CG_Cartesian_f* p_prp, float p_front_plane,
			float p_back_plane, int count);

	/*!
	 * \brief object destructor, which deletes all the internal objects
	 * but NONE of the passed ones
	 */
	virtual ~CG_Projector();

	std::list<CG_HomPoint_f*>* transform_polygon(std::list<CG_HomPoint_f*>& vertices);

	std::list<CG_HomPoint_f*>* transform_vertices(std::list<CG_HomPoint_f*>& vertices);

	/*!
	 * \brief sets the VRP, i.e. the viewing reference point (in WC, world coordinates).
	 *
	 * This methods, as the other setters, sets the current VRP and
	 * initializes the internal transformation matrices which depend
	 * on the VRP.
	 * The implementation of these methods is left to implementing classes,
	 * because for each type of projection the VRP (or any other set parameter)
	 * affects different transformations.
	 * Note that all these transformations must be stored and accumulated
	 * into the main transformation matrix in the proper order, because the matrix
	 * product is NOT a commutative operator (it is only associative).
	 *
	 * \param  p_vrp
	 */
	virtual void set_vrp(CG_Cartesian_f* p_vrp) =0;

	/*!
	 * \brief sets the VPN, the viewing plane normal (in WC)
	 *
	 * \param  p_vpn
	 */
	virtual void set_vpn(CG_Cartesian_f* p_vpn) =0;

	/*!
	 * \brief sets the VUP, viewing up vector (in WC)
	 *
	 * \param  p_vup
	 */
	virtual void set_vup(CG_Cartesian_f* p_vup) =0;

	/*!
	 * \brief sets the PRP, projection reference point (in VRC,
	 * viewing reference coordinates)
	 *
	 * \param  p_prp
	 */
	virtual void set_prp(CG_Cartesian_f* p_prp) =0;

	/*!
	 * \brief sets the viewing window coordinates (expressed in VRC)
	 *
	 * \param  p_umax
	 * \param  p_umin
	 * \param  p_vmax
	 * \param  p_vmin
	 */
	virtual void set_window(float p_umax, float p_umin, float p_vmax, float p_vmin) =0;

	/*!
	 * \brief set the cutting planes (along the n axis of the VRC system)
	 * \param  p_front_plane
	 * \param  p_back_plane
	 */
	virtual void set_planes(float p_front_plane, float p_back_plane) =0;

	/*!
	 * \brief set the viewport coordinates and reinitializes the viewport
	 * transformation matrix (stored inside)
	 *
	 * \param p_xmin
	 * \param p_xmax
	 * \param p_ymin
	 * \param p_ymax
	 * \param p_zmin
	 * \param p_zmax
	 */
	void set_viewport(float p_xmin, float p_xmax, float p_ymin, float p_ymax, float p_zmin,
			float p_zmax);

	/*!
	 * \brief sets the output buffers: the results of the projection will be stored there
	 *
	 * This (low-level) choice has been chosen to avoid continuous creation and destruction of
	 * objects when projecting many points, which can have a significant overhead (and is prone
	 * to memory leakages).
	 * If you want objects, you are free (and responsible) to create them.
	 *
	 * \param xo
	 * \param yo
	 * \param zo
	 */
	//void set_buffers(float *xo, float* yo, float* zo);
protected:
	/*!
	 * \brief method to be implemented by inheriting classes to initialize
	 * ALL the matrices of partial transformations.
	 *
	 * The matrices are multiplied beginning from the last one, i.e. from
	 * the matrix partial[partial_count-1] to the matrix partial[0]:
	 *
	 * partial[partial_count-1] * partial[partial_count-2] * ... * partial[1] * partial[0]
	 *
	 */
	virtual void init_partials(void) =0;	// constructor of ALL partial matrices

	/*!
	 * \brief deletes the old transformation matrix and initializes the new one
	 * from partial transformations
	 */
	void reinit_transformation(void);

	/*!
	 * \brief initializes the transformation matrix as product of partial
	 * transformation matrices
	 */
	void init_transformation(void);

	/*!
	 * \brief initializes the viewport matrix from the coordinates of the viewport
	 */
	void init_viewport();

	/*
	 * data from outside
	 */
	CG_Cartesian_f* vrp;
	CG_Cartesian_f* vpn;
	CG_Cartesian_f* vup;
	float umax;
	float umin;
	float vmax;
	float vmin;
	CG_Cartesian_f* prp;
	float front_plane;
	float back_plane;
	float xmax;
	float xmin;
	float ymax;
	float ymin;
	float zmax;
	float zmin;
	/*
	 float* x_out;
	 float* y_out;
	 float* z_out;
	 */
	CG_TransMatrix** partial; /*!< partial transformations matrices */
	const int partial_count; /*!< number of partial transformations matrices */
	CG_TransMatrix* transformation; /*!< main transformation matrix */

private:

	float compute_in_bound(int bound, CG_HomPoint_f& p);

	bool test_in_bound(int bound, CG_HomPoint_f& p);

	void compute_intersection(int bound, CG_HomPoint_f& s, CG_HomPoint_f& p,
			CG_HomPoint_f& target);

	std::list<CG_HomPoint_f*>* transform(std::list<CG_HomPoint_f*>& vertices, const bool add_points);

	//float prev[4];
	CG_TransMatrix* viewport;

};

} /* namespace CGLib */
#endif /* CG_PERSPECTIVE_HPP_ */
