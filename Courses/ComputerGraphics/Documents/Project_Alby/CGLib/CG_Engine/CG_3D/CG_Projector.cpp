/*
 * CG_Perspective.cpp
 *
 *  Created on: 17/ott/2012
 *      Author: alberto
 */

#include <cstddef>
#include <list>
#include <iterator>

#include "CG_Common/CG_Debug/cg_debug.hh"
#include "CG_Projector.hpp"
#include "CG_Common/CG_Matrices/CG_Vector.hpp"
#include "CG_Common/CG_Matrices/CG_Matrix.hpp"
#include "CG_Common/CG_Matrices/cg_transformations.hh"
#include "cg_3d_draw_primitives.hh"
#include "CG_Common/basic_functions.hh"
using namespace CGLib;

#define APPR ((float)(0.0))

namespace CGLib {

CG_Projector::CG_Projector(CG_Cartesian_f* p_vrp, CG_Cartesian_f* p_vpn, CG_Cartesian_f* p_vup,
		float p_umax, float p_umin, float p_vmax, float p_vmin, CG_Cartesian_f* p_prp,
		float p_front_plane, float p_back_plane, int count) :
		partial_count(count) {
	this->partial = new CG_TransMatrix*[this->partial_count];
	this->vpn = p_vpn;
	this->vup = p_vup;
	this->vrp = p_vrp;
	this->umax = p_umax;
	this->umin = p_umin;
	this->vmax = p_vmax;
	this->vmin = p_vmin;
	this->prp = p_prp;
	this->front_plane = p_front_plane;
	this->back_plane = p_back_plane;
	this->transformation = NULL;	// to prevent Eclipse from complaining...
	this->viewport = NULL;			// to prevent Eclipse from complaining...
// predefined limits
	this->xmax = 800;
	this->xmin = 0;
	this->ymax = 800;
	this->ymin = 0;
	this->zmax = 800;
	this->zmin = 0;
//	this->init_partials();
//	this->init_transformation();
//	this->init_viewport();
}

CG_Projector::~CG_Projector() {

	delete transformation;
	for (int i = 0; i < this->partial_count; i++) {
		delete this->partial[i];
	}

	delete[] this->partial;
	delete this->viewport;
}

void CG_Projector::init_transformation(void) {

	this->transformation = get_unit_matrix();

	for (int i = this->partial_count - 1; i >= 0; i--) {
		(*(this->transformation)) *= (*(this->partial[i]));
	}

	DEBUG_STM(println("");println("transformation");print_matrix_f(*transformation))
}

void CG_Projector::reinit_transformation(void) {
	delete transformation;
	this->init_transformation();
}

void CG_Projector::init_viewport() {
	CG_TransMatrix* tmp;

	this->viewport = get_translation_matrix((this->xmin), (this->ymin), (this->zmin));

	tmp = get_scale_matrix((this->xmax - this->xmin) / 2, (this->ymax - this->ymin) / 2,
			(this->zmax - this->zmin));
	*(this->viewport) *= (*(tmp));
	delete tmp;

	tmp = get_translation_matrix(1, 1, 0);
	*(this->viewport) *= (*(tmp));
	delete tmp;

	DEBUG_STM(println("\nviewport");print_matrix_f(*viewport))

}

void CG_Projector::set_viewport(float p_xmin, float p_xmax, float p_ymin, float p_ymax,
		float p_zmin, float p_zmax) {
	this->xmax = p_xmax;
	this->xmin = p_xmin;
	this->ymax = p_ymax;
	this->ymin = p_ymin;
	this->zmax = p_zmax;
	this->zmin = p_zmin;
	delete this->viewport;
	this->init_viewport();
}


float CG_Projector::compute_in_bound(int bound, CG_HomPoint_f& p) {

	float sign = 1, res = 0;

	if (w(p)< 0) {
		// change comparisons signs
		sign = -1;
	}

	switch (bound) {
		case 0:
			res = sign * (w(p)+ x(p));
			break;
			case 1:
			res= sign* (w(p)+ y(p));
			break;
			case 2:
			res= sign * (w(p)+ z(p));
			break;
			case 3:
			res= (sign * (w(p)- x(p)));
			break;
			case 4:
			res= (sign * (w(p)- y(p)));
			break;
			case 5:
			res= (sign * z(p));
			break;
		}

			// + APPR is to prevent errors due to float arithmetic precision: I clip a little less..
	return res +APPR;
}

bool CG_Projector::test_in_bound(int bound, CG_HomPoint_f& p) {
	if (bound < 5) {
		return this->compute_in_bound(bound, p) >= 0;
	} else {
		return this->compute_in_bound(bound, p) <= 0;
	}
}

void CG_Projector::compute_intersection(int bound, CG_HomPoint_f& prev, CG_HomPoint_f& p,
		CG_HomPoint_f& target) {

	float w_x_1 = 1, w_x_2 = 1, a;

	switch (bound) {
		case 0:
			w_x_1 = w(prev)+ x(prev);
			w_x_2 = w(p) + x(p);
			break;

			case 1:
			w_x_1 = w(prev) + y(prev);
			w_x_2 = w(p) + y(p);
			break;

			case 2:
			w_x_1 = w(prev) + z(prev);
			w_x_2 = w(p) + z(p);
			break;

			case 3:
			w_x_1 = w(prev)- x(prev);
			w_x_2 = w(p) - x(p);
			break;

			case 4:
			w_x_1 = w(prev) - y(prev);
			w_x_2 = w(p) - y(p);
			break;

			case 5:
			w_x_1 = z(prev);
			w_x_2 = z(p);
			break;

		}
	a = w_x_1 / (w_x_1 - w_x_2);
	x(target)= (1-a)*x(prev) + a* x(p);
	y(target)= (1-a)*y(prev) + a* y(p);
	z(target)= (1-a)*z(prev) + a* z(p);
	w(target)= (1-a)*w(prev) + a* w(p);
}

std::list<CG_HomPoint_f*>* CG_Projector::transform_polygon(std::list<CG_HomPoint_f*>& vertices) {
	return this->transform(vertices,true);
}

std::list<CG_HomPoint_f*>* CG_Projector::transform_vertices(std::list<CG_HomPoint_f*>& vertices) {
	return this->transform(vertices,false);
}

std::list<CG_HomPoint_f*>* CG_Projector::transform(std::list<CG_HomPoint_f*>& vertices, const bool add_points) {

	std::list<CG_HomPoint_f*> *reslist = new std::list<CG_HomPoint_f*>();
	std::list<CG_HomPoint_f*> *tmplist = new std::list<CG_HomPoint_f*>();
	CG_HomPoint_f *tmp, *prev, *intersection;
	bool prev_in;
	float p[4];

	std::list<CG_HomPoint_f*>::iterator it, end;

	/* *******************************************************
	 * 		1) PROJECT TO THE CANONICAL VIEWING VOLUME
	 * 		this is the normalization transformation which
	 * 		sizes the given point to the canonical viewing
	 * 		volume
	 ******************************************************* */

	/*
	 * transform all the given vertices and put the results
	 * in the results list
	 */
	for (it = vertices.begin(); it != vertices.end(); it++) {
		tmp = new CG_HomPoint_f(0, 0, 0);
		w(*tmp)=0;
//		DEBUG_STM(println("before");print_matrix_f(*(*it)))
		for (unsigned int i = 0; i < 4; i++) {
			for (unsigned int j = 0; j < 4; j++) {
				(tmp->vals[i][0]) += ((this->transformation->vals[i][j]) * ((*it)->vals[j][0]));
			}
		}
//		DEBUG_STM(println("after");print_matrix_f(*tmp))
		reslist->push_back(tmp);
	}

	/* *******************************************************
	 * 			2) CLIP IN HOMOGENEOUS COORDINATES
	 * clip all the results with Sutherland-Hodgman algorithm
	 ******************************************************* */

	for (int i = 0; i < 6 && !reslist->empty(); i++) {
		// last element as first previous element
//		DEBUG_STM(std::cout<<"STEP "<<i<<std::endl;)
		prev = reslist->back();
		prev_in = test_in_bound(i, *prev);

		for (it = reslist->begin(); it != reslist->end(); it++) {
			if (test_in_bound(i, *(*it))) { // cases 1 and 4

				if (prev_in) {
					/*
					 * case 1: both prev and p are inside the border
					 * add p to tmplist
					 */
//					DEBUG_STM(println("");println("prev ");print_matrix_f(*prev))
//					DEBUG_STM( println("");println("last (inserted) ");print_matrix_f(*(*it)))
					tmplist->push_back(*it);
				} else {
					/*
					 * case 4: compute intersection and insert both intersection
					 * and p into tmplist
					 */
//					DEBUG_STM(println("");println("prev ");print_matrix_f(*prev))
					if (this->compute_in_bound(i, *(*it)) != 0 && add_points) {
						intersection = new CG_HomPoint_f(0, 0, 0);
						this->compute_intersection(i, *prev, *(*it), *intersection);

//						DEBUG_STM(
//								println("");println("intersectin (inserted) ");print_matrix_f(*intersection))
						tmplist->push_back(intersection);
					}
//					DEBUG_STM( println("");println("last (inserted) ");print_matrix_f(*(*it)))
					tmplist->push_back(*it);
					// delete prev if not pointing to the last element
					if (prev != reslist->back()) {
//						DEBUG_STM( println("");println("prev (deleted) ");print_matrix_f(*prev))
						delete prev;
					}
				}
			} else { // cases 2 and 3
				if (prev_in) {
					/*
					 * case 2: compute intersection and add
					 */
//					DEBUG_STM(println("");println("prev ");print_matrix_f(*prev))
					if ((reslist->size() > 2) || ((*it) != reslist->back())) {
						if (this->compute_in_bound(i, *prev) != 0 && add_points) {
							intersection = new CG_HomPoint_f(0, 0, 0);
							this->compute_intersection(i, *prev, *(*it), *intersection);
//							DEBUG_STM(
//									println("");println("intersectin (inserted) ");print_matrix_f(*intersection))
							tmplist->push_back(intersection);
						}
//						DEBUG_STM(println("");println("last ");print_matrix_f(*(*it)))
					}
				} else {
					/*
					 * case 3: do nothing
					 */
					// delete prev
					if (prev != reslist->back()) {
//						DEBUG_STM( println("");println("prev (deleted) ");print_matrix_f(*prev))
						delete prev;

					}
				}

				// if at the end and p is out, delete p
				if (*it == reslist->back()) {
//					DEBUG_STM( println("");println("last (deleted) ");print_matrix_f(*(*it)))
					delete reslist->back();
				}

			}

			// new previous element
			if (*it != reslist->back()) {
				prev = *it;
				prev_in = test_in_bound(i, *prev);
			}
		}
		// swap lists and clear old points
//		DEBUG_STM(std::cout <<"SIZE IS: "<<tmplist->size()<<std::endl;println(""))
		reslist->swap(*tmplist);
		tmplist->clear();

	}

	delete tmplist;

	/* *******************************************************
	 * 				4) MAP INTO VIEWPORT
	 ******************************************************* */

	for (it = reslist->begin(); it != reslist->end(); it++) {
		// compute values
		for (unsigned int i = 0; i < 4; i++) { // rows of matrix
			p[i] = 0;
			for (unsigned int j = 0; j < 4; j++) { // columns of matrix
				p[i] += (this->viewport->vals[i][j]) * ((*it)->vals[j][0]);
			}
		}

		// store results
		for (unsigned int i = 0; i < 4; i++) {
			(*it)->vals[i][0] = p[i];
		}
//		DEBUG_STM(println("\nafter viewport mapping");print_matrix_f(*(*it)))
	}

	/* *******************************************************
	 * 	5) CONVERT BACK TO CARTESIAN COORDINATES AND OUTPUT
	 ******************************************************* */

	for (it = reslist->begin(); it != reslist->end(); it++) {
		for (unsigned int j = 0; j < 3; j++) { // row of vector
			(*it)->vals[j][0] /= w(*(*it));
		}
		w(*(*it)) = 1;
//		DEBUG_STM(println("\nafter cartesian conversion");print_matrix_f(*(*it)))
	}

	return reslist;

}

} /* namespace CGLib */
