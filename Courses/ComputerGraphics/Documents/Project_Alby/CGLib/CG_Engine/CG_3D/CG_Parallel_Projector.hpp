/*!
 * \file
 *
 * \brief definition of CG_Parallel_Projector
 *
 * \author Alberto Scolari
 */

#ifndef CGPARALLELPERSPECTIVE_HPP_
#define CGPARALLELPERSPECTIVE_HPP_
#include "CG_Projector.hpp"
using namespace CGLib;

namespace CGLib {

/*!
 * \brief class which implements the CG_Projector interface in order to
 * perform a parallel projection.
 */
class CG_Parallel_Projector: public CG_Projector {
public:
	CG_Parallel_Projector(CG_Cartesian_f* p_vrp, CG_Cartesian_f* p_vpn, CG_Cartesian_f* p_vup,
			float p_umax, float p_umin, float p_vmax, float p_vmin, CG_Cartesian_f* p_prp,
			float p_front_plane, float p_back_plane);
	virtual void set_vrp(CG_Cartesian_f* p_vrp);
	virtual void set_vpn(CG_Cartesian_f* p_vpn);
	virtual void set_vup(CG_Cartesian_f* p_vup);
	virtual void set_prp(CG_Cartesian_f* p_prp);
	virtual void set_window(float p_umax, float p_umin, float p_vmax, float p_vmin);
	virtual void set_planes(float p_front_plane, float p_back_plane);

protected:
	/*!
	 * \brief
	 */
	void init_partials(void);

private:

	/*!
	 * \brief translate the VRP to the origin
	 */
	void init_trans1(void);

	/*!
	 * \brief rotate VRC to make VPN (i.e. the n axis) coincident with the z axis
	 */
	void init_trans2(void);

	/*!
	 * \brief shear so that the diection of projection (CW - PRP) is parallel to n
	 */
	void init_trans3(void);

	/*!
	 * \brief to transform into the CVV, translate the  projection of the CW
	 * onto the front plane to the origin
	 */
	void init_trans4(void);

	/*!
	 * \brief scale to the size of the CVV (2-2-1)
	 */
	void init_trans5(void);

};

} /* namespace CGLib */
#endif /* CGPARALLELPERSPECTIVE_HPP_ */
