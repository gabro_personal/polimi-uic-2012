/*
 * cg_3d_draw_primitives.hh
 *
 *  Created on: 16/ott/2012
 *      Author: alberto
 */

#ifndef CG_3D_DRAW_PRIMITIVES_HH_
#define CG_3D_DRAW_PRIMITIVES_HH_

#include "CG_Common/CG_Matrices/CG_geometric_vectors.hpp"
using namespace CGLib;

namespace CGLib {

CG_TransMatrix* get_par_project_matrix(CG_Cartesian_f& vrp, CG_Cartesian_f& vpn,
		CG_Cartesian_f& vup, float umax, float umin, float vmax, float vmin,
		CG_Cartesian_f& prp, float front_plane, float back_plane);

CG_TransMatrix* get_per_project_matrix(CG_Cartesian_f& vrp, CG_Cartesian_f& vpn,
		CG_Cartesian_f& vup, float umax, int umin, float vmax, float vmin,
		CG_Cartesian_f& prp, float front_plane, float back_plane);

}

#endif /* CG_3D_DRAW_PRIMITIVES_HH_ */
