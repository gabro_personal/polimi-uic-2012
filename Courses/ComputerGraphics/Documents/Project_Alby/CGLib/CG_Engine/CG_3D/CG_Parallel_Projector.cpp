/*
 * CGParallelPerspective.cpp
 *
 *  Created on: 17/ott/2012
 *      Author: alberto
 */

#include "CG_Common/CG_Debug/cg_debug.hh"
#include "CG_Parallel_Projector.hpp"
#include "CG_Projector.hpp"
#include "CG_Common/CG_Matrices/cg_transformations.hh"
#include "CG_Common/CG_Matrices/CG_Matrix.hpp"
#include "CG_Common/CG_Matrices/CG_Vector.hpp"
using namespace CGLib;

namespace CGLib {

CG_Parallel_Projector::CG_Parallel_Projector(CG_Cartesian_f* p_vrp, CG_Cartesian_f* p_vpn,
		CG_Cartesian_f* p_vup, float p_umax, float p_umin, float p_vmax, float p_vmin,
		CG_Cartesian_f* p_prp, float p_front_plane, float p_back_plane) :
		CG_Projector(p_vrp, p_vpn, p_vup, p_umax, p_umin, p_vmax, p_vmin, p_prp, p_front_plane,
				p_back_plane, 5) {
	this->init_partials();
	this->init_transformation();
	this->init_viewport();
}

void CG_Parallel_Projector::init_trans1(void) {
	/*
	 * 1) translate VRP to the origin
	 */
	this->partial[0] = get_translation_matrix_opposite(*(this->vrp));
	DEBUG_STM(println("");println("partial 0");print_matrix_f(*partial[0]))
}

void CG_Parallel_Projector::init_trans2(void) {
	/*
	 * 2) rotate VRC to make vpn coincident with the z axis
	 */
	CG_Cartesian_f *rx, *ry;

	// calculate rotation vectors rx and ry (rz coincides with vpn)

	// rx is perpendicular to vup and vpn
	rx = (this->vup)->vector_prod_3d(*(this->vpn));

	// ry is perpendicular to vpn (=rz) and to rx

	ry = (this->vpn)->vector_prod_3d(*rx);

	this->partial[1] = get_rot_matrix_from_vectors(*rx, *ry, *(this->vpn));
	DEBUG_STM(println("");println("partial 1");print_matrix_f(*partial[1]))

	delete rx;
	delete ry;
}

void CG_Parallel_Projector::init_trans3(void) {
	/*
	 * 3) make the shear matrix, which is determined by the dop, in turn
	 * 		determined by the cw and the prp
	 *
	 * 		(the signs - inside the matrix construction function do not appear because
	 * 		also the sign of the denominator doz is switched)
	 */
	float dop_z_opposite = this->prp->get_el(Z_coord);

	this->partial[2] = get_shear_matrix(
			((umax + umin) / 2 - (this->prp->get_el(X_coord))) / dop_z_opposite,
			((vmax + vmin) / 2 - (this->prp->get_el(Y_coord))) / dop_z_opposite, Z_coord);
	DEBUG_STM(println("");println("partial 2");print_matrix_f(*partial[2]))
}

void CG_Parallel_Projector::init_trans4(void) {
	/*
	 * 4) translate s.t. the projection of the CW onto the front plane becomes
	 * 		the origin
	 */

	this->partial[3] = get_translation_matrix(-(umax + umin) / 2, -(vmax + vmin) / 2, -front_plane);
	DEBUG_STM(println("");println("partial 3");print_matrix_f(*partial[3]))
}

void CG_Parallel_Projector::init_trans5(void) {
	/*
	 * 5) scale to the size of the parallel CVV (the "cube" 2-2-1)
	 */

	this->partial[4] = get_scale_matrix(2 / (umax - umin), 2 / (vmax - vmin),
			2 / (front_plane - back_plane));
	DEBUG_STM(println("");println("partial 4");print_matrix_f(*partial[4]))
}

void CG_Parallel_Projector::init_partials(void) {
	init_trans1();
	init_trans2();
	init_trans3();
	init_trans4();
	init_trans5();
}

void CG_Parallel_Projector::set_vrp(CG_Cartesian_f* p_vrp) {
	this->vrp = p_vrp;
	delete this->partial[0];
	init_trans1();
	reinit_transformation();
}

void CG_Parallel_Projector::set_vpn(CG_Cartesian_f* p_vpn) {
	this->vpn = p_vpn;
	delete this->partial[1];
	init_trans2();
	reinit_transformation();

}

void CG_Parallel_Projector::set_vup(CG_Cartesian_f* p_vup) {
	this->vup = p_vup;
	delete this->partial[1];
	init_trans2();
	reinit_transformation();
}

void CG_Parallel_Projector::set_prp(CG_Cartesian_f* p_prp) {
	this->prp = p_prp;
	delete this->partial[2];
	init_trans3();
	reinit_transformation();
}

void CG_Parallel_Projector::set_window(float p_umax, float p_umin, float p_vmax, float p_vmin) {
	this->umax = p_umax;
	this->umin = p_umin;
	this->vmax = p_vmax;
	this->vmin = p_vmin;
	delete this->partial[2];
	delete this->partial[3];
	delete this->partial[4];
	init_trans3();
	init_trans4();
	init_trans5();
	reinit_transformation();
}

void CG_Parallel_Projector::set_planes(float p_front_plane, float p_back_plane) {
	this->front_plane = p_front_plane;
	this->back_plane = p_back_plane;
	delete this->partial[3];
	delete this->partial[4];
	init_trans4();
	init_trans5();
	reinit_transformation();
}

} /* namespace CGLib */
