/*
 * cg_3d_draw_primitives.cc
 *
 *  Created on: 16/ott/2012
 *      Author: alberto
 */

#include "CG_Common/CG_Matrices/cg_transformations.hh"
#include "CG_Common/CG_Matrices/CG_geometric_vectors.hpp"
using namespace CGLib;

namespace CGLib {

CG_TransMatrix* get_par_project_matrix(CG_Cartesian_f& vrp, CG_Cartesian_f& vpn,
		CG_Cartesian_f& vup, float umax, float umin, float vmax, float vmin,
		CG_Cartesian_f& prp, float front_plane, float back_plane) {

	CG_Cartesian_f *rx, *ry;
	float doz;
	CG_TransMatrix *trans_vrp, *rot_vrp, *shear_dop, *trans_front,
			*scale_cvv;

	/*
	 * 1) translate VRP to the origin
	 */
	trans_vrp = get_translation_matrix_opposite(vrp);

	/*
	 * 2) rotate VRC to make vpn coincident with the z axis
	 */
	// calculate rotation vectors rx and ry (rz coincides with vpn)
	// rx is perpendicular to vup and vpn
	rx = vup.vector_prod_3d(vpn);

	// ry is perpendicular to vpn (=rz) and to rx

	ry = vpn.vector_prod_3d(*rx);

	rot_vrp = get_rot_matrix_from_vectors(*rx, *ry, vpn);
	*trans_vrp *= *rot_vrp;
	delete rx;
	delete ry;
	delete rot_vrp;

	/*
	 * 3) make the shear matrix, which is determined by the dop, in turn
	 * 		determined by the cw and the prp
	 *
	 * 		(the signs - inside the matrix construction function do not appear because
	 * 		also the sign of the denominator doz is switched)
	 */
	doz = float(prp.get_el(Z_coord));

	shear_dop = get_shear_matrix(
			((umax + umin) / 2 - (prp.get_el(X_coord))) / doz,
			((vmax + vmin) / 2 - (prp.get_el(Y_coord))) / doz,
			Z_coord);

	*trans_vrp *= *shear_dop;
	delete shear_dop;

	/*
	 * 4) translate s.t. the projection of the CW onto the front plane becomes
	 * 		the origin
	 */

	trans_front = get_translation_matrix(-(umax + umin) / 2,
			-(vmax + vmin) / 2, -(front_plane));

	*trans_vrp *= *trans_front;
	delete trans_front;

	/*
	 * 5) scale to the size of the CVV
	 */

	scale_cvv = get_scale_matrix(2 / (umax - umin),
			2 / (vmax - vmin), 2 / (front_plane - back_plane));

	*trans_vrp *= *scale_cvv;
	delete scale_cvv;

	return trans_vrp;
}

}
