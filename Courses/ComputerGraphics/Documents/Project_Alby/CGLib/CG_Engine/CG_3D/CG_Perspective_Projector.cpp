/*
 * CG_Perspective_Projector.cpp
 *
 *  Created on: 19/ott/2012
 *      Author: alberto
 */

#include "CG_Common/CG_Debug/cg_debug.hh"
#include "CG_Common/CG_Matrices/CG_Vector.hpp"
#include "CG_Perspective_Projector.hpp"
#include "CG_Common/CG_Matrices/CG_Matrix.hpp"
#include "CG_Common/CG_Matrices/cg_transformations.hh"
#include "cg_3d_draw_primitives.hh"
#include "CG_Common/basic_functions.hh"
using namespace CGLib;

namespace CGLib {

CG_Perspective_Projector::CG_Perspective_Projector(CG_Cartesian_f* p_vrp, CG_Cartesian_f* p_vpn,
		CG_Cartesian_f* p_vup, float p_umax, float p_umin, float p_vmax, float p_vmin,
		CG_Cartesian_f* p_prp, float p_front_plane, float p_back_plane) :
		CG_Projector(p_vrp, p_vpn, p_vup, p_umax, p_umin, p_vmax, p_vmin, p_prp, p_front_plane,
				p_back_plane, 6) {
	this->init_partials();
	this->init_viewport();
	this->init_transformation();
}

void CG_Perspective_Projector::init_trans1(void) {
	/*
	 * 1) translate VRP to the origin
	 */
	this->partial[0] = get_translation_matrix_opposite(*(this->vrp));
	DEBUG_STM(println("");println("partial 0");print_matrix_f(*partial[0]))
}

void CG_Perspective_Projector::init_trans2(void) {
	CG_Cartesian_f *rx, *ry;
	/*
	 * 2) rotate VRC to make vpn coincident with the z axis
	 */
	// calculate rotation vectors rx and ry (rz coincides with vpn)
	// rx is perpendicular to vup and vpn
	rx = (this->vup)->vector_prod_3d(*(this->vpn));

	// ry is perpendicular to vpn (=rz) and to rx

	ry = (this->vpn)->vector_prod_3d(*rx);

	this->partial[1] = get_rot_matrix_from_vectors(*rx, *ry, *(this->vpn));
	DEBUG_STM(println("");println("partial 1");print_matrix_f(*partial[1]))

	delete rx;
	delete ry;
}

void CG_Perspective_Projector::init_trans3(void) {
	/*
	 * 3) translate s.t. the COP is at the origin
	 */

	this->partial[2] = get_translation_matrix_opposite(*(this->prp));
	DEBUG_STM(println("");println("partial 2");print_matrix_f(*partial[2]))
}

void CG_Perspective_Projector::init_trans4(void) {
	/*
	 * 4) shear s.t. the viewing volume is symmetric w.r.t. the z axis
	 */

	float dop_z_opposite = float(this->prp->get_el(Z_coord));

	this->partial[3] = get_shear_matrix(
			((umax + umin) / 2 - this->prp->get_el(X_coord)) / dop_z_opposite,
			((vmax + vmin) / 2 - this->prp->get_el(Y_coord)) / dop_z_opposite, Z_coord);

	DEBUG_STM(println("");println("partial 3");print_matrix_f(*partial[3]))
}

void CG_Perspective_Projector::init_trans5(void) {
	/*
	 * 5) scale to the size of the perspective VV (the truncated pyramid)
	 */

	CG_HomPoint_f* tmp1 = new CG_HomPoint_f(0,0,0);


	CG_TransMatrix* tmp2 = *(this->partial[3]) * *(this->partial[2]);

	CG_Vector<float>* vrp_p = *tmp2 * *tmp1;

	delete tmp1;

	delete tmp2;

	float vrp_z = (*(vrp_p))[Z_coord];

	delete vrp_p;

	float c1 = 2 * vrp_z / ((umax - umin) * (vrp_z + (this->back_plane)));

	float c2 = 2 * vrp_z / ((vmax - vmin) * (vrp_z + (this->back_plane)));

	float c3 = -1 / (vrp_z + (this->back_plane));

	float z_min = -(vrp_z + (this->front_plane)) / (vrp_z + (this->back_plane));

	this->partial[4] = get_scale_matrix(c1, c2, c3);

	DEBUG_STM(println("");println("partial 4");print_matrix_f(*partial[4]))

	// set the CVV "scale" matrix

	this->partial[5] = new CG_TransMatrix();

	this->partial[5]->set_el(0, 0, 1);

	this->partial[5]->set_el(1, 1, 1);

	this->partial[5]->set_el(2, 2, (1 / (1 + z_min)));

	this->partial[5]->set_el(2, 3, (-z_min / (1 + z_min)));

	this->partial[5]->set_el(3, 2, -1);

	DEBUG_STM(println("");println("partial 5");print_matrix_f(*partial[5]))

}

void CG_Perspective_Projector::init_partials(void) {
	init_trans1();
	init_trans2();
	init_trans3();
	init_trans4();
	init_trans5();
}

void CG_Perspective_Projector::set_vrp(CG_Cartesian_f* p_vrp) {
	this->vrp = p_vrp;
	delete this->partial[0];
	init_trans1();
	reinit_transformation();
}

void CG_Perspective_Projector::set_vpn(CG_Cartesian_f* p_vpn) {
	this->vpn = p_vpn;
	delete this->partial[1];
	init_trans2();
	reinit_transformation();

}

void CG_Perspective_Projector::set_vup(CG_Cartesian_f* p_vup) {
	this->vup = p_vup;
	delete this->partial[1];
	init_trans2();
	reinit_transformation();
}

void CG_Perspective_Projector::set_prp(CG_Cartesian_f* p_prp) {
	this->prp = p_prp;
	delete this->partial[2];
	delete this->partial[3];
	delete this->partial[4];
	delete this->partial[5];
	init_trans3();
	init_trans4();
	init_trans5();
	reinit_transformation();
}

void CG_Perspective_Projector::set_window(float p_umax, float p_umin, float p_vmax, float p_vmin) {
	this->umax = p_umax;
	this->umin = p_umin;
	this->vmax = p_vmax;
	this->vmin = p_vmin;
	delete this->partial[3];
	delete this->partial[4];
	delete this->partial[5];
	init_trans4();
	init_trans5();
	reinit_transformation();
}

void CG_Perspective_Projector::set_planes(float p_front_plane, float p_back_plane) {
	this->front_plane = p_front_plane;
	this->back_plane = p_back_plane;
	delete this->partial[4];
	delete this->partial[5];
	init_trans5();
	reinit_transformation();
}

} /* namespace CGLib */
