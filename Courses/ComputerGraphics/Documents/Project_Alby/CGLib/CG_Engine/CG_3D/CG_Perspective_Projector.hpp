/*!
 * \file
 *
 * \brief definition of CG_Perspective_Projector
 *
 * \author Alberto Scolari
 */

#ifndef CG_PERSPECTIVE_PROJECTOR_HPP_
#define CG_PERSPECTIVE_PROJECTOR_HPP_
#include "CG_Projector.hpp"
using namespace CGLib;

namespace CGLib {

/*!
 * \brief class which implements the CG_Projector "interface" to compute
 * the perspective projection of given vertices.
 */
class CG_Perspective_Projector: public CG_Projector {

public:
	CG_Perspective_Projector(CG_Cartesian_f* p_vrp, CG_Cartesian_f* p_vpn, CG_Cartesian_f* p_vup,
			float p_umax, float p_umin, float p_vmax, float p_vmin, CG_Cartesian_f* p_prp,
			float p_front_plane, float p_back_plane);
	virtual void set_vrp(CG_Cartesian_f* p_vrp);
	virtual void set_vpn(CG_Cartesian_f* p_vpn);
	virtual void set_vup(CG_Cartesian_f* p_vup);
	virtual void set_prp(CG_Cartesian_f* p_prp);
	virtual void set_window(float p_umax, float p_umin, float p_vmax, float p_vmin);
	virtual void set_planes(float p_front_plane, float p_back_plane);

protected:
	void init_partials(void);

private:
	/*!
	 * \brief translate VRP to the origin
	 */
	virtual void init_trans1(void);

	/*!
	 * \brief translate the VRC
	 */
	virtual void init_trans2(void);

	/*!
	 * \brief translate s.t. the center of projection (here the PRP)
	 * is at the center
	 */
	virtual void init_trans3(void);

	/*!
	 * \brief shear s.t. the vector CW - PRP lies on the n z axis
	 */
	virtual void init_trans4(void);

	/*!
	 * \brief scales to fit the space to the perspective viewing volume
	 * (i.e. the frustum) and from this to the CVV (the "cube" 2-2-1)
	 */
	virtual void init_trans5(void);

};

} /* namespace CGLib */
#endif /* CG_PERSPECTIVE_PROJECTOR_HPP_ */
