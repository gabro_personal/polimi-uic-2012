/*
 * File:   SampleMain.cpp
 * Author: Chihua Ma
 *
 * Date: Sep 1st, 2012
 *
 * Operating System: Windows 7 64-bit
 * Compiler: Visual Studio 2010
 */

// Without include this header in Windows, you will get error C2381: 'exit' : redefinition; __declspec(noreturn) differs
// #include <windows.h>
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>

#include "CG_Engine/cg_drawing_primitives.hh"
#include "CG_Engine/cg_filling_primitives.hh"
#include "CG_Engine/Basic/cg_gl_interface.hh"
#include "CG_Elements/CG_Characters/CG_Coord_Character.hpp"
#include "CG_Elements/CG_Routines/cg_draw_routines.hh"
#include "CG_Elements/CG_Characters/chars_generator/cg_char_defs.hh"
#include "CG_Elements/CG_Generics/cg_simple_figures.hh"
#include "CG_Elements/CG_Generics/CG_Figure.hpp"
using namespace CGLib;

/* Intialization of the sign of drawing graphics */
bool drawSquare = true;
bool drawTriangle = false;

/******************************************************************/

/* output code by Mark Kilgard */
void output(int x, int y, char *string) {
	int len, i;
	glRasterPos2f(x, y);
	len = (int) strlen(string);
	for (i = 0; i < len; i++) {
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, string[i]);
	}
}

int current = 0;

void Keyboard(unsigned char key, int x, int y) {
	//cout<<key<<endl;
	switch (key) {
	case 27:       //ESCAPE key
		exit(0);
		break;

	case 'd':      // Draw name
		if (current < 2) {
			++current;
		}
		break;

	case 'a':      // Draw face
		if (current > 0) {
			--current;
		}
		break;
	}
}

CG_Coord_Character* name[9] = { new CG_Coord_Character('A'), new CG_Coord_Character('L'),
		new CG_Coord_Character('B'), new CG_Coord_Character('Y'), new CG_Coord_Character(
				'S'), new CG_Coord_Character('C'), new CG_Coord_Character('O'),
		new CG_Coord_Character('L'), new CG_Coord_Character('A') };

/******************************************************************/

inline void show_name(void) {
	int y = 450;
	int x = 250;
	int dist = 30;

	int d = 0;
	for (int i = 0; i < 4; i++) {
		if (i > 0) {
			d += name[i - 1]->getWidth();
		}
		cg_draw_figure(*(name[i]), x + d, y);
		d += dist;
	}

	d = 0;
	x -= 280;
	y -= 300;
	for (int i = 4; i < 9; i++) {
		if (i > 0) {
			d += name[i - 1]->getWidth();
		}
		cg_draw_figure(*(name[i]), x + d, y);
		d += dist;
	}

}

void show_face(void) {
	glColor3f(1.0, 0.0, 0.0);
	cg_draw_circumference(500, 400, 350, 0, 360);

	glColor3f(1.0, 1.0, 0.0);
	CG_Figure* s1 = new CG_Figure(cg_star(7, 60, 60));
	cg_draw_figure(*s1, 230, 450);
	CG_Figure* s2 = new CG_Figure(cg_star(7, 60, 60));
	cg_draw_figure(*s2, 580, 450);

	glColor3f(0.0, 0.0, 1.0);
	CG_Figure* tri = new CG_Figure(cg_triangle(100, 150, 50));
	cg_draw_figure(*tri, 450, 300);

	glColor3f(0.0, 1.0, 0.2);
	CG_Figure* tra = new CG_Figure(cg_trapezium(150, 100, 250, -50));
	cg_draw_figure(*tra, 375, 150);

	delete s1;
	delete s2;
	delete tri;
	delete tra;
}

void show_fill(void) {
	glColor3f(1.0, 0.0, 0.0);
	cg_init_window(1000, 1000);

	//draw the circle
	cg_polygon_new_fill();
	cg_polygon_add_circle(500, 400, 350, 0, 360);
	cg_polygon_draw();

	//draw the eyes
	glColor3f(1.0, 1.0, 0.0);
	CG_Figure* s1 = new CG_Figure(cg_star(7, 60, 60));
	CG_Figure* s2 = new CG_Figure(cg_star(7, 60, 60));
	cg_polygon_new_fill();
	cg_polygon_fill_figure(*s1, 230, 450);
	cg_polygon_fill_figure(*s1, 580, 450);
	cg_polygon_draw();
	delete s1;
	delete s2;

	//draw the triangle
	glColor3f(0.0, 0.0, 1.0);
	cg_polygon_new_fill();
	CG_Figure* tri = new CG_Figure(cg_triangle(100, 150, 50));
	cg_polygon_fill_figure(*tri, 450, 300);
	cg_polygon_draw();
	delete tri;

	//draw the mouth
	glColor3f(0.0, 1.0, 0.2);
	CG_Figure* tra = new CG_Figure(cg_trapezium(150, 100, 250, -50));
	cg_polygon_new_fill();
	cg_polygon_fill_figure(*tra, 375, 150);
	cg_polygon_draw();
	delete tra;
	//cg_set_auto_close(false);
	//cg_polygon_fill_figure(*name[2], 100, 100);
}

void display(void) {
	/* clear the screen to the clear colour */
	glClear(GL_COLOR_BUFFER_BIT);

	if (current == 0) {
		glColor3f(1.0, 0.0, 0.0);
		glBegin(GL_POINTS); // in the first lab assignment, you can only use GL_POINT

		show_name();

		glEnd();

		;
	}

	if (current == 1) {
		glBegin(GL_POINTS); // in the first lab assignment, you can only use GL_POINT

		show_face();

		glEnd();
	}

	if (current == 2) {
		glBegin(GL_POINTS);
		show_fill();
		glEnd();

		glColor3f(1.0, 1.0, 1.0);
		//output(200, 150, "Triangle");
	}

	/* swap buffers */
	glutSwapBuffers();
}

void reshape(int w, int h) {
	/* set the viewport */
	glViewport(0, 0, (GLsizei) w, (GLsizei) h);

	/* Matrix for projection transformation */
	glMatrixMode(GL_PROJECTION);

	/* replaces the current matrix with the identity matrix */
	glLoadIdentity();

	/* Define a 2d orthographic projection matrix */
	gluOrtho2D(0.0, (GLdouble) w, 0.0, (GLdouble) h);
}

/*******************************************************************/

int main(int argc, char** argv) {

	/* deal with any GLUT command Line options */
	glutInit(&argc, argv);

	/* create an output window */
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(1000, 1000);
	//communicate the window size
	//cg_init_window(1000,1000);

	/* set the name of the window and try to create it */
	glutCreateWindow("CS 488 - Sample");

	/* specify clear values for the color buffers */
	glClearColor(1.0, 1.0, 1.0, 1.0);

	/* Receive keyboard inputs */
	glutKeyboardFunc(Keyboard);

	/* assign the display function */
	glutDisplayFunc(display);

	/* assign the idle function */
	glutIdleFunc(display);

	/* sets the reshape callback for the current window */
	glutReshapeFunc(reshape);

	/* enters the GLUT event processing loop */
	glutMainLoop();

	return (EXIT_SUCCESS);
}

