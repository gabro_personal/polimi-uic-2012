#pragma once
#include <vector>
using namespace std;
#include "Polygon.h"

/************************************************************************/
/* This class is used to draw a shapes made of different polygons       */
/* the shape can be resize or moved                                     */
/************************************************************************/
class Shape
{
public:
	Shape(void);
	~Shape(void);

	/* Draws the shape using GL_POINTS. Returns false is something went wrong, otherwise true*/
	bool Draw();
	/*Adds a polygon to the current set of the Shape*/
	void AddPolygon(Polygon p);

	/*GETTERS AND SETTERS*/
	int Width() const;
	void Width(int val);
	int Height() const;
	void Height(int val);
	vector<int> Position() const;
	void Position(vector<int> val);
	void Position(int x,int y);
	vector<int> Color() const;

	/*add a polygon with a shape of a star to the current vector of polygons of the shape*/
	void generateStar();

	void fill();

private:
	int width;
	int height;
	vector<int> position;

	/*Contains the primitives from which the shape comes*/
	vector<Polygon> polys;
};