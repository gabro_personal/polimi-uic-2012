#pragma once
#include "Shape.h"
using namespace std;

/************************************************************************/
/* Class used to generate letters in 2D                                 */
/************************************************************************/
class Letter : public Shape
{
public:
	Letter(void);
	~Letter(void);
	/*Generate a 2D letter from the character passed. It returns false if the letter wasn't properly created, otherwise true*/
	bool generateLetter(char c);

	/*Generates a list of Letters from the string passed as parameter*/
	static vector<Letter> GenerateWord(string word);
};

