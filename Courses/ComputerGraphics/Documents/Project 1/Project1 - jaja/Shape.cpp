#include "Shape.h"
#include <vector>
#include "jutils.h"
using namespace std;

/*standard constructor, width and height are set to 50, position at (0,0)*/
Shape::Shape(void)
{
	width = 50;
	height = 50;
	position.resize(2);
}


Shape::~Shape(void)
{

}

/* Draws the shape using GL_POINTS. Returns false is something went wrong, otherwise true*/
bool Shape::Draw()
{
	for (int i=0;i<this->polys.size();i++){
		polys[i].Draw();
	}
	return true;
}

void Shape::AddPolygon( Polygon p ){
	this->polys.push_back(p);
}

#pragma region Getters and Setters
void Shape::Height( int val )
{
	if (height != val)
	{
		for(int i=0;i<polys.size();i++){	//resizes every polygon included in the shape
			double growth = (double)val/(double)height*100;
			polys[i].Resize(growth,0);
		}
		height = val;
	}
}

int Shape::Height() const
{
	return height;
}

void Shape::Width( int val )
{
	if (width != val)
	{
		for(int i=0;i<polys.size();i++){	//resizes every polygon included in the shape
			double growth = (double)val/(double)width*100;
			polys[i].Resize(growth,1);
		}
		width = val;
	}
}

int Shape::Width() const
{
	return width;
}

vector<int> Shape::Position() const
{
	return position;
}

void Shape::Position( vector<int> val )
{
	Position(val[0],val[1]);
}

void Shape::Position(int x, int y)
{
	if (x!=this->position[0] || y!=this->position[1]){
		for(int i=0;i<polys.size();i++){		//moves every polygon included in the shape
			int xOffset = x - this->position[0];
			int yOffset = y - this->position[1];
			polys[i].Move(xOffset,yOffset);
		}
		position[0] = x;
		position[1] = y;
	}
}
#pragma endregion Getters and Setters

void Shape::generateStar()
{
	Polygon p;
	vector<int> x;
	vector<int> y;
	x = FillIntArray(10,20,50,80,70,100,61, 50,39, 0,30);
	y = FillIntArray(10, 0,21, 0,41, 59,59,100,59,59,41);
	for (int i=0;i<x.size();i++)
		/*Add the vertex to the polygon taking into account the size and the position values of the Shape*/
		p.addVertex(x[i]*this->Width()/100+this->Position()[0],y[i]*this->Height()/100+this->Position()[1]);
	AddPolygon(p);
}

void Shape::fill()
{
	//the shape fill method simply calls the fill method of the first polygon
	polys[0].fill();
}
