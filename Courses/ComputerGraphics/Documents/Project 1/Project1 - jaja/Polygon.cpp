#pragma once
#include "Polygon.h"
#include "jutils.h"
#include <vector>
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
using namespace std;

Polygon::Polygon(void)
{
}

Polygon::~Polygon(void)
{
}

void Polygon::addVertex( int x, int y )
{
	vector<int> v(2,0);
	v[0]=x;
	v[1]=y;
	this->points.push_back(v);	//Put the new vertex at the end of the vector
}

void Polygon::Draw()
{
	for (unsigned int i=1;i<this->points.size();i++){	//Draw a line from each vertex to the next one
		DrawLine(points[i-1][0],points[i-1][1],points[i][0],points[i][1]);
	}
	if (points.size()>2){	//If the polygon as three or more vertex, close the polygon
		DrawLine(points[0][0],points[0][1],points[points.size()-1][0],points[points.size()-1][1]);
	}
}

void Polygon::Resize(double val,int type ){
	if (type==0){	//resizes height
		for (unsigned int i=0;i<this->points.size();i++){
			this->points[i][1]*=val/100;
		}
	}
	if (type==1){	//width
		for (unsigned int i=0;i<this->points.size();i++){
			this->points[i][0]*=val/100;
		}
	}

}

void Polygon::Move( int xOffset, int yOffset )
{
	for (unsigned int i=0;i<this->points.size();i++){
		this->points[i][0] += xOffset;
		this->points[i][1] += yOffset;
	}
}

//insert an edge in the right position of the edge table line
void insertEdge(vector<Edge> *l,Edge e){
	vector<Edge>::iterator it;
	for (it=l->begin(); it<l->end(); it++){
		if(it->x>e.x || (it->x==e.x && it->slope>e.slope)){
			l->insert(it,e);
			return;
		}
	}
	l->push_back(e);
}

//insert a line in the right position of the edge table
void insertLine(EdgeTable *t,EdgeTableLine l){
	vector<EdgeTableLine>::iterator it;
	for (it=t->lines.begin(); it<t->lines.end(); it++){
		if(it->y > l.y){
			t->lines.insert(it,l);
			return;
		}
	}
	t->lines.push_back(l);
}

EdgeTableLine *findLine(int y, EdgeTable *t){
	for (int i = 0; i<t->lines.size();i++){
		if(t->lines[i].y==y)
			return &t->lines[i];
	}
	return NULL;
}

EdgeTableLine *findInsertLine(int y, EdgeTable *t){
	//create and insert the line
	EdgeTableLine *l = findLine(y,t);
	if(l==NULL){
		EdgeTableLine newLine;
		newLine.y = y;
		insertLine(t,newLine);
		l = findLine(y,t);
	}
	return l;
}

EdgeTable Polygon::generateEdgeTable()
{
	EdgeTable t;

	for (int i = 0;i<this->points.size();i++){
		int cx = points[i][0],
			cy = points[i][1];
		int ox = (i==points.size()-1) ? points[0][0] : points[i+1][0],
			oy = (i==points.size()-1) ? points[0][1] : points[i+1][1];
		for(int c=0;c<2;c++){
			if (oy>cy){
				EdgeTableLine *l = findInsertLine(cy,&t);
				Edge e;
				e.x = cx;
				e.ymax = oy;
				e.slope = (double)(cx-ox)/(double)(cy-oy);
				insertEdge(&l->edges,e);
			}
			ox = (i==0) ? points[points.size()-1][0] : points[i-1][0];
			oy = (i==0) ? points[points.size()-1][1] : points[i-1][1];
		}
	}
	return t;
}

void Polygon::fill()
{
	EdgeTable t = generateEdgeTable();
	vector<Edge> AET;
	for (int y = t.lines[0].y;;y++){	//iterate from the first to the last y
		/*add entering edges*/
		EdgeTableLine* entering = findLine(y,&t);
		if(entering!=NULL)
		{
			for (vector<Edge>::iterator it = entering->edges.begin(); it<entering->edges.end(); it++){
				insertEdge(&AET,*it);
			}
		}
		/*remove exiting edges*/
		for (int i = 0; i<AET.size(); i++){
			if(AET[i].ymax==y){
				AET.erase(AET.begin()+i);
				i--;
			}
		}

		if(AET.size()==0) break;

		/*draw the scanline*/
		bool bit = false;
		glBegin(GL_POINTS);
		vector<Edge>::iterator nextPoint = AET.begin();
		for (int x=AET[0].x;x<AET[AET.size()-1].x;x++){
			while(x >= nextPoint->x){	//has passed the vertex
				bit =! bit;				//invert the bit
				nextPoint++;			//pass to the next vertex
			}	
			if(bit) glVertex2f(x,y);	//draw
		}
		glEnd();

		/*update x*/
		for (vector<Edge>::iterator it=AET.begin(); it<AET.end();it++){
			it->x+=it->slope;
		}
	}
}