#pragma once
#include <vector>
using namespace std;

/************************************************************************/
/* This library contains useful functions for programming with the      */
/* GLUT framework                                                       */
/*                                                                      */
/* Author: Giacomo Tagliabue                                            */
/* 2012                                                                 */
/************************************************************************/

//************************************
// swaps the two values of the integer passed as pointers
//************************************
void XorISwap(int *x, int *y);

//************************************
// draws a straight line using the Bresenham's line algorithm
//************************************
void DrawLine(int beginx, int beginy, int endx, int endy);

//************************************
// draws a circle using the Bresenham's integer midpoint circle algorithm,
// if the last parameter is true, the circle will be filled, otherwise it draws only the circonference
//************************************
void DrawCircle(int radius, int centerx, int centery, bool fill);

//************************************
// fills the array with the integer passed as parameter
// the first argument must be the length of the array
//************************************
vector<int> FillIntArray(int n, ...);

//************************************
// These three structs are used for implementig the
// EdgeTable data structure used in the PolygonFill algorithm
//************************************
typedef struct Edge
{
	double x;
	int ymax;
	double slope;
}Edge;
typedef struct EdgeTableLine{
	int y;
	vector<Edge> edges;
}EdgeTableLine;
typedef struct EdgeTable
{
	vector<EdgeTableLine> lines;
}EdgeTable;