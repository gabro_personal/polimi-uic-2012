#include "Letter.h"
#include <vector>
#include <string>
#include "jutils.h"
using namespace std;

Letter::Letter(void)
{

}

vector<Letter> Letter::GenerateWord(string word)
{
	vector<Letter> c(word.length());
	for (unsigned int i=0; i<word.length(); i++)
	{
		c[i].generateLetter(word[i]);
	}
	return c;
}

Letter::~Letter(void){

}

bool Letter::generateLetter( char c )
{	
	Polygon p,p2,p3; //some letters may use more than on polygon
	vector<int> x;
	vector<int> y;
	vector<int> x2;
	vector<int> y2;
	vector<int> x3;
	vector<int> y3;
	switch(c){ //Other letters to be implemented
	case 'G': case 'g':
		x = FillIntArray(20,5,95,100,100,95,65,60,60,65,80,80,20,20,95,100,100, 95,  5, 0,0);
		y = FillIntArray(20,0, 0,  5, 55,60,60,55,45,40,40,20,20,80,80, 85, 95,100,100,95,5);
		break;
	case 'I': case 'i': 
		x = FillIntArray(8,55,60,60, 55, 45,40,40,45);
		y = FillIntArray(8, 0, 5,95,100,100,95, 5, 0);
		break;
	case 'A': case 'a': 
		x = FillIntArray(14,5,15,20,20,80,80,85, 95,100,100, 95,  5, 0,0);
		y = FillIntArray(14,0, 0, 5,40,40, 5, 0,  0,  5, 95,100,100,95,5);
		x2 = FillIntArray(8,25,75,80,80,75,25,20,20);
		y2 = FillIntArray(8,60,60,65,75,80,80,75,65);
		break;
	case 'C': case 'c':
		x = FillIntArray(14,5,95,100,100,95,20,20,95,100,100, 95,  5, 0,0);
		y = FillIntArray(14,0, 0,  5, 15,20,20,80,80, 85, 95,100,100,95,5);
		break;
	case 'O': case 'o':
		x = FillIntArray(8,5,95,100,100, 95,  5, 0,0);
		y = FillIntArray(8,0, 0,  5, 95,100,100,95,5);
		x2 = FillIntArray(8,25,75,80,80,75,25,20,20);
		y2 = FillIntArray(8,20,20,25,75,80,80,75,25);
		break;
	case 'M': case 'm':
		x = FillIntArray(19,5,15,20,20,40,60,80,80,85,95,100,100, 95, 80,50, 20,  5, 0,0);
		y = FillIntArray(19,0, 0, 5,80,40,40,80,5,  0, 0,  5, 95,100,100,60,100,100,95,5);
		break;
	case ' ': break;
	case 'T': case 't':
		x = FillIntArray(14,45,55,60,60,95,100,100, 95,  5, 0, 0, 5,40,40);
		y = FillIntArray(14, 0, 0, 5,80,80, 85, 95,100,100,95,85,80,80, 5);
		break;
	case 'L': case 'l':
		x = FillIntArray(11,5,95,100,100,95,20,20, 15,  5, 0,0);
		y = FillIntArray(11,0, 0,  5, 15,20,20,95,100,100,95,5);
		break;
	case 'B': case 'b':
		x = FillIntArray(12,5,85,100,100,85,85,100,100, 85,   5,   0,0);
		y = FillIntArray(12,0, 0, 15, 30,45,55, 70, 85, 100, 100, 95,5);
		x2 = FillIntArray(6,20,75,80,80,75,20);
		y2 = FillIntArray(6,15,15,20,35,40,40);
		x3 = x2;
		y3 = FillIntArray(6,60,60,65,80,85,85);
		break;
	case 'U': case 'u':
		x = FillIntArray(14,5,95,100,100, 95, 85,80,80,20,20, 15,  5, 0,0);
		y = FillIntArray(14,0, 0,  5, 95,100,100,95,20,20,95,100,100,95,5);
		break;
	case 'E': case 'e':
		x = FillIntArray(20,5,95,100,100,95,20,20,95,100,100,95,20,20,95,100,100, 95,  5, 0,0);
		y = FillIntArray(20,0, 0,  5, 15,20,20,40,40, 45, 55,60,60,80,80, 85, 95,100,100,95,0);
		break;
	default: return false;
	}
	for (int i=0;i<x.size();i++)
		/*Add the vertex to the polygon taking into account the size and the position values of the Shape*/
		p.addVertex(x[i]*this->Width()/100+this->Position()[0],y[i]*this->Height()/100+this->Position()[1]);
	AddPolygon(p);	//add the polygon to the Shape
	if (!x2.empty()){
		for (int i=0;i<x2.size();i++)
			p2.addVertex(x2[i]*this->Width()/100+this->Position()[0],y2[i]*this->Height()/100+this->Position()[1]);
		AddPolygon(p2);
	}
	if (!x3.empty()){
		for (int i=0;i<x3.size();i++)
			p3.addVertex(x3[i]*this->Width()/100+this->Position()[0],y3[i]*this->Height()/100+this->Position()[1]);
		AddPolygon(p3);
	}
	return true;
}
