#pragma once
#include <vector>
#include <iostream>
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include <stdarg.h>
using namespace std;

#include "jutils.h"

void XorISwap (int *x, int *y) {
	// This is the basic implementation of the classic xorSwap
	// Algorithm for integer numbers
	if (x != y) {
		*x ^= *y;
		*y ^= *x;
		*x ^= *y;
	}
}


void DrawLine(int beginx, int beginy, int endx, int endy)
{
	int x,y;			//coordinates

	/*using pointers permits to reuse the same code while |slope|>1, just inverting
	 *the x pointer with the y and viceversa*/
	int *wx=&x,*wy=&y;

	/*case |slope|>1*/
	if (abs(endx-beginx)<abs(endy-beginy)){
		XorISwap(&endx,&endy);				//swap x and y
		XorISwap(&beginx,&beginy);
		wx=&y;								//redirect pointers
		wy=&x;
	}

	/*case when the line goes backward*/
	if (endx<beginx)
	{
		XorISwap(&beginx,&endx);	//swap begin point with end point
		XorISwap(&beginy,&endy);
	}

	int dx =  abs(endx-beginx);		//delta along x
	int dy =  abs(endy-beginy);		//delta along y
	int sy = beginy<=endy ? 1 : -1;	//the y increment is negative if slope < 0
	int d=2*dy-dx;					//decision variable
	int incrE=2*dy;					//increment values
	int incrNE=2*(dy-dx);

	x=beginx;
	y=beginy;
	
	while (x<endx){
		glBegin(GL_POINTS);
		glVertex2f(*wx,*wy);		//draw the point
		glEnd();
		if (d<=0){					//case 1: go east
			d+=incrE;
		}
		else{						//case 2: go north-east
			d+=incrNE;
			y+=sy;
		}
		x++;
	}
}

void DrawCircle(int radius, int centerx, int centery, bool fill){
	/*starting point: top of the circle*/
	int x = 0, y = radius;

	int d = 1-radius;	//decision variable
	
	glBegin(GL_POINTS);
	while(y>=x){
		glVertex2f(centerx+x,centery+y);	// II octant
		glVertex2f(centerx+x,centery-y);	// VIII octant
		glVertex2f(centerx-x,centery+y);	// IV octant
		glVertex2f(centerx-x,centery-y);	// VI octant
		/*the odd octants are obtained swapping the x coordinate with the y*/
		glVertex2f(centerx+y,centery+x);	// I octant
		glVertex2f(centerx+y,centery-x);	// VII octant
		glVertex2f(centerx-y,centery+x);	// III octant
		glVertex2f(centerx-y,centery-x);	// V octant

		if(fill){
			for (int f=-x;f<=x;f++){
				glVertex2f(centerx+f,centery+y);
				glVertex2f(centerx+f,centery-y);
			}
			for (int f=-y;f<=y;f++){
				glVertex2f(centerx+f,centery+x);
				glVertex2f(centerx+f,centery-x);
			}
		}


		if (d<0){				//select East
			d += 2*x+3;			//incrementing decision variable
		}
		else{					//select SouthEast
			d += 2*(x-y)+5;		//incrementing decision variable
			y--;
		}
		x++;
	}
	glEnd();
	//if (fill){
	//	//EdgeTable t;
	//	//EdgeTableLine l;
	//	Edge e1,e2;
	//	e1.x = centerx - radius;	//the two edges start at the center x and bottom y
	//	e2.x = centerx + radius;
	//	e1.ymax = centery+radius;
	//	e2.ymax = centery+radius;
	//	e1.slope = 0;
	//	e2.slope = 0;
	//	vector<Edge> AET;
	//	AET.push_back(e1); AET.push_back(e2);
	//	for (y=centery; y<=centery+radius;y++){
	//		/*draw the scanline*/
	//		bool bit = false;
	//		glBegin(GL_POINTS);
	//		vector<Edge>::iterator nextPoint = AET.begin();
	//		for (x=AET[0].x;x<AET[AET.size()-1].x;x++){
	//			while(x >= nextPoint->x){	//has passed the vertex
	//				bit =! bit;				//invert the bit
	//				nextPoint++;			//pass to the next vertex
	//			}	
	//			if(bit){
	//				glVertex2f(x,y);	//draw
	//				glVertex2f(x,2*centery-y);
	//			}

	//		}
	//		glEnd();

	//		/*update x AND slope*/
	//		AET[0].x += AET[0].slope;
	//		AET[1].x += AET[1].slope;
	//		//How to update slope?
	//		double m = 0.01;
	//		AET[0].slope += m;
	//		AET[1].slope -= m;
	//		m *=0.1;
	//	}
	//}
}

vector<int> FillIntArray( int n, ... )
{
	vector<int> v;
	va_list ap;
	va_start(ap,n);
	for(int i=0;i < n; i++){
		v.push_back(va_arg(ap,int));	//insert the ith argument at the end of the vector
	}
	va_end(ap);
	return v;
}
