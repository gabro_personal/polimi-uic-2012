#pragma once
#include <vector>
using namespace std;
#include "jutils.h"

/************************************************************************/
/* This class contains the function to draw a polygon                   */
/************************************************************************/
class Polygon
{
public:
	Polygon(void);
	~Polygon(void);

	/*add a point to the current set of vertexes*/
	void addVertex(int x, int y);
	/*draws the polygon*/
	void Draw();
	/*resizes the polygon by the given percentage
	 *val: value in percentage of the resizing
	 *type: the dimension to resize: height=0, width=1*/
	void Resize(double val, int type);
	/*Moves the polygon for the given offset*/
	void Move(int xOffset, int yOffset);

	void fill();
	EdgeTable generateEdgeTable();
private:
	vector< vector<int> > points;
};