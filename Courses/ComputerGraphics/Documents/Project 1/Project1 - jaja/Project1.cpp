/*
** CS 488 – Fall 2012 
** Project 1 – Scan conversion & polygon fill
**
** Giacomo Tagliabue
**
** Compiled on windows 7 - Visual Studio 2010
*/


#include "Letter.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include <iostream>
#include "jutils.h"

using namespace std;

/*MACROS*/
#define MINNUMSLIDE 1
#define MAXNUMSLIDE 3
#define WINDOW_SIZE 1000,1000
#define BACKGROUND_COLOR 0.1,0.1,0.1,1.0

#define SLIDE1_NAME "GiacomoTagliabue"
#define SLIDE2_NAME_LENGHT 7
#define SLIDE1_COLORS 0.941,0.588,0.035
#define SLIDE1_INTERLEAVE 10
#define SLIDE1_START_NAME_POSX 100
#define SLIDE1_START_NAME_POSY 420
#define SLIDE1_START_LASTNAME_POSX 0
#define SLIDE1_START_LASTNAME_POSY 250
#define SLIDE1_LETTER_WIDTH 100
#define SLIDE1_LETTER_HEIGHT 125

#define SLIDE2_COLORS 0.3,0.3,0.3
#define SLIDE2_REYE_POS 300,550
#define SLIDE2_LEYE_POS 600,550
#define SLIDE2_EYE_WIDTH 100
#define SLIDE2_EYE_HEIGHT 100
#define SLIDE2_MOUTH_POS1 450,300
#define SLIDE2_MOUTH_POS2 550,300
#define SLIDE2_MOUTH_POS3 570,350
#define SLIDE2_MOUTH_POS4 420,350
#define SLIDE2_NOSE_POS1 475,450
#define SLIDE2_NOSE_POS2 525,450
#define SLIDE2_NOSE_POS3 510,500
#define SLIDE2_CIRCLE_POS 500,500
#define SLIDE2_CIRCLE_RADIUS 300

#define SLIDE3_EYE_FILL 0.105,0.631,0.886
#define SLIDE3_NOSE_FILL 0.627,0.313,0.0
#define SLIDE3_MOUTH_FILL 0.54,0.749,0.149
#define SLIDE3_FACE_FILL 0.801,0.443,0.521



/*GLOBAL VARIABLES*/
int slideNumber=MINNUMSLIDE;	//number of current slide to visualize

vector<Letter> name;			//First slide global variable

Shape rEye,lEye;				//Second slide global variables
Polygon nose,mouth;

/*Function for handling the keyboard interrupts*/
void Keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
		case 'a':	//Pass to the next slide
			if (slideNumber>MINNUMSLIDE){
				slideNumber--;
			}
			break;
		case 'd':	//Pass to the previous slide
			if (slideNumber<MAXNUMSLIDE){
				slideNumber++;
			}
			break;		
	}
}

/*FIRST SLIDE: NAME + LASTNAME*/
void GenerateFirstSlide(){
	name = Letter::GenerateWord(SLIDE1_NAME);
	int posy = SLIDE1_START_NAME_POSY;
	int posx = SLIDE1_START_NAME_POSX;
	int interleave = SLIDE1_INTERLEAVE; //interleave space between characters
	int LWidth = SLIDE1_LETTER_WIDTH;
	int LHeight = SLIDE1_LETTER_HEIGHT;

	for (unsigned int i=0;i<name.size();i++)
	{
		posx += interleave;		//Moves the cursor by the interleaving space
		name[i].Height(LHeight); //Sets the size and the position of the letters
		name[i].Width(LWidth);
		name[i].Position(posx,posy);
		name[i].Draw();			//Draws the letter
		posx += LWidth;			//Moves the cursor to the next letter position
		if (i==SLIDE2_NAME_LENGHT-1)
		{
			posx=SLIDE1_START_LASTNAME_POSX;
			posy=SLIDE1_START_LASTNAME_POSY;
		}
	}
}

/*SECOND SLIDE: SMILING FACE*/
void GenerateSecondSlide(){
	rEye.Width(SLIDE2_EYE_WIDTH);  lEye.Width(SLIDE2_EYE_WIDTH);
	rEye.Height(SLIDE2_EYE_HEIGHT); lEye.Height(SLIDE2_EYE_HEIGHT);
	rEye.Position(SLIDE2_REYE_POS); lEye.Position(SLIDE2_LEYE_POS);
	rEye.generateStar(); lEye.generateStar();
	nose.addVertex(SLIDE2_NOSE_POS1); nose.addVertex(SLIDE2_NOSE_POS2); nose.addVertex(SLIDE2_NOSE_POS3);
	mouth.addVertex(SLIDE2_MOUTH_POS1); mouth.addVertex(SLIDE2_MOUTH_POS2); mouth.addVertex(SLIDE2_MOUTH_POS3); mouth.addVertex(SLIDE2_MOUTH_POS4);
}

/*draws the face for slide 2 and 3*/
void drawFace(){
	rEye.Draw();
	lEye.Draw();
	nose.Draw();
	mouth.Draw();
}

void display(void) {
	/* clear the screen to the clear color */
	glClear(GL_COLOR_BUFFER_BIT);
	switch(slideNumber){	//selects which slide to visualize depending on the slideNumber variable
	case 1:					//slide 1
		glColor3f(SLIDE1_COLORS);
		for (unsigned int i=0;i<name.size();i++){
			name[i].Draw();
		}
		break;

	case 2:					//slide 2
		glColor3f(SLIDE2_COLORS);
		DrawCircle(SLIDE2_CIRCLE_RADIUS,SLIDE2_CIRCLE_POS,false);
		drawFace();
		break;

	case 3:					//slide 3
		glColor3f(SLIDE3_FACE_FILL);
		DrawCircle(SLIDE2_CIRCLE_RADIUS,SLIDE2_CIRCLE_POS,true);
		glColor3f(SLIDE2_COLORS);
		drawFace();
		glColor3f(SLIDE3_MOUTH_FILL);
		mouth.fill();
		glColor3f(SLIDE3_EYE_FILL);
		lEye.fill();
		rEye.fill();
		glColor3f(SLIDE3_NOSE_FILL);
		nose.fill();
		break;
	}	
	/* swap buffers */
	glutSwapBuffers();
}

void reshape (int w, int h) {
	/* set the viewport */
	glViewport (0, 0, (GLsizei) w, (GLsizei) h);

	/* Matrix for projection transformation */
	glMatrixMode (GL_PROJECTION); 

	/* replaces the current matrix with the identity matrix */
	glLoadIdentity ();

	/* Define a 2d orthographic projection matrix */
	gluOrtho2D (0.0, (GLdouble) w, 0.0, (GLdouble) h);
}

/*******************************************************************/

int main(int argc, char** argv) {

	/* deal with any GLUT command Line options */
	glutInit(&argc, argv);

	/* create an output window */
	glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);
	glutInitWindowSize(WINDOW_SIZE);

	/* set the name of the window and try to create it */
	glutCreateWindow("Project 1");

	/*Initialize the objects and variables for the three slides*/
	GenerateFirstSlide();
	GenerateSecondSlide();

	/* specify clear values for the color buffers */
	glClearColor (BACKGROUND_COLOR);

	/* Receive keyboard inputs */
	glutKeyboardFunc (Keyboard);

	/* assign the display function */
	glutDisplayFunc(display);

	/* assign the idle function */
	glutIdleFunc(display);

	/* sets the reshape callback for the current window */
	glutReshapeFunc(reshape);

	/* enters the GLUT event processing loop */
	glutMainLoop();

	return (EXIT_SUCCESS);
}