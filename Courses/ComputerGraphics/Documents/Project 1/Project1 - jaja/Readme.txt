CS 488 – Fall 2012 
Project 1 – Scan conversion & polygon fill
Student: Giacomo Tagliabue

This is the code for the first project of CS 488 Course.

The program is written using C++ and compiled using g++, there are some classes for the figures (Shape, Polygon...), other functions used in many parts of the code are in the library "jutils.h".

--Description of the classes
Polygon: used to draw a polygon, it has utility methods to move and stretch the polygon.
Shape: Can contain more than one polyogon.
Letter: inherits Shape, has some methods to draw letters and words

--Description of the algorithms
-For drawing line I used the Bresenham algorithm. In order to be able to draw a line with every step, I swap x and y when needed or I invert the end points with the begin points (see comments on the code)
-For drawing circles I used the Bresenham algorithm for scan converting circles. To draw all of the octets of a circle I draw every permutation of x,y,-x,-y (see comments on the code)
-For filling polygons I used the Filling algorithm presented during the lessons and on the book.
-For filling circles I implemented an algorithm that fill the circle during the scan conversion of its circonference.

--Issues
Compiling on Windows, some random vertical black lines appear over the top of any image drawn. This problem seems not to affect OSX