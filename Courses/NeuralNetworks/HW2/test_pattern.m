function [X_test] = test_pattern

% Three letters to be recognized
O = ...
[      1      1      1      1      1      1      1      ;
       1      -1     -1     -1     -1     -1     1      ;
       1      -1     -1     -1     -1     -1     1      ;
       1      -1     -1     -1     -1     -1     1      ;
       1      -1     -1     -1     -1     -1     1      ;
       1      -1     -1     -1     -1     -1     1      ;
       1      1      1      1      1      1      1      ];

I = ...
[      -1     -1     -1     1      -1     -1     -1     ;
       -1     -1     -1     1      -1     -1     -1     ;
       -1     -1     -1     1      -1     -1     -1     ;
       -1     -1     -1     1      -1     -1     -1     ;
       -1     -1     -1     1      -1     -1     -1     ;
       -1     -1     -1     1      -1     -1     -1     ;
       -1     -1     -1     1      -1     -1     -1     ];


E = ...
[      1      1      1      1      1      1      1      ;
       1      -1     -1     -1     -1     -1     -1     ;
       1      -1     -1     -1     -1     -1     -1     ;
       1      1      1      1      1      1      1      ;
       1      -1     -1     -1     -1     -1     -1     ;
       1      -1     -1     -1     -1     -1     -1     ;
       1      1      1      1      1      1      1      ];

% Other letters
C = ...
[      1      1      1      1      1      1      1      ;
       1      -1     -1     -1     -1     -1     -1     ;
       1      -1     -1     -1     -1     -1     -1     ;
       1      -1     -1     -1     -1     -1     -1     ;
       1      -1     -1     -1     -1     -1     -1     ;
       1      -1     -1     -1     -1     -1     -1     ;
       1      1      1      1      1      1      1      ];

F = ...
[      1      1      1      1      1      1      1      ;
       1      -1     -1     -1     -1     -1     -1     ;
       1      -1     -1     -1     -1     -1     -1     ;
       1      1      1      1      1      1      1      ;
       1      -1     -1     -1     -1     -1     -1     ;
       1      -1     -1     -1     -1     -1     -1     ;
       1      -1     -1     -1     -1     -1     -1     ];

S = ...
[      1      1      1      1      1      1      1      ;
       1      -1     -1     -1     -1     -1     -1     ;
       1      -1     -1     -1     -1     -1     -1     ;
       1      1      1      1      1      1      1      ;
       -1     -1     -1     -1     -1     -1     1      ;
       -1     -1     -1     -1     -1     -1     1      ;
       1      1      1      1      1      1      1      ];

T = ...
[      1      1      1      1      1      1      1      ;
       -1     -1     -1     1      -1     -1     -1     ;
       -1     -1     -1     1      -1     -1     -1     ;
       -1     -1     -1     1      -1     -1     -1     ;
       -1     -1     -1     1      -1     -1     -1     ;
       -1     -1     -1     1      -1     -1     -1     ;
       -1     -1     -1     1      -1     -1     -1     ];

L = ...
[      1      -1     -1     -1     -1     -1     -1     ;
       1      -1     -1     -1     -1     -1     -1     ;
       1      -1     -1     -1     -1     -1     -1     ;
       1      -1     -1     -1     -1     -1     -1     ;
       1      -1     -1     -1     -1     -1     -1     ;
       1      -1     -1     -1     -1     -1     -1     ;
       1      1      1      1      1      1      1      ];

O_mutation_2perc = mutate_grid(O, 2);
O_mutation_4perc = mutate_grid(O, 4);
I_mutation_2perc = mutate_grid(I, 2);
I_mutation_4perc = mutate_grid(I, 4);
E_mutation_2perc = mutate_grid(E, 2);
E_mutation_4perc = mutate_grid(E, 4);
C_mutation_2perc = mutate_grid(C, 2);
C_mutation_4perc = mutate_grid(C, 4);
F_mutation_2perc = mutate_grid(F, 2);
F_mutation_4perc = mutate_grid(F, 4);
S_mutation_2perc = mutate_grid(S, 2);
S_mutation_4perc = mutate_grid(S, 4);
T_mutation_2perc = mutate_grid(T, 2);
T_mutation_4perc = mutate_grid(T, 4);
L_mutation_2perc = mutate_grid(L, 2);
L_mutation_4perc = mutate_grid(L, 4);


X_test = [O I E C F S T L O_mutation_2perc O_mutation_4perc I_mutation_2perc I_mutation_4perc ... 
E_mutation_2perc E_mutation_4perc C_mutation_2perc C_mutation_4perc F_mutation_2perc F_mutation_4perc ...
S_mutation_2perc S_mutation_4perc T_mutation_2perc T_mutation_4perc L_mutation_2perc L_mutation_4perc];

