%****Function to calculate the parameters (z,y at the hidden and output layers given the weights at the two layers)******************
function [z_hidden, w_hidden, y_hidden, z_output, w_output, y_output, counter] = calculation(w_hidden, w_output, X)
% Outputs: % z_hidden - hidden layer z value
% w_hidden - hidden layer weight % y_hidden - hidden layer output % Respecitvely for the output layers
% Inputs: % Weights at the hidden and output layers and the training pattern set
counter = 0;

r = 1;
while(r <=15 ),
    r;
    for i = 1:6,
        z_hidden(i) = w_hidden(i,:)*X(:,r);
        
        if (z_hidden(i)>=0),
            y_hidden(i) = 1;
            
        else y_hidden(i) = -1;
            
        end %%End of If loop end %% End of for loop
    end
    z_hidden;
    y_hiddent = y_hidden';
    
    for i = 1:2
        z_output(i) = w_output(i,:)*y_hiddent;
        if (z_output(i)>=0),
            y_output(i) = 1;
        else
            y_output(i) = -1;
        end
    end
    y_output;
    
    % Desired Output
    if (r<=5),
        d1 = [1 1];
    else if (r > 10)
            d1 = [-1 -1];
        end
    end
    
    for i = 1:2;
        error_val(i) = d(1) - y_output(i);
        if (error_val(i)~=0),
            counter = counter + 1;
        end
    end
    
    r = r + 1;
end


%******Function to find weight changes for paired hidden layer**********
function [w_hidden_two] = min_hidden_double(z_hidden,w_hidden,counter,X,nu,k,l)
w_hidden_two = w_hidden;
for j = 1:36,
    w_hidden_two(k,j) = w_hidden_two(k,j) + 2*nu*X(j,15)*counter;
    w_hidden_two(l,j) = w_hidden_two(l,j) + 2*nu*X(j,15)*counter;
    
end

%*********Function to find weight changes at hidden layer**************
function [w_hidden_min] = min_hidden_case(z_hidden,w_hidden,counter,X,nu,k)
w_hidden_min = w_hidden;
for j = 1:36,
    w_hidden_min(k,j) = w_hidden_min(k,j) + 2*nu*X(j,15)*counter;
    
end
%w_hidden_min

%****Function to change weights for the max of 2z values at Output****
function [w_output_max,z_ind] = max_case(z_output,w_output,counter,y_hidden,nu)
%load w_output;
%load z_output;
w_output_max = w_output;
z_ind = find(abs(z_output) == max(abs(z_output)))
for j = 1:5, w_output_max(z_ind,j) = w_output(z_ind,j)+2*nu*y_hidden(j)*counter;
    %	end
end
%
z_output(z_index) = w_output(z_index,:)*y_hiddent;

%****************Function to compute weight change at the output for neuron whose Z value is close to the threshold**********************
function [w_output_min,z_index] = min_case(z_output,w_output,counter,y_hidden,nu)
z_index = find(abs(z_output) == min(abs(z_output))) 
w_output_min = w_output 
for j = 1:5,
    w_output_min(z_index,j) = w_output(z_index,j) + 2*nu*y_hidden(j)*counter;
end
w_output_min
%*******Function to find weight changes with paired output neurons******
function [w_output_two] = min_output_double(z_hidden,y_hidden,counter,X,nu,w_output)
w_output_two = w_output;

for j = 1:6, w_output_two([1:2],j) = w_output([1:2],j)+2*nu*y_hidden(j)*counter;
    
end
y_hidden;
counter;
2*nu*y_hidden*counter;

% Function to mutate the training grids of a specified percent
% This function allows arbitrary size for the grid (no need to be squared)
function [x_muted] = mutate_grid(x, percent)

x_muted = x

[n_rows, n_cols] = size(x);
size_tot = n_rows * n_cols;

n_cells = round(size_tot * percent/100)

for i = 1:n_cells,
    r_x = randi(n_rows,1,1);
    r_y = randi(n_cols,1,1);

    if (x_muted(r_x,r_y) ~= 0),
        x_muted(r_x,r_y) = 0;
    else
        n_cells = n_cells + 1;
    end
end

for i = 1:n_rows,
    for j = 1:n_cols,
        if (x_muted(i,j) == 0)
            if (x(i,j) == -1)
                x_muted(i,j) = 1
            else
                x_muted(i,j) = -1
            end
        end
    end
end
