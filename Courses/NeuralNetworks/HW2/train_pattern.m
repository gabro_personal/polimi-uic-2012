function [X] = train_pattern

% Three letters to be recognized
O = ...
[	1	1	1	1	1	1	1	;
 	1	-1	-1	-1	-1	-1	1	;
 	1	-1	-1	-1	-1	-1	1	;
 	1	-1	-1	-1	-1	-1	1	;
 	1	-1	-1	-1	-1	-1	1	;
 	1	-1	-1	-1	-1	-1	1	;
 	1	1	1	1	1	1	1	];

I = ...
[	-1	-1	-1	1	-1	-1	-1	;
	-1	-1	-1	1	-1	-1	-1	;
	-1	-1	-1	1	-1	-1	-1	;
	-1	-1	-1	1	-1	-1	-1	;
	-1	-1	-1	1	-1	-1	-1	;
	-1	-1	-1	1	-1	-1	-1	;
	-1	-1	-1	1	-1	-1	-1	];


E = ...
[	1	1	1	1	1	1	1	;
	1	-1	-1	-1	-1	-1	-1	;
	1	-1	-1	-1	-1	-1	-1	;
	1	1	1	1	1	1	1	;
	1	-1	-1	-1	-1	-1	-1	;
	1	-1	-1	-1	-1	-1	-1	;
	1	1	1	1	1	1	1	];


% Generate mutations
O_mutation_2perc = mutate_grid(O, 2);
O_mutation_8perc = mutate_grid(O, 8);
O_mutation_16perc = mutate_grid(O, 16);

I_mutation_2perc = mutate_grid(I, 2);
I_mutation_8perc = mutate_grid(I, 8);
I_mutation_16perc = mutate_grid(I, 16);

E_mutation_2perc = mutate_grid(E, 2);
E_mutation_8perc = mutate_grid(E, 8);
E_mutation_16perc = mutate_grid(E, 16);

% Build the training vector
X = [O(:)' O_mutation_2perc(:)' O_mutation_8perc(:)' O_mutation_16perc(:)' I(:)' I_mutation_2perc(:)' I_mutation_8perc(:)' I_mutation_16perc(:)' E(:)' E_mutation_2perc(:)' E_mutation_8perc(:)' E_mutation_16perc(:)'];


