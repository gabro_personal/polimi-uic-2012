\documentclass[a4paper, 12pt]{article}

\usepackage{hyperref}
\usepackage{enumitem}
\usepackage{amssymb}
\usepackage{amsmath}

\newcommand{\separator}{\begin{center}
 	\line(1,0){100}
\end{center}}

\title{\textsc{HPPS \\ second midterm questions}}
\author{Gabriele Petronella}

\begin{document}
\maketitle
\tableofcontents

\clearpage

\section{Question 1}
Snooping protocols: describe and discuss the main characteristics and differences 
of the main two types:
\begin{itemize}
	\item Write-Invalidate Protocol
	\item Write-Update or Write-Broadcast Protocol
\end{itemize}

\separator

\begin{paragraph}{Write-Invalidate}\hfill\\
This snooping protocol allows a single writer and multiple readers. When a writing processor needs to write a value, it sends an invalidation signal over the bus. This causes all the copies in other caches to be invalidated before changing its local copy.
Any subsequent write is then performed locally.

The bus is then used only once (when the first write occurs) and this provides benefits in terms of reducing demands on bus bandwidth.

In case of a read miss, two scenarios may happen depending on the underlying cache policy:
\begin{itemize}
	\item[$\rightarrow$] Write-through policy: the data is simply fetched from the memory, since it is always up-to-date.
	\item[$\rightarrow$] Write-back policy: the data needs to be found by snooping in caches in order to find the most recent copy.
\end{itemize}
\end{paragraph}

\begin{paragraph}{Write-Update}\hfill\\
With this snooping protocol whenever a processor updates some data it broadcasts it over the bus. The caches that hold a copy of the data update it to with the new value.

This protocol makes a heavy use of the bus, but has the advantage of reducing latency since new values appears in caches sooner.
On a read miss it works always like a write-through, since the memory is always up-to-date.
\end{paragraph}

\newpage
\section{Question 2}
Introduce a taxonomy of multiprocessor architectures, explaining their characteristics.

\separator

\begin{table}[!ht]
\centering
	\begin{tabular}{c|cc}
					& \textsc{Single instruction} & \textsc{Multiple instruction}\\
		\hline
		\textsc{Single data} & SISD & MISD\\
		\textsc{Multiple data} & SIMD & MIMD\\
	\end{tabular}
	\caption{The taxonomy proposed by Michael J. Flynn in 1966}
\end{table}

\begin{paragraph}{SISD}\hfill\\
Uniprocessor systems
\end{paragraph}

\begin{paragraph}{MISD}\hfill\\
Very uncommon architecture generally used for fault tolerance
\end{paragraph}

\begin{paragraph}{SIMD}\hfill\\
An architecture which exploits multiple data streams against a single instruction stream to perform operations which may be naturally parallelized.

A central controller broadcast an instruction to multiple processing elements; all computations are fully synchronized (single program counter) and it only requires one central controller and one copy of the running program.

A typical example is vector processing, where operations are applied to arrays of numbers instead of scalars.
\end{paragraph}

\begin{paragraph}{MIMD}\hfill\\
In this architecture each processor fetches its own instructions and operates on its own data.
This allows flexible and cost efficient machines to be built.

This architecture splits in two sub-categories:
\begin{description}[labelindent=1cm]
	\item[Shared Memory] The processors are all connected to a globally available memory, either via software or hardware means. The main limit is the number of processors that can be put together, which is bounded to a few dozen.
	\item[Distributed Memory] Each processors has its own memory location and no knowledge about the memory state of other processors.
	This allows the memory to be always coherent since there's no contention, being private.

	Processors exchange data via message passing and this may represent a significant overhead.
\end{description}
\end{paragraph}

\newpage
\section{Question 3}
MSI vs MESI invalidate protocols

\separator

\begin{paragraph}{MSI}\hfill\\
Every block in cache can be in three states
	\begin{description}[labelindent=1cm]
		\item[Modified] If the block has been modified locally.
		\item[Shared] If the block is shared coherently with other caches.
		\item[Invalid] If the block has been modified by some other cache and it's no longer valid.
	\end{description}
	At every read the block is obtained as `Shared', even if the processor is the only one reading it.
\end{paragraph}

\begin{paragraph}{MESI}\hfill\\
Every block in cache can be in four state
	\begin{description}[labelindent=1cm]
		\item[Modified] If the block has been modified locally (dirty) and cannot be shared.
		\item[Exclusive] If block is clean and it's the only existing copy.
		\item[Shared] If the block is clean and it's shared coherently with other caches.
		\item[Invalid] If the block has been modified by some other cache and it's no longer valid.
	\end{description}
\end{paragraph}

MESI is an improvement of MSI that allows to reduce the use of the bus, avoiding to broadcast the modified value of a block if this was in an `Exclusive' state.

Moreover is worthy to notice that `Exclusive' means that the block is load into the cache at the time when no one else was reading it. There can be the case when the cache has the only copy but its state is `Shared'
This happens because there cannot be a downgrade from `Shared' to `Exclusive'.
To summarize,
\begin{center}
	`Exclusive' $\rightarrow$ only one copy\\
	only one copy $\nrightarrow$ `Exclusive'
\end{center}

\newpage
\section{Question 4}
Describe the main architectural limits to ILP that do not allow the possibility to reach the theoretical parallelism present in the program.

\separator

There are several architectural limits to ILP. Let's go through them:
\begin{description}
	\item[Register renaming] the number of registers available for renaming is limited and when there are no free registers the issue is stalled.
	\item[Branch prediction] the branch prediction techniques are not perfect and mispredictions may happen.
	\item[Jump prediction] same as branch prediction
	\item[Memory disambiguation] takes time and it's necessary in order to avoid unexpected behavior in memory operations.
	\item[Cache misses] cache misses can happen, slowing down the execution.
	\item[Maximum issue count] the number of instruction that can be issued in a clock cycle is limited.
	\item[Window size] the dynamic scheduler detects conflict between instruction within a finite lookahead window.
	\item[N. of functional units] the number of functional units is limited.
	\item[N. of register ports] the number of ports available on a register file is limited.
	\item[N. of busses] the number of busses is limited.
\end{description}

\newpage
\section{Question 6}
Explain the concepts of coherence and consistency in multiprocessors. What are 
the main difference?

\separator

\begin{quote}
``Cache coherency problems can arise when more than one processor refers to the same data. Assuming each processor has cached a piece of data, what happens if one processor modifies its copy of the data? The other processor now has a stale copy of the data in its cache.\\
Cache coherency and consistency define the action of the processors to maintain coherence. More precisely, \emph{coherency} defines what value is returned on a read, and \emph{consistency} defines when it is available.''
\footnote{\emph{Optimizing Applications on the Cray X1TM System}, Appendix D, \url{http://docs.cray.com/books/S-2315-50/html-S-2315-50/zfixedgtci5niw.html}}
\end{quote}

Or in other words:
\begin{quote}
``\emph{Coherence} insures that writes to a particular location will be seen in order.\\
\emph{Consistency} insures that writes to different locations will be seen in an order that makes sense, given the source code.''
\footnote{\url{http://people.engr.ncsu.edu/efg/506/s01/lectures/notes/lec14.pdf}}
\end{quote}

\newpage
\section{Question 7}
\label{sec:question7}
Explain the concept of the Virtually Addressed Cache and how it works.  What kind of improvement it can provide? Identify advantages and drawbacks of such 
a solution.

\separator

Virtually Addressed Caches are caches that address blocks in cache with a virtual address (the one used by the process) instead of the physical address of the main memory. The cache will have to translate the virtual address only on a miss, for retrieving the missing block from the main memory. Since misses are less common than hits, this is clearly an optimization of the most common case.

However this technique has some relevant drawbacks. Let's go through them:
\begin{description}[labelindent=1cm]
	\item[Cache flush on switching] whenever a process is switched the virtual addresses in cache refer to different physical addresses, requiring the cache to be flushed.\\
	A possible solution to this problem is to extend the cache address tag with a process identifier (PID). In this way there's no need to flush the cache every time a process is switched, since there will be no ambiguity thanks to the PID in the tag.
	\item[Synonyms] it may happen that a process refers to the same physical address with more than one virtual address. This implies that if one block is modified in cache, the one referred by another virtual address will have the wrong value.\\
	A possible software solution is to enforce aliases to share some address bits, a technique called \emph{page coloring}.
	\item[I/O] I/O typically uses physical addresses and this requires mapping to virtual address every time I/O needs to interact with a virtual cache.
\end{description}

\newpage
\section{Hypothetical Question 8}
Explain the main techniques used to reduce the miss rate, the miss penalty and the hit time in caches.

\separator

%
%%%
\subsection*{Reducing the miss rate}
%
\begin{paragraph}{Increase the block size}
will improve the compulsory misses due to the cache being initially empty, thanks to the concept of spatial locality.\\
However larger blocks increase the miss penalty and may increase the conflict and capacity misses as well.
\end{paragraph}
%
\begin{paragraph}{Increase the associativity}
will reduce the conflict misses rate thanks to the rule of thumb
\begin{align*}
	miss rate_{direct\ mapped}\ size N \approx miss rate_{2-way\ associative} size \frac{N}{2}
\end{align*}
However the increased associativity will cause a slower hit time.
\end{paragraph}
%
\begin{paragraph}{Victim cache}
every time a block is discarded it is ``remembered''. Being it already fetched it will be accessible faster than the main memory. For every cache entry there is a limited number of previously discarded blocks (the ``victims''). On every miss the victim cache is checked before going through the main memory. If it is found there the victim block and the cache block are swapped.
\end{paragraph}
%
\begin{paragraph}{Pseudo-Associativity}
the cache is divided in two, and on every miss on the first part, the second part is checked before going to the next level of the memory hierarchy.
This can be easily implemented by swapping the most significant bit of the cache address.\\
This structure causes the cache to have a fast hit and a slow (or ``pseudo'') hit, and this may represent an issue in the CPU pipeline design. For this motivation this technique is better for cache not directly tied with the processor, like L2 caches.
\end{paragraph}
%
\begin{paragraph}{Prefetching instructions and data}
\begin{description}[labelindent=1cm]
	\item[Instruction prefetching] CPU fetches 2 blocks on a miss: the requested one and the consecutive one. The former is put in cache, while the latter is put in a stream buffer, which is checked on a miss.
	\item[Data prefetching] Different techniques exists:
	\begin{description}[labelindent=0.8cm]
		\item[Prefetch on miss] block $b+1$ is prefetched on miss on block $b$.
		\item[One block lookahead] block $b+1$ is prefetched when block $b$ is accessed.
		\item[Strided prefetch] tries to predict the access step. E.g. if blocks $b$, $b+N$, $b+2N$ are fetched, then block $b+3N$ is prefetched.
	\end{description}
\end{description}
\end{paragraph}
%
\begin{paragraph}{Software prefetching}
the compiler inserts prefetch instructions to request the data before they are needed. This obviously makes sense only if the CPU is allowed to proceed while the prefetched data are being fetched.
\end{paragraph}
%
\begin{paragraph}{Compiler optimizations}
the compiler may introduce various optimizations in order the exploit the data locality. Let's go through some of them:
\begin{description}[labelindent=1cm]
	\item[Merging arrays] improves spatial locality by merging two arrays in a single compound.
	\item[Loop interchange] improves spatial locality by exchanging the nesting of loops and making the code to access the data in the order they are stored.
	\item[Loop fusion] improves the spatial locality by combining the bodies of two loops that perform the same iterations having some variable overlapping.
	\item[Blocking] improves temporal locality by accessing blocks of data instead of going down the single rows or column of a multi-dimensional array.
\end{description}
\end{paragraph}
%
%%%
\subsection*{Reducing the miss penalty}
%
\begin{paragraph}{Read priority over write on miss}
this optimization serves reads before writes have completed. In case of a write-through caches, write buffers are a great improvement. However they complicate memory access, in the sense that they may contain the updated value for a block that is missing in cache on a read.\\
The simplest way to deal with this problem is to wait for the buffer to be empty. Clearly this solution is slow, so the alternative is to check the write buffers on a read miss looking for an updated value.
\end{paragraph}
%
\begin{paragraph}{Sub-block placement}
loading a sub-block instead of the full block on a miss. Requires additional valid bits to indicate valid sub-blocks.
\end{paragraph}
%
\begin{paragraph}{`Early restart' and `Critical word first'}
the key of this techniques is impatience. Instead of waiting for the full block to be loaded, the CPU is restarted as soon as the needed word is available.
Let's see how this two techniques work:
\begin{description}[labelindent=1cm]
	\item[Early restart] the block is fetched in the usual order, but the CPU is restarted as soon as the requested word is available.
	\item[Critical word first] the needed work is fetched first and provided to the CPU that can restart. In the meanwhile the rest of the block is loaded into memory. This technique is also called \emph{Wrapped fetch}.
\end{description}
Clearly this techniques are effective when the block size is large.
\end{paragraph}
%
\begin{paragraph}{Non-blocking caches}
this caches allow the cache to continue to perform useful operations during a miss, instead of ignoring the requests of the CPU.
\begin{description}[labelindent=1cm]
	\item[hit under miss] allows data cache to continue to supply cache hit during a miss.
	\item[miss under miss] allows multiple misses to occur at the same time.
\end{description}
\end{paragraph}
%
\begin{paragraph}{L2 caches}
introducing a second-level cache allows to keep the first-level one to be small enough to match the cycle time of the CPU, while the second-level one can be large enough to capture many accesses that would go to main memory, lessing the effective miss penalty.
The average memory access time can be computed as follows
\begin{align*}
 	AMAT = Hit\ Time_{L1} + Miss\ Rate_{L1} \cdot (Hit\ Time_{L2} +\\+ Miss\ Rate_{L2} \cdot Miss Penalty_{L2})
\end{align*} 
\end{paragraph}
%
%%%
\subsection*{Reducing the hit time}
%
\begin{paragraph}{Small and simple caches}
Small caches can improve hit time since smaller memory takes less time to index.\\
Simple caches, i.e. direct mapped caches, allow to overlap the tag check with the data transmission since there is no choice.
\end{paragraph}
%
\begin{paragraph}{Avoiding Address Translation}
Virtually addressed caches have some advantage but come with many issues to deal with, as described in Section~\ref{sec:question7}.
A technique to improve this kind of caches is to index the cache addresses using part of the physical addresses.This allows to start the tag access in parallel with the address translation, speeding up the whole process.
\end{paragraph}
%
\begin{paragraph}{Pipelining writes}
Cache access can be pipelined.
This technique increases the bandwidth of instructions rather that decreasing the actual latency of a cache hit.
\end{paragraph}
%
\begin{paragraph}{Trace caches}
in order to go beyond spatial locality, a trace cache finds a dynamic sequence of instructions to load into a cache block.\\
The drawback of this technique is that an instruction may appear multiple time in cache, since branches making different choices result as part of separate traces.
\end{paragraph}

\end{document}