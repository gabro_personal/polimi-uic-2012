\documentclass[11pt,a4paper]{article}

\usepackage{hyperref}
\usepackage{amsmath}

\title{\textsc{Data Mining Summary}}
\author{Gabriele Petronella}

\begin{document}

\tableofcontents
\clearpage
	
%%% Information Retrieval %%%
\section{Information Retrieval}
Information retrieval deals with the problem of locating relevant documents w.r.t. the user input or preference.
Main IR approaches
\begin{itemize}
	\item ``pull'', the user decides to take out a relevant information from the collection
	\item ``push'', the system decides to notify the user of a new information if it is considered of interest.
\end{itemize}

\subsection{Measures for text retrieval}
Basic measures for text retrieval:
\begin{description}
	\item[Precision] percentage of retrieved documents that are in fact relevant to the query.
	\begin{align*}
		precision = \dfrac{|\{Relevant\}\cap\{Retrieved\}|}{|\{Retrieved\}|}
	\end{align*}
	\item[Recall] percentage of documents that are relevant to the query and were, in fact, retrieved
	\begin{align*}
	 	recall = \dfrac{|\{Relevant\}\cap\{Retrieved\}|}{|\{Relevant\}|}
	\end{align*}
	\item[F-Score] harmonic mean of recall and precision
	\begin{align*}
		F\mbox{-}Score = \dfrac{recall \times precision}{(recall+precision)/2}
	\end{align*}
\end{description}

\subsection{Methods for text retrieval}
Two categories
\begin{description}
	\item[Document selection] the query defines specific constraints for selecting relevant documents.
	\item[Document ranking] the documents are ranked in order of relevance w.r.t. the query.
\end{description}

\subsubsection{Vector space model}
Technique for document ranking.
The idea of vector space model is to represent both a document and a query as vectors in a high-dimensional space, corresponding to all the keywords, and use an appropriate similarity measure to compute the similarity between the the query and the document vectors.
The similarity values can then be used for ranking documents.

Two possible similarity measures are:
\begin{description}
	\item[Term Frequency] the number of occurrence of a term in a document w.r.t. to the total number of terms.
	\item[Inverse Term Frequency] the importance of a term in a set of documents: if a term is very common in different documents its importance is scaled down (i.e. the term \emph{database} in a set of research papers about database systems).
\end{description}

Typically the two measures are combined together, which forms the \emph{TF-IDF measure}.

\subsection{Text indexing}
Two main techniques for indexing:
\begin{description}
	\item[Inverted index] It maintains two tables
		\begin{description}
			\item[document table] a set of document records each containing a \emph{doc\_id} and a \emph{posting\_list}, which is a list of the terms that occur in the document, sorted by relevance.
			\item[term table] a set of term records each containing a \emph{term\_id} and a \emph{posting\_list}, which is a list of document identifiers in which the term appears. 
		\end{description}
		Easy to implement, but requires a lot of storage space and it is not so good at handling \emph{synonymy} and \emph{polysemy}.
	\item[Signature file] It stores a signature record for each document in the database. Every signature has a fixed size of $b$ bits representing terms. Every term is mapped to a bit, which is set to 1 if the term appears in the document.

	The problem of multiple-to-one mappings is the major disadvantage of this approach.
\end{description}

\subsection{Dimensionality reduction}
Since the number of terms and the number of documents are usually quite large, we need some dimensionality reduction techniques. Here's the most relevant ones:
\begin{description}
	\item[Latent Semantic Indexing] uses the SVD (Single Value Decomposition) to reduce the size of the frequency table. The basic idea of LSI is to extract the most representative features.
	\item[Locality Preserving Indexing] tries to preserve locality between documents that are `close' in the original space also in the transformed space.
	\item[Probabilistic Latent Semantic Indexing] similar to LSI, but uses a probabilistic model instead of SVD.
\end{description}

%%% Text Mining %%%
\section{Text Mining}
Text Mining aims to extract useful knowledge from text
documents.
\subsection{Approaches}
Here's the main text mining approaches:
\begin{description}
	\item[Keyword-Based Association Analysis] Collects sets of keywords or terms that occur frequently together and then finds the association or correlation relationships among them. Each document is seen as a transaction, where the keywords of the document represent the item set.

	A set of frequently occurring consecutive or closely located keywords may form a \emph{term} or a \emph{phrase}, compound or noncompound.

	Mining based on these associations is referred to as \emph{term-level association mining}, which more effective w.r.t. word-level.

	\item[Document Classification Analysis] Solves the problem of labeling automatically text documents on the basis of topic, style and purpose.
	Usual classification techniques can be used to learn from a training set of manually labeled documents.
	Main approaches:
	\begin{description}
		\item[Similarity-based] exploits IR and KNN: two documents are similar if they share similar document vectors.
		\item[Dimensionality reduction] a feature selection process can be used to remove terms in the training documents that are statistically uncorrelated with the class labels. This will reduce the set of terms to be used in classification, thus improving both efficiency and accuracy.
		\item[Naive-Ba\"ies] a Bayesian classifier first trains the model by calculating a generative document distribution $P(d|c)$ to each class c of document d and then tests which class is most likely to generate the test document.
	\end{description}
\end{description}

%%% Web Mining %%%
\section{Web Mining}

\subsection{Web page layout structure mining}
Compared to a traditional plain text, a web page has more structures. The basic structure of a Web page is its DOM (Document Object Model) a tree structure, where every HTML tag in the page corresponds to a node in the DOM tree. However some issues are out there:
\begin{itemize}
	\item Not all the pages follow the standards
	\item The DOM may not reflect the page semantic
\end{itemize}

\subsection{Web page link structure mining}
We want to identify \emph{authoritative pages} and this is possible by analyzing the linkage structure.
Main issues:
\begin{itemize}
	\item Not all links always represent endorsement (e.g. advertising)
	\item Competitors do not usually link each other
	\item Authoritative pages are seldom particularly descriptive. (e.g. the home page of Yahoo! may not contain the explicit self-description ``Web search engine'').
\end{itemize}
In order to discover authoritative pages we should look also for \emph{hub pages}, that provide collection of links to authorities.
In general \emph{hubs} and \emph{authorities} have a mutual reinforcement relationship.

\subsubsection{Hyperlink-Induced Topic Search}
\begin{enumerate}
	\item Collect a starting set, called \emph{root set}, via an index-based search engine.
	\item Expand the set including pages linked by and linking to the root set pages, obtaining a \emph{base set}.
	\item Authority and hub weight are then iteratively computed as follows
	\begin{align*}
	 	a_p = \sum\limits_{\forall q:q\rightarrow p}h_q
	 	\hspace{2cm}
	 	h_p = \sum\limits_{\forall q:q\leftarrow p}a_q
	\end{align*} 
\end{enumerate}

\subsection{Multimedia Web Data Mining}
Mining multimedia data contents on the web is different from general-purpose multimedia data mining.
Multimedia data is embedded in web pages and links and surrounding text may help the data mining process.

VIPS algorithm is the basis to extract knowledge and a semantic \emph{image graph} can be built.
Such a graph can be used for classification and clustering purposes.

\subsection{Web usage mining}
Web usage mining is the extraction of interesting knowledge from server log files.
Logs can be collected at different levels
\begin{itemize}
	\item Server side
	\item Proxy side
	\item Client side
\end{itemize}
\subsubsection{Preprocessing}
In order to perform data mining task, the data needs some preprocessing step.
First of all useless information, such as images in content requests, are removed from the weblogs. Robots and web spiders are removed as well.

Then we try to identify the sessions of different users, reconstructing the navigation path in identified sessions. This can be done using cookies, javascript, URL rewiring, \ldots

Finally another preprocessing step may be the retrieval of the web pages content, instead of working just on URLs.

\subsubsection{Mining Techniques}
Three main techniques may be applied
\begin{itemize}
	\item Association rules
	\item Sequential pattern extraction
	\item Clustering of sessions
\end{itemize}

%%% Graph mining %%%
\section{Graph Mining}

\subsection{Mining frequent subgraphs}
Given a graph data set $D=\{G_1,G_2,\ldots,G_n\}$ we define $support(g)$ as the percentage of graphs in $D$ where $g$ is a subgraph.
A graph is said to be \emph{frequent} if its support is above a threshold called \emph{min\_sup}.

The discovery of frequent subgraphs (or \emph{substructures}) is divided in two steps:
\begin{enumerate}
	\item We generate frequent substructure candidates
	\item We check the frequency of each substructure
\end{enumerate}
The first step is computationally very expensive being a NP-complete problem. We want to reduce the complexity of such a problem and there are two basic approaches in order to do that:
\begin{description}
	\item[Apriori-based] breadth-first search: the search for frequent graphs starts with graphs of small ``size'' and proceeds in a bottom-up manner by increasing the size of the candidates.
	The size is increased by joining two similar substructures.
	Very high computational cost due to the merging step.
	\item[Pattern-Growth] depth-first search: for each discovered graph $g$, it performs extensions recursively until all the frequent graphs with $g$ embedded are discovered.
	Simple but not very efficient, since the same subgraph may be discovered multiple times.
\end{description}

\subsection{Graph indexing}
Indexing is basilar for effective search and query processing.
Two main techniques for indexing graphs:
\begin{description}
	\item[Path-based] takes path as indexing unit: all path up to $MaxL$ length are indexed.
	\item[gIndex] takes frequent and discriminative subgraphs as indexing unit. 
\end{description}

\subsection{Mining techniques}
Main graph mining tasks:
\begin{description}
	\item[Classification] frequent and discriminative subgraphs are used as features to perform the classification tasks.
	\item[Clustering] mined frequent subgraphs are used to define similarity between graphs. 
\end{description}

\end{document}