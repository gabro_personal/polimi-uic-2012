#! /usr/bin/env python

from math import log
from sys import float_info

# Compute the entropy for the given values
def entropy(values):
	entropy = 0
	for x in values:
		if x == 0:
			# Avoids log(0) !
			x = float_info.epsilon
		entropy -= x*log(x,2)
	return entropy

if __name__ == '__main__':
	import argparse

	values = []

	# Build a list of floats from the command line parameters. Exceptions handled automatically.
	parser = argparse.ArgumentParser()
	parser.add_argument('values', nargs='+', type=float)
	values = parser.parse_args().__dict__.values()[0]
	
	msg = "The entropy for {values} is {entropy}"
	print(msg.format(values = values, entropy = entropy(values)))